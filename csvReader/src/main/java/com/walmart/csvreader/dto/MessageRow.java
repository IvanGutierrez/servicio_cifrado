package com.walmart.csvreader.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class MessageRow {
	
	String message;
	
	AsociadosDTO asociado;
	
	public MessageRow(String message, AsociadosDTO asociado) {
		super();
		this.message = message;
		this.asociado = asociado;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MessageRow [asociado=").append(asociado).append(", message=").append(message).append("]");
		return builder.toString();
	}

	

}
