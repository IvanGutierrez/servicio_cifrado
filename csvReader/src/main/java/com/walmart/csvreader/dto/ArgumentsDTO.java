package com.walmart.csvreader.dto;

import com.beust.jcommander.Parameter;
import com.walmart.csvreader.validator.KeyValidator;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class ArgumentsDTO {
	
	@Parameter(names= {"--help", "-h"}, help = true)
	private boolean help = false;
	
	@Parameter(names= {"--pathFile", "-pf"}, description = "Path of csv file", required = true)
	private String path;
	
	@Parameter(names= {"--secretKey", "-s"}, description = "Encryption secret key", required = true, validateWith = KeyValidator.class)
	private String secretKey;
	
	@Parameter(names= {"--vector", "-v"}, description = "Encryption vector", required = true, validateWith = KeyValidator.class)
	private String vector;
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ArgumentsDTO [help=").append(help).append(", path=").append(path).append(", configurationFile=").append("]");
		return builder.toString();
	}

	
}
