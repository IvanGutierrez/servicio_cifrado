package com.walmart.csvreader.business;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.walmart.csvreader.business.converter.AsociadoConverter;
import com.walmart.csvreader.dto.ArgumentsDTO;
import com.walmart.csvreader.dto.AsociadosDTO;
import com.walmart.csvreader.exception.BusinessException;
import com.walmart.csvreader.model.Asociado;
import com.walmart.csvreader.repository.AsociadoDatPersonalRepository;
import com.walmart.csvreader.repository.AsociadoRepository;

@Service
public class AsociadoBusinessImpl implements AsociadoBusiness{

	@Autowired
	private AsociadoRepository asociadoRepository;
	
	@Autowired
	private AsociadoDatPersonalRepository asociadoDatPersonalRepository;
	
	@Override
	@Transactional(readOnly = false)
	public void saveAsociadoInformation(AsociadosDTO asociadoDTO, ArgumentsDTO arguments) {
		int countNoAsociado = asociadoRepository.findCountByNoAsoc(asociadoDTO.getNoAsoc());
		if(countNoAsociado>0) {
			throw new BusinessException("No asociado already exist");
		}
		Asociado asociado = asociadoRepository.save(AsociadoConverter.convertToAsociadoFromAsociadosDTO(asociadoDTO, arguments.getSecretKey(), arguments.getVector()));
		asociadoDatPersonalRepository.save(AsociadoConverter.convertToAsocDatPersonalFromAsociadosDTO(asociadoDTO, asociado.getIdAsoc(),arguments.getSecretKey(), arguments.getVector()));
		
	}

}
