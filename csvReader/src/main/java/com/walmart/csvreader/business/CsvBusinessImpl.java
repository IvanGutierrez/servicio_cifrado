package com.walmart.csvreader.business;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.enums.CSVReaderNullFieldIndicator;
import com.walmart.csvreader.dto.ArgumentsDTO;
import com.walmart.csvreader.dto.AsociadosDTO;
import com.walmart.csvreader.dto.MessageRow;
import com.walmart.csvreader.exception.BusinessException;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.StreamSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CsvBusinessImpl implements CsvBusiness {

	private static final Logger LOGGER = LoggerFactory.getLogger(CsvBusinessImpl.class);

	private static final char CSV_DELIMITER = ';';

	@Autowired
	private AsociadoBusiness asociadoBusiness;

	@Override
	public void saveAsociateCsvInformation(ArgumentsDTO arguments) {
		
		List<MessageRow> asociadosErrorList = new ArrayList<MessageRow>();
		List<AsociadosDTO> asociadosExistentList = new ArrayList<AsociadosDTO>();
		AtomicInteger successCount = new AtomicInteger(0);
		AtomicInteger errorCount = new AtomicInteger(0);
		AtomicInteger existentCount = new AtomicInteger(0);
		try (Reader reader = Files.newBufferedReader(Paths.get(arguments.getPath()));) {
			String path = new File(".").getCanonicalPath();

			CsvToBean<AsociadosDTO> cb = new CsvToBeanBuilder(reader).withType(AsociadosDTO.class).withSkipLines(1)
					.withSeparator(CSV_DELIMITER).withFieldAsNull(CSVReaderNullFieldIndicator.EMPTY_SEPARATORS).withIgnoreLeadingWhiteSpace(true).withThrowExceptions(true).build();

			StreamSupport.stream(cb.spliterator(), false).forEach(asociado ->{
				try {
					asociadoBusiness.saveAsociadoInformation(asociado, arguments);
					successCount.getAndIncrement();
				} catch (BusinessException e) {
					asociadosExistentList.add(asociado);
					existentCount.getAndIncrement();
					LOGGER.error(e.getMessage(),e);
				} catch(Exception e) {
					errorCount.getAndIncrement();
					asociadosErrorList.add(new MessageRow(e.getMessage(),asociado));
					LOGGER.error(e.getMessage(),e);
				}
			});
			
			LOGGER.info("-------------------------------------");
			LOGGER.info("Successful row(s): {}", successCount);
			LOGGER.info("-------------------------------------");
			LOGGER.info("Error row(s): {}", errorCount);
			asociadosErrorList.forEach(m -> LOGGER.info(m.toString()));
			LOGGER.info("-------------------------------------");
			LOGGER.info("Repeat row(s): {}", existentCount);
			asociadosExistentList.forEach(m -> LOGGER.info(m.toString()));
			LOGGER.info("-------------------------------------");
			
		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

}
