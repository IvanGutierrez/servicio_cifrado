package com.walmart.csvreader.business;

import com.walmart.csvreader.dto.ArgumentsDTO;
import com.walmart.csvreader.dto.AsociadosDTO;

public interface AsociadoBusiness {
	
	public void saveAsociadoInformation(AsociadosDTO asociadoDTO, ArgumentsDTO arguments);

}
