package com.walmart.csvreader.validator;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

public class KeyValidator implements IParameterValidator{

	@Override
	public void validate(String name, String value) throws ParameterException {
		if(value != null && value.length()!=24) {
			 throw new ParameterException(
		              "Key length is incorrect");
		}
		
	}

}
