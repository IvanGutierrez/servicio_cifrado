package com.walmart.csvreader.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserCursoAvance extends JpaRepository<UserCursoAvance, Long>{

}
