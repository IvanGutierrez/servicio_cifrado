package com.walmart.csvreader.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.walmart.csvreader.model.AsocDatPersonal;

@Repository
public interface AsociadoDatPersonalRepository extends JpaRepository<AsocDatPersonal, Long>{

}
