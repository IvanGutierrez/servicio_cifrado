package com.walmart.csvreader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.beust.jcommander.JCommander;
import com.walmart.csvreader.business.CsvBusiness;
import com.walmart.csvreader.dto.ArgumentsDTO;

@Component
public class CommandLineMain implements CommandLineRunner{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CommandLineMain.class);
	
	@Autowired
	private CsvBusiness csvReader;

	@Override
	public void run(String... args) throws Exception {
		LOGGER.info("Validating parameters");
		ArgumentsDTO arguments = new ArgumentsDTO();
		JCommander jct = JCommander.newBuilder().addObject(arguments).acceptUnknownOptions(true).build();
		jct.parse(args);
		LOGGER.info("Parameters: {}",arguments);
		if(arguments.isHelp()) {
			jct.usage();
		} else {
			csvReader.saveCsvInformation(arguments);
		}
		
	}

}
