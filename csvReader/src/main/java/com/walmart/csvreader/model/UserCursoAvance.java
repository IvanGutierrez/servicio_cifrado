package com.walmart.csvreader.model;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "UserCursoAvance")
@Getter @Setter @NoArgsConstructor
public class UserCursoAvance implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdUsuCurAva")
    private Long idUsuCurAva;
    @JoinColumn(name = "IdUsu", nullable = false)
    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    private Asociado idUsu;
    @JoinColumn(name = "IdCur", nullable = false)
    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    private Cursos idCur;
    @Basic(optional = false)
    @Column(name = "Ava")
    private Long ava;
    @Basic(optional = false)
    @Column(name = "FecFin")
    private Long fecFin;
    
	@Override
	public int hashCode() {
		return Objects.hash(idUsuCurAva);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserCursoAvance other = (UserCursoAvance) obj;
		return Objects.equals(idCur, other.idCur);
	}
    

    

    
    
}
