package com.walmart.csvreader.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "AsocDatPersonal")
@Getter @Setter @NoArgsConstructor
public class AsocDatPersonal implements Serializable{
	
	private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "IdAsoc")
    private Long idAsoc;
    @Basic(optional = false)
    @Column(name = "CerEst")
    private String cerEst;
    @Basic(optional = false)
    @Column(name = "Curp")
    private String curp;
    @Basic(optional = false)
    @Column(name = "RFC")
    private String rfc;
    @Basic(optional = false)
    @Column(name = "IMSS")
    private String imss;
    @Basic(optional = false)
    @Column(name = "RazSoc")
    private String razSoc;
    @Basic(optional = false)
    @Column(name = "Escol")
    private String escol;
    @Column(name = "FecAlta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecUVis;
    @Column(name = "FechUMod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecAct;
	
    @Override
	public int hashCode() {
		return Objects.hash(idAsoc);
	}
    
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AsocDatPersonal other = (AsocDatPersonal) obj;
		return Objects.equals(idAsoc, other.idAsoc);
	}
    
    
    
}
