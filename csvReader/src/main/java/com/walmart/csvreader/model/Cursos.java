package com.walmart.csvreader.model;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "Cursos")
@Getter @Setter @NoArgsConstructor
public class Cursos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdCur")
    private Long idCur;
    @Basic(optional = false)
    @Column(name = "IdRef")
    private Integer idRef;
    
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Cursos [idCur=").append(idCur).append(", IdRef=").append(idRef).append("]");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		return Objects.hash(idCur);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cursos other = (Cursos) obj;
		return Objects.equals(idCur, other.idCur);
	}
    

    

    
    
}
