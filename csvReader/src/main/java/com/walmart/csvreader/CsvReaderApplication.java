package com.walmart.csvreader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan({"com.walmart.csvreader"})
@SpringBootApplication
@EnableJpaRepositories(basePackages = {"com.walmart.csvreader.*"})
@EntityScan(basePackages = {"com.walmart.csvreader.*"})
public class CsvReaderApplication {

	public static void main(String[] args) {
		SpringApplication.run(CsvReaderApplication.class, args);
	}

}
