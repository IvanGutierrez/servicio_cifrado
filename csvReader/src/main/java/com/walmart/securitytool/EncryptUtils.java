package com.walmart.securitytool;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import com.walmart.securitytool.EncryptionException;
import com.walmart.securitytool.SecurityMessageDTO;

public class EncryptUtils {

	public static final String AES_ALGORITHM = "AES";
	public static final String CYPHER_CBC = "AES/CBC/PKCS5Padding";

	public static String encrypt(String cypherType, SecurityMessageDTO securityMessage) {
		
		if(securityMessage.getMessage()==null) {
			return null;
		}
		String finalMessage = "";

		try {
			byte[] cypherText = generateCipher(Cipher.ENCRYPT_MODE, cypherType, securityMessage);
			Base64.Encoder encoder = Base64.getEncoder();
			String encryptedText = encoder.encodeToString(cypherText);
			finalMessage = encryptedText;
		} catch (NoSuchAlgorithmException | IllegalBlockSizeException | BadPaddingException | InvalidKeyException
				| InvalidAlgorithmParameterException | NoSuchPaddingException e) {
			throw new EncryptionException(e.getMessage(), e);
		}

		return finalMessage;

	}

	public static String decrypt(String cypherType, SecurityMessageDTO securityMessage) {
		String finalMessage = "";
		try {
			byte[] cypherText = generateCipher(Cipher.DECRYPT_MODE, cypherType, securityMessage);
			finalMessage =new String(cypherText);
		} catch (NoSuchAlgorithmException | IllegalBlockSizeException | BadPaddingException | InvalidKeyException
				| InvalidAlgorithmParameterException | NoSuchPaddingException e) {
			throw new EncryptionException(e.getMessage(), e);
		}

		return finalMessage;

	}

	private static byte[] generateCipher(int mode, String cypherType, SecurityMessageDTO securityMessage)
			throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
			InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {

		Cipher cipher = Cipher.getInstance(cypherType);
		KeyGenerator keyGenerator = KeyGenerator.getInstance(EncryptUtils.AES_ALGORITHM);
		keyGenerator.init(128);

		byte[] decodedKey = Base64.getDecoder().decode(securityMessage.getSecretKey());
		SecretKey secretKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, EncryptUtils.AES_ALGORITHM);

		Base64.Decoder decoder = Base64.getDecoder();
		byte[] messageBytes = mode == Cipher.ENCRYPT_MODE ? securityMessage.getMessage().getBytes() : decoder.decode(securityMessage.getMessage());
		byte[] encryptedVector = decoder.decode(securityMessage.getVector());

		SecretKeySpec keySpec = new SecretKeySpec(secretKey.getEncoded(), AES_ALGORITHM);
		IvParameterSpec ivSpec = new IvParameterSpec(encryptedVector);
		byte[] cypherText;
		cipher.init(mode, keySpec, ivSpec);
		cypherText = cipher.doFinal(messageBytes);
		return cypherText;

	}

}
