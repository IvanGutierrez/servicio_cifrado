/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.business;

import com.walmart.app.customfeatures.model.Asociado;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import com.walmart.app.customfeatures.repository.GenericOperationsRepository;
import com.walmart.app.customfeatures.repository.impl.GenericOperationsRepositoryImpl;
import com.walmart.app.customfeatures.util.GenerateDataSourceForTestUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 *
 * @author alan
 */
public class AsociadoBusinessTest {
    
    private static AsociadoSaveBatchBusiness asociadoBusiness;
    private static final String[] csvColumns = new String[]{null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,"México",null};

    private DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
    
    @BeforeAll
    public static void init(){
        GenericOperationsRepository genericRepository = new GenericOperationsRepositoryImpl();
        genericRepository.setNamedParameterJdbcTemplate(GenerateDataSourceForTestUtil.buildDataSrouceForTest());
        asociadoBusiness = new AsociadoSaveBatchBusiness();
        asociadoBusiness.setGenericRepository(genericRepository);
    }

    @Test
    public void testStr(){
        String fileName = "reporte_asdasd_2342342.zip";
        String finalFileName = fileName.split("_")[0].concat(fileName.substring(fileName.lastIndexOf(".")));
        System.out.println(finalFileName);
        Assertions.assertEquals("reporte.zip",finalFileName);
    }


    @Test
    public void testAsociado7caracteres(){
        String noAsociado = "1234567";
        String result = asociadoBusiness.completarNumeroAsociado(csvColumns,noAsociado);
        Assertions.assertEquals(noAsociado ,result );
    }

    @Test
    public void testAsociadoMenor7caracteres(){
        String noAsociado = "123456";
        String result = asociadoBusiness.completarNumeroAsociado(csvColumns,noAsociado);
        Assertions.assertEquals("0"+noAsociado,result );
    }

    @Test
    public void testAsociadoMenor7caracteres2(){
        String noAsociado = "123";
        String result = asociadoBusiness.completarNumeroAsociado(csvColumns,noAsociado);
        Assertions.assertEquals("0000"+noAsociado,result );
    }

    @Test
    public void testAsociadoMayor7caracteres(){
        String noAsociado = "123456789";
        String result = asociadoBusiness.completarNumeroAsociado(csvColumns,noAsociado);
        Assertions.assertEquals(noAsociado,result );
    }



    
    @Test
    public void testOneChangeOnAsociadoObjects(){
        Asociado oldAsociado = new Asociado(1L);
        Asociado newAsociado = new Asociado(1L);
        oldAsociado.setEma("a@a.com");
        newAsociado.setEma("b@a.com");
        List<String> changesList = asociadoBusiness.getAsociadoChanges(oldAsociado, newAsociado);
        Assertions.assertEquals("[Ema]::("+oldAsociado.getEma()+") change to ("+newAsociado.getEma()+")",changesList.stream().findFirst().get());
    }
    
    @Test
    public void testDatesAllowedChangeOnAsociadoObjects(){
        Asociado oldAsociado = new Asociado(1L);
        Asociado newAsociado = new Asociado(1L);
        
        oldAsociado.setEma("a@a.com");
        newAsociado.setEma("b@a.com");
        try{
            oldAsociado.setFecAct(dateFormat.parse("2020/12/12 12:12:12"));
            newAsociado.setFecAct(dateFormat.parse("2020/12/12 12:12:00"));
            oldAsociado.setFecNac(dateFormat.parse("2020/12/12 12:12:12"));
            newAsociado.setFecNac(dateFormat.parse("2020/12/12 12:12:12"));
        }catch(Exception e){
            e.printStackTrace();
        }
        List<String> changesList = asociadoBusiness.getAsociadoChanges(oldAsociado, newAsociado);
        Assertions.assertEquals(changesList.size(), 2);
    }
    
    @Test
    public void testNoChangeOnAsociadoObjects(){
        Asociado oldAsociado = new Asociado(1L);
        Asociado newAsociado = new Asociado(1L);
        
        oldAsociado.setEma("a@a.com");
        newAsociado.setEma("a@a.com");
        try{
            oldAsociado.setFecAct(dateFormat.parse("2020/12/12 12:12:12"));
            newAsociado.setFecAct(dateFormat.parse("2020/12/12 12:12:12"));
            oldAsociado.setFecNac(dateFormat.parse("2020/12/12 12:12:12"));
            newAsociado.setFecNac(dateFormat.parse("2020/12/12 12:12:12"));
        }catch(Exception e){
            e.printStackTrace();
        }
        List<String> changesList = asociadoBusiness.getAsociadoChanges(oldAsociado, newAsociado);
        Assertions.assertEquals(changesList.size(), 0);
    }
    
    @Test
    public void testDatesNotAllowedChangeOnAsociadoObjects(){
        Asociado oldAsociado = new Asociado(1L);
        Asociado newAsociado = new Asociado(1L);
        oldAsociado.setEma("a@a.com");
        newAsociado.setEma("b@a.com");
        try{
            oldAsociado.setPswCad(dateFormat.parse("2020/12/12 12:12:12"));
            newAsociado.setPswCad(dateFormat.parse("2020/12/12 12:12:00"));
            oldAsociado.setFecNac(dateFormat.parse("2020/12/12 12:12:12"));
            newAsociado.setFecNac(dateFormat.parse("2020/12/12 12:12:12"));
        }catch(Exception e){
            e.printStackTrace();
        }
        List<String> changesList = asociadoBusiness.getAsociadoChanges(oldAsociado, newAsociado);
        System.out.println("Size "+changesList.size());
        Assertions.assertNotEquals(changesList.size(), 2);
    }

    @Test
    public void testDateFormat(){
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
        long result = 0l;
        try{
            result = dateFormat.parse("21/4/00").getTime();
        }catch(Exception e){
            e.printStackTrace();
        }
        Assertions.assertEquals(956293200000L,result);
    }
    
}
