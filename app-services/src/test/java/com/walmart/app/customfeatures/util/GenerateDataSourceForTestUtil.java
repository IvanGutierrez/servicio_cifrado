package com.walmart.app.customfeatures.util;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;

public class GenerateDataSourceForTestUtil {

    public static final String ymlTest = "application-test.yml";

    public static NamedParameterJdbcTemplate buildDataSrouceForTest() {
        DataSource dataSource = DataSourceBuilder.create()
                .driverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver")
                .url("jdbc:sqlserver://servsqlwlxptest.database.windows.net;databaseName=UvelopWT")
                .username("wlxpLink2Test")
                .password("WmUvelop123/*").build();
        return new NamedParameterJdbcTemplate(dataSource);
    }

}
