package com.walmart.app.customfeatures.business;


import com.walmart.app.customfeatures.dto.DynamicReportAsyncResponseDTO;
import com.walmart.app.customfeatures.dto.DynamicReportRequestDTO;
import com.walmart.app.customfeatures.repository.config.DynamicReportsRepositoryImpl;
import com.walmart.app.customfeatures.util.GenerateDataSourceForTestUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class DynamiReportBusinessTest {

    private static String path;
    private static DynamicReportsRepositoryImpl dynamicReportsRepository = new DynamicReportsRepositoryImpl();
    private static TaskExecutor taskExecutorAux;
    private static DynamiReportBusiness dynamiReportBusiness = new DynamiReportBusiness();

    @BeforeAll
    public static void init(){
        path = "/home/alan/projects/servicio_cifrado/app-services/src/main/resources/sql/";
        dynamicReportsRepository = new DynamicReportsRepositoryImpl();
        dynamicReportsRepository.setNamedParameterJdbcTemplate(GenerateDataSourceForTestUtil.buildDataSrouceForTest());
        dynamiReportBusiness.setDynamicReportsRepository(dynamicReportsRepository);
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(2);
        executor.setKeepAliveSeconds(Integer.MAX_VALUE);
        executor.setThreadNamePrefix("report_task_executor_thread");
        executor.initialize();
        taskExecutorAux = executor;
        dynamiReportBusiness.setTaskExecutor(taskExecutorAux);
    }

    @Test
    public void createCsvOnDiskAsync(){
        DynamicReportRequestDTO dynamicReportRequestDTO = new DynamicReportRequestDTO();
        dynamicReportRequestDTO.setPath(path);
        dynamicReportRequestDTO.setFileNameSql("reporte_accesos.sql");
        dynamicReportRequestDTO.setReportFileName("LinkedInLearningHistoryX");
        List<DynamicReportAsyncResponseDTO> pathList = new ArrayList<>();
        AtomicInteger count = new AtomicInteger(0);
        try{
            do{
                dynamicReportRequestDTO.setReportFileName(""+count.getAndIncrement());
                pathList.add(dynamiReportBusiness.generateDynamicReportAsync(dynamicReportRequestDTO));
            }while(count.get() < 1);
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            pathList.forEach(path-> System.out.println(path));
        }
        Assertions.assertNotNull(pathList);
    }

    @Test
    public void test(){
        //RestTemplate restTemplate = new RestTemplate();
        //ResponseEntity<DynamicReportAsyncResponseDTO> response =  restTemplate.getForEntity("https://wd.brainb.mx:8091/reports/generic-report-csv-async?sqlFileName=dinAvanceCursoGlobal.sql&reportFileName=reporteAvanceCurso&cveCurso=541",DynamicReportAsyncResponseDTO.class);77
        //System.out.println(response.getBody().toString());
        Assertions.assertNotNull("response.getBody().toString()");
    }


}
