package com.walmart.app.securitytool.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class KeyDTO {
	
	private String secretKey;
	private String vector;
	
	public KeyDTO(String secretKey, String vector) {
		super();
		this.secretKey = secretKey;
		this.vector = vector;
	}

}
