package com.walmart.app.securitytool.controller;

import java.util.Optional;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.walmart.app.exception.BusinessException;
import com.walmart.app.securitytool.business.EncryptBusiness;
import com.walmart.app.securitytool.dto.KeyDTO;
import com.walmart.app.securitytool.dto.SecurityMessageDTO;

@RestController
@RequestMapping("/security")
@Validated
public class SecurityController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SecurityController.class);

	@Autowired
	private EncryptBusiness encryptBusiness;
	
	@GetMapping("/generateSecretKeys/{passphrase}")
	public ResponseEntity<KeyDTO> generateSecretKeys(@PathVariable @Size(min = 16 , max = 16) String passphrase) {
		return new ResponseEntity<KeyDTO>(encryptBusiness.generateSecretKey(passphrase), HttpStatus.OK);
	}
	
	@PostMapping("/encryptMessage")
	public ResponseEntity<String> encryptMessage(@RequestBody SecurityMessageDTO securityMessage) {
		try {
			Optional<String> message = encryptBusiness.encryptMessage(securityMessage);
			if(message.isPresent()) {
				return new ResponseEntity<String>(message.get(), HttpStatus.OK);
			}
		
		}catch(BusinessException e) {
			LOGGER.error(e.getMessage(),e);
		}
		return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@PostMapping("/decryptMessage")
	public ResponseEntity<String> decryptMessage(@RequestBody SecurityMessageDTO securityMessage) {
		try {
			Optional<String> message = encryptBusiness.decryptMessage(securityMessage);
			if(message.isPresent()) {
				return new ResponseEntity<String>(message.get(), HttpStatus.OK);
			}
		
		}catch(BusinessException e) {
			LOGGER.error(e.getMessage(),e);
		}
		return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
