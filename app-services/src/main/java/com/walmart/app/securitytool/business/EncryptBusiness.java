package com.walmart.app.securitytool.business;

import java.util.Optional;

import com.walmart.app.securitytool.dto.KeyDTO;
import com.walmart.app.securitytool.dto.SecurityMessageDTO;

public interface EncryptBusiness {
	
	public Optional<String> encryptMessage(SecurityMessageDTO securityMessage);
	
	public Optional<String> decryptMessage(SecurityMessageDTO securityMessage);
	
	public KeyDTO generateSecretKey(String passphrase);

}
