package com.walmart.app.securitytool.business;

import java.security.SecureRandom;
import java.util.Base64;
import java.util.Optional;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.stereotype.Service;

import com.walmart.app.exception.BusinessException;
import com.walmart.app.securitytool.business.exception.EncryptionException;
import com.walmart.app.securitytool.dto.KeyDTO;
import com.walmart.app.securitytool.dto.SecurityMessageDTO;
import com.walmart.app.securitytool.util.EncryptUtils;

@Service
public class EncryptedBusinessImpl implements EncryptBusiness{

	@Override
	public Optional<String> encryptMessage(SecurityMessageDTO securityMessage) {
		Optional<String> finalMessage = Optional.empty();
		try {
			finalMessage= Optional.ofNullable(EncryptUtils.encrypt(EncryptUtils.CYPHER_CBC, securityMessage));
			
		} catch (EncryptionException e) {
			 throw new BusinessException(e.getMessage(), e);
		}
		return finalMessage;
	}

	@Override
	public Optional<String> decryptMessage(SecurityMessageDTO securityMessage) {
		Optional<String> finalMessage = Optional.empty();
		try {
			finalMessage= Optional.ofNullable(EncryptUtils.decrypt(EncryptUtils.CYPHER_CBC, securityMessage));
			
		} catch (EncryptionException e) {
			 throw new BusinessException(e.getMessage(), e);
		}
		return finalMessage;
	}

	@Override
	public KeyDTO generateSecretKey(String passphrase) {
		SecretKey secretKey= new SecretKeySpec(passphrase.getBytes(), 0, passphrase.length(), EncryptUtils.AES_ALGORITHM);
		String encodedKey = Base64.getEncoder().encodeToString(secretKey.getEncoded());
		byte[] vector = new byte[16];
		SecureRandom random = new SecureRandom();
		random.nextBytes(vector);
		String encodedVector = Base64.getEncoder().encodeToString(vector);
		return new KeyDTO(encodedKey, encodedVector);
	}
	

}
