package com.walmart.app.securitytool.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class SecurityMessageDTO extends KeyDTO {

	private String message;

	public SecurityMessageDTO(String secretKey, String vector, String message) {
		super(secretKey, vector);
		this.message = message;
	}

}
