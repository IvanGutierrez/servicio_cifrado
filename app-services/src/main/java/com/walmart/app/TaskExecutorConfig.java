/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 *
 * @author alan
 */
@Configuration
public class TaskExecutorConfig {

    @Primary
    @Bean("CustomThreadPoolTaskExecutor")
    public TaskExecutor threadPoolTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(100);
        executor.setMaxPoolSize(100);
        executor.setThreadNamePrefix("default_task_executor_thread");
        executor.initialize();
        return executor;

    }
    
    @Bean("threadPoolTaskExecutorAux")
    public TaskExecutor threadPoolTaskExecutorAux() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(Runtime.getRuntime().availableProcessors());
        executor.setMaxPoolSize(1000);
        executor.setThreadNamePrefix("default_task_executor_thread_aux");
        executor.initialize();
        return executor;

    }

    @Bean("threadPoolTaskExecutorReports")
    public TaskExecutor threadPoolTaskExecutorReports() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(2);
        executor.setQueueCapacity(100);
        executor.setKeepAliveSeconds(Integer.MAX_VALUE);
        executor.setThreadNamePrefix("report_task_executor_thread");
        executor.initialize();
        return executor;

    }

}
