package com.walmart.app.util;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Stream;

public class StreamUtils {

	public static <T> Stream<T> asStream(final Collection<T> collection) {
		return Optional.ofNullable(collection).map(Collection::stream).orElse(Stream.empty());

	}
}
