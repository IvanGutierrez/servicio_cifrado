package com.walmart.app.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateUtils {
	
	public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DateUtils.class);
	
	public static final Optional<Date> convertStringToDate(String format, String date) {
		Optional<Date> dateOptional = Optional.empty();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
		try {
			dateOptional = Optional.of(simpleDateFormat.parse(date));
		} catch (ParseException e) {
			LOGGER.error(e.getMessage(),e);
		}
		
		return dateOptional;
	}
	
	public static final String convertDateToString(String format, Date date) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
		return simpleDateFormat.format(date);
	}

}
