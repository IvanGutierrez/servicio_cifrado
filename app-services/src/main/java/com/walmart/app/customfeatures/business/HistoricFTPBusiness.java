package com.walmart.app.customfeatures.business;

import com.walmart.app.customfeatures.dto.FTPFileDetailsDTO;
import com.walmart.app.customfeatures.util.SSUtils;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class HistoricFTPBusiness {

    private SSUtils ssUtils;

    public List<FTPFileDetailsDTO> getAllHistoricFTP() throws IOException {
        Path pathFTPHistoricFiles = Paths.get(ssUtils.getFTPHistoricPath());
        return Files.list(pathFTPHistoricFiles)
                .map(this::getFileDetails)
                .sorted((fd1,fd2) -> fd2.getDateInMillis().compareTo(fd1.getDateInMillis()))
                .collect(Collectors.toList());
    }

    public InputStream getFileInputStream(String fileName) throws FileNotFoundException {
        return new FileInputStream(new File(ssUtils.getFTPHistoricPath().concat(fileName)));
    }

    private FTPFileDetailsDTO getFileDetails(Path file) {
        FTPFileDetailsDTO ftpFileDetailsDTO = new FTPFileDetailsDTO();
        ftpFileDetailsDTO.setFileName(file.getFileName().toString());
        try{
            FileTime fileTime =  Files.getLastModifiedTime(file);
            Long timeInMillis = fileTime.toMillis();
            ftpFileDetailsDTO.setDate(new SimpleDateFormat("dd/MM/yyyy").format(new Date(timeInMillis)));
            ftpFileDetailsDTO.setDateInMillis(timeInMillis);
        }catch(IOException e){
            e.printStackTrace();
        }
        return ftpFileDetailsDTO;
    }

    public void setSsUtils(SSUtils ssUtils) {
        this.ssUtils = ssUtils;
    }
}
