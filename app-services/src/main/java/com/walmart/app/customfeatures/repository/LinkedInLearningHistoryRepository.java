package com.walmart.app.customfeatures.repository;

import com.walmart.app.customfeatures.dto.LearningActivityReportRequestDTO;
import com.walmart.app.customfeatures.dto.LearningActivityReportResponseDTO;
import com.walmart.app.customfeatures.model.LinkedInLearningHistory;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

public interface LinkedInLearningHistoryRepository {

    String getToken();

    LearningActivityReportResponseDTO getLearningActivityFromLinkedInService(LearningActivityReportRequestDTO requestDTO) throws URISyntaxException;

    Optional<LinkedInLearningHistory> findByWinNumberAndContentUrn(LinkedInLearningHistory linkedRow);

    int update(LinkedInLearningHistory linkedInLearningHistory);

    int save(LinkedInLearningHistory linkedInLearningHistory);

    void deleteHistory();

    Boolean isLinkedInLearningEmpty();

}
