package com.walmart.app.customfeatures.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PagingDTO implements Serializable {
    private Integer start;
    private Integer count;
    private Integer total;

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PagingDTO pagingDTO = (PagingDTO) o;
        return Objects.equals(start, pagingDTO.start) &&
                Objects.equals(count, pagingDTO.count) &&
                Objects.equals(total, pagingDTO.total);
    }

    @Override
    public int hashCode() {
        return Objects.hash(start, count, total);
    }

    @Override
    public String toString() {
        return "PagingDTO{" +
                "start=" + start +
                ", count=" + count +
                ", total=" + total +
                '}';
    }
}
