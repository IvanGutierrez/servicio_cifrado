/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.util;

/**
 *
 * @author alan
 */
public class RutaFormacionCSVConstants {

    public static final Integer PAIS = 0;
    public static final Integer NEGOCIO = 1;
    public static final Integer DETERMINANTE = 2;
    public static final Integer DEPARTAMENTO = 3;
    public static final Integer PUESTO = 4;
    public static final Integer CURSO = 5;
    public static final Integer SECUENCIA = 6;
    public static final Integer CREDITOS = 7;
    public static final Integer OBLIGATORIA = 8;
    public static final Integer FECHA_INICIO = 9;
    public static final Integer NIVEL = 10;
    public static final Integer D_NEGOCIO_R = 11;
    public static final Integer USUARIO = 12;
    public static final Integer PROGRAMA = 13;
    public static final Integer EJE = 14;
    public static final Integer SUB_EJE = 15;
    public static final Integer PRIORIDAD = 16;

}
