package com.walmart.app.customfeatures.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Objects;

public class FTPFileDetailsDTO implements Serializable {

    private String fileName;
    private String date;
    @JsonIgnore
    private Long dateInMillis;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getDateInMillis() {
        return dateInMillis;
    }

    public void setDateInMillis(Long dateInMillis) {
        this.dateInMillis = dateInMillis;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FTPFileDetailsDTO that = (FTPFileDetailsDTO) o;
        return Objects.equals(fileName, that.fileName) &&
                Objects.equals(date, that.date) &&
                Objects.equals(dateInMillis, that.dateInMillis);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fileName, date, dateInMillis);
    }

    @Override
    public String toString() {
        return "FTPFileDetailsDTO{" +
                "fileName='" + fileName + '\'' +
                ", date='" + date + '\'' +
                ", dateInMillis=" + dateInMillis +
                '}';
    }
}
