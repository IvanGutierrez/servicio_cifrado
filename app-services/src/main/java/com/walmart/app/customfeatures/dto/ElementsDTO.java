package com.walmart.app.customfeatures.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ElementsDTO implements Serializable {

    private LearnerDetailsDTO learnerDetails;
    private List<ActivitiesDTO> activities;
    private ContentDetailsDTO contentDetails;

    public LearnerDetailsDTO getLearnerDetails() {
        return learnerDetails;
    }

    public void setLearnerDetails(LearnerDetailsDTO learnerDetails) {
        this.learnerDetails = learnerDetails;
    }

    public List<ActivitiesDTO> getActivities() {
        return activities;
    }

    public void setActivities(List<ActivitiesDTO> activities) {
        this.activities = activities;
    }

    public ContentDetailsDTO getContentDetails() {
        return contentDetails;
    }

    public void setContentDetails(ContentDetailsDTO contentDetails) {
        this.contentDetails = contentDetails;
    }
}
