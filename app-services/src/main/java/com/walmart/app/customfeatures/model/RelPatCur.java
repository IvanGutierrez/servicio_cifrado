/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.model;

import java.sql.Timestamp;
import java.util.Objects;

/**
 *
 * @author alan
 */
@CustomTable(name = "RelPatCur")
public class RelPatCur {

    
    private Long IdPC;
    @CustomColumn(name = "IdPat")
    private Long IdPat;
    @CustomColumn(name = "IdCur")
    private Long IdCur;
    @CustomColumn(name = "ConSec")
    private Long ConSec;
    @CustomColumn(name = "OrdPat")
    private Long OrdPat;
    @CustomColumn(name = "ClvEst")
    private Integer ClvEst;
    @CustomColumn(name = "NivCur")
    private Long NivCur;
    @CustomColumn(name = "IdEje")
    private Long IdEje;
    @CustomColumn(name = "IdSubEje")
    private Long IdSubEje;
    @CustomColumn(name = "IdPro")
    private Long IdPro;
    @CustomColumn(name = "Prioridad")
    private Long Prioridad;
    @CustomColumn(name = "CvePuesto")
    private Long CvePuesto;
    @CustomColumn(name = "FecIni")
    private Timestamp FecIni;
    @CustomColumn(name = "creditos")
    private Long creditos;
    @CustomColumn(name = "CveNivDL")
    private Long CveNivDL;
    @CustomColumn(name = "cveDepto")
    private Long cveDepto;
    @CustomColumn(name = "cveDeter")
    private Long cveDeter;
    @CustomColumn(name = "cveDistrito")
    private Long cveDistrito;
    @CustomColumn(name = "CveRegion")
    private Long CveRegion;
    @CustomColumn(name = "CveNegocio")
    private Long CveNegocio;
    @CustomColumn(name = "CveZE")
    private Long CveZE;
    @CustomColumn(name = "CvePais")
    private Long CvePais;
    @CustomColumn(name = "Obligatoria")
    private String Obligatoria;

    public Long getIdPC() {
        return IdPC;
    }

    public void setIdPC(Long IdPC) {
        this.IdPC = IdPC;
    }

    public Long getIdPat() {
        return IdPat;
    }

    public void setIdPat(Long IdPat) {
        this.IdPat = IdPat;
    }

    public Long getIdCur() {
        return IdCur;
    }

    public void setIdCur(Long IdCur) {
        this.IdCur = IdCur;
    }

    public Long getConSec() {
        return ConSec;
    }

    public void setConSec(Long ConSec) {
        this.ConSec = ConSec;
    }

    public Long getOrdPat() {
        return OrdPat;
    }

    public void setOrdPat(Long OrdPat) {
        this.OrdPat = OrdPat;
    }

    public Integer getClvEst() {
        return ClvEst;
    }

    public void setClvEst(Integer ClvEst) {
        this.ClvEst = ClvEst;
    }

    public Long getNivCur() {
        return NivCur;
    }

    public void setNivCur(Long NivCur) {
        this.NivCur = NivCur;
    }

    public Long getIdEje() {
        return IdEje;
    }

    public void setIdEje(Long IdEje) {
        this.IdEje = IdEje;
    }

    public Long getIdSubEje() {
        return IdSubEje;
    }

    public void setIdSubEje(Long IdSubEje) {
        this.IdSubEje = IdSubEje;
    }

    public Long getIdPro() {
        return IdPro;
    }

    public void setIdPro(Long IdPro) {
        this.IdPro = IdPro;
    }

    public Long getPrioridad() {
        return Prioridad;
    }

    public void setPrioridad(Long Prioridad) {
        this.Prioridad = Prioridad;
    }

    public Long getCvePuesto() {
        return CvePuesto;
    }

    public void setCvePuesto(Long CvePuesto) {
        this.CvePuesto = CvePuesto;
    }

    public Timestamp getFecIni() {
        return FecIni;
    }

    public void setFecIni(Timestamp FecIni) {
        this.FecIni = FecIni;
    }

    public Long getCreditos() {
        return creditos;
    }

    public void setCreditos(Long creditos) {
        this.creditos = creditos;
    }

    public Long getCveNivDL() {
        return CveNivDL;
    }

    public void setCveNivDL(Long CveNivDL) {
        this.CveNivDL = CveNivDL;
    }

    public Long getCveDepto() {
        return cveDepto;
    }

    public void setCveDepto(Long cveDepto) {
        this.cveDepto = cveDepto;
    }

    public Long getCveDeter() {
        return cveDeter;
    }

    public void setCveDeter(Long cveDeter) {
        this.cveDeter = cveDeter;
    }

    public Long getCveDistrito() {
        return cveDistrito;
    }

    public void setCveDistrito(Long cveDistrito) {
        this.cveDistrito = cveDistrito;
    }

    public Long getCveRegion() {
        return CveRegion;
    }

    public void setCveRegion(Long CveRegion) {
        this.CveRegion = CveRegion;
    }

    public Long getCveNegocio() {
        return CveNegocio;
    }

    public void setCveNegocio(Long CveNegocio) {
        this.CveNegocio = CveNegocio;
    }

    public Long getCveZE() {
        return CveZE;
    }

    public void setCveZE(Long CveZE) {
        this.CveZE = CveZE;
    }

    public Long getCvePais() {
        return CvePais;
    }

    public void setCvePais(Long CvePais) {
        this.CvePais = CvePais;
    }

    public String getObligatoria() {
        return Obligatoria;
    }

    public void setObligatoria(String Obligatoria) {
        this.Obligatoria = Obligatoria;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.IdPC);
        hash = 37 * hash + Objects.hashCode(this.IdPat);
        hash = 37 * hash + Objects.hashCode(this.IdCur);
        hash = 37 * hash + Objects.hashCode(this.ConSec);
        hash = 37 * hash + Objects.hashCode(this.OrdPat);
        hash = 37 * hash + Objects.hashCode(this.ClvEst);
        hash = 37 * hash + Objects.hashCode(this.NivCur);
        hash = 37 * hash + Objects.hashCode(this.IdEje);
        hash = 37 * hash + Objects.hashCode(this.IdSubEje);
        hash = 37 * hash + Objects.hashCode(this.IdPro);
        hash = 37 * hash + Objects.hashCode(this.Prioridad);
        hash = 37 * hash + Objects.hashCode(this.CvePuesto);
        hash = 37 * hash + Objects.hashCode(this.FecIni);
        hash = 37 * hash + Objects.hashCode(this.creditos);
        hash = 37 * hash + Objects.hashCode(this.CveNivDL);
        hash = 37 * hash + Objects.hashCode(this.cveDepto);
        hash = 37 * hash + Objects.hashCode(this.cveDeter);
        hash = 37 * hash + Objects.hashCode(this.cveDistrito);
        hash = 37 * hash + Objects.hashCode(this.CveRegion);
        hash = 37 * hash + Objects.hashCode(this.CveNegocio);
        hash = 37 * hash + Objects.hashCode(this.CveZE);
        hash = 37 * hash + Objects.hashCode(this.CvePais);
        hash = 37 * hash + Objects.hashCode(this.Obligatoria);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RelPatCur other = (RelPatCur) obj;
        if (!Objects.equals(this.Obligatoria, other.Obligatoria)) {
            return false;
        }
        if (!Objects.equals(this.IdPC, other.IdPC)) {
            return false;
        }
        if (!Objects.equals(this.IdPat, other.IdPat)) {
            return false;
        }
        if (!Objects.equals(this.IdCur, other.IdCur)) {
            return false;
        }
        if (!Objects.equals(this.ConSec, other.ConSec)) {
            return false;
        }
        if (!Objects.equals(this.OrdPat, other.OrdPat)) {
            return false;
        }
        if (!Objects.equals(this.ClvEst, other.ClvEst)) {
            return false;
        }
        if (!Objects.equals(this.NivCur, other.NivCur)) {
            return false;
        }
        if (!Objects.equals(this.IdEje, other.IdEje)) {
            return false;
        }
        if (!Objects.equals(this.IdSubEje, other.IdSubEje)) {
            return false;
        }
        if (!Objects.equals(this.IdPro, other.IdPro)) {
            return false;
        }
        if (!Objects.equals(this.Prioridad, other.Prioridad)) {
            return false;
        }
        if (!Objects.equals(this.CvePuesto, other.CvePuesto)) {
            return false;
        }
        if (!Objects.equals(this.FecIni, other.FecIni)) {
            return false;
        }
        if (!Objects.equals(this.creditos, other.creditos)) {
            return false;
        }
        if (!Objects.equals(this.CveNivDL, other.CveNivDL)) {
            return false;
        }
        if (!Objects.equals(this.cveDepto, other.cveDepto)) {
            return false;
        }
        if (!Objects.equals(this.cveDeter, other.cveDeter)) {
            return false;
        }
        if (!Objects.equals(this.cveDistrito, other.cveDistrito)) {
            return false;
        }
        if (!Objects.equals(this.CveRegion, other.CveRegion)) {
            return false;
        }
        if (!Objects.equals(this.CveNegocio, other.CveNegocio)) {
            return false;
        }
        if (!Objects.equals(this.CveZE, other.CveZE)) {
            return false;
        }
        if (!Objects.equals(this.CvePais, other.CvePais)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "RelPatCur{" + "IdPC=" + IdPC + ", IdPat=" + IdPat + ", IdCur=" + IdCur + ", ConSec=" + ConSec + ", OrdPat=" + OrdPat + ", ClvEst=" + ClvEst + ", NivCur=" + NivCur + ", IdEje=" + IdEje + ", IdSubEje=" + IdSubEje + ", IdPro=" + IdPro + ", Prioridad=" + Prioridad + ", CvePuesto=" + CvePuesto + ", FecIni=" + FecIni + ", creditos=" + creditos + ", CveNivDL=" + CveNivDL + ", cveDepto=" + cveDepto + ", cveDeter=" + cveDeter + ", cveDistrito=" + cveDistrito + ", CveRegion=" + CveRegion + ", CveNegocio=" + CveNegocio + ", CveZE=" + CveZE + ", CvePais=" + CvePais + ", Obligatoria=" + Obligatoria + '}';
    }

}
