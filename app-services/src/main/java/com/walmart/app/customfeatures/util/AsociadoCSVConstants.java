/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.util;

/**
 *
 * @author Alan
 */
public class AsociadoCSVConstants {
    
    public static final Integer EMPLOYE_ID=0;
    public static final Integer WIN_NUMBER=1;
    public static final Integer EMAIL=2;
    public static final Integer NOMBRE=3;
    public static final Integer APELLIDO_PATERNO=4;
    public static final Integer APELLIDO_MATERNO=5;
    public static final Integer GENERO=6;
    public static final Integer ANOTHER_EMAIL=7;
    public static final Integer CURP=8;
    public static final Integer CLAVE_EMPRESA_IDC=9;
    public static final Integer DESCIRPCION_COMPANIA=10;
    public static final Integer ESCOLARIDAD=11;
    public static final Integer DOCUMENTO_APROBADO=12;
    public static final Integer INSTITUCION=13;
    public static final Integer FECHA_NACIMIENTO=14;
    public static final Integer PAIS=15;
    public static final Integer LINEA_NEGOCIO_ID=16;
    public static final Integer NOMBRE_LINEA_NEGOCIO=17;
    public static final Integer REGION=18;
    public static final Integer DISTRITO=19;
    public static final Integer DEPTO_ID=20;
    public static final Integer NOMBRE_DEPTO=21;
    public static final Integer DETERMINANTE_ID=22;
    public static final Integer NOMBRE_DETERMINANTE=23;
    public static final Integer TIPO_DETERMINANTE=24;
    public static final Integer CLUSTER_RH=25;
    public static final Integer NIVEL_DL_ID=26;
    public static final Integer NIVEL_DL_DESCRIPCION=27;
    public static final Integer CLASE_EMPLEADO=28;
    public static final Integer PUESTO_ID=29;
    public static final Integer PUESTO_NOMBRE=30;
    public static final Integer CONTRATO_TIPO_EMPLEADO=31;
    public static final Integer ID_JEFE=32;
    public static final Integer FECHA_INGRESO=33;
    public static final Integer FECHA_CONTRATACION=33;
    public static final Integer EVENTO=34;
    public static final Integer FECHA_ULTIMA_MODIFICACION=35;
    public static final Integer EMPLOYE_STATUS=36;
    
    
}
