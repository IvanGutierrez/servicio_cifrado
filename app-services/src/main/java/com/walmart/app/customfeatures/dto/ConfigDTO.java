package com.walmart.app.customfeatures.dto;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import java.io.Serializable;
import java.util.Objects;

@Component
public class ConfigDTO implements Serializable {

    private String host;
    private String user;
    private String password;
    private Integer port;
    private String basePath;

    public ConfigDTO(
            @Value("${sftp.walmart.host}") String host,
            @Value("${sftp.walmart.user}") String user,
            @Value("${sftp.walmart.password}") String password,
            @Value("${sftp.walmart.port}") Integer port,
            @Value("${sftp.walmart.basePath}") String basePath) {
        this.host = host;
        this.user = user;
        this.password = password;
        this.port = port;
        this.basePath = basePath;
    }

    public String getHost() {
        return host;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public Integer getPort() {
        return port;
    }

    public String getBasePath() {
        return basePath;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConfigDTO configDTO = (ConfigDTO) o;
        return Objects.equals(host, configDTO.host) &&
                Objects.equals(user, configDTO.user) &&
                Objects.equals(password, configDTO.password) &&
                Objects.equals(port, configDTO.port) &&
                Objects.equals(basePath, configDTO.basePath);
    }

    @Override
    public int hashCode() {
        return Objects.hash(host, user, password, port, basePath);
    }

    @Override
    public String toString() {
        return "ConfigDTO{" +
                "host='" + host + '\'' +
                ", user='" + user + '\'' +
                ", password='" + password + '\'' +
                ", port=" + port +
                ", basePath='" + basePath + '\'' +
                '}';
    }
}
