package com.walmart.app.customfeatures.business;

import com.jcraft.jsch.*;
import com.walmart.app.customfeatures.dto.ConfigDTO;
import com.walmart.app.customfeatures.util.SSUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

@Service
public class WalmartFTPBusiniess {

    private static final Integer DEFAULT_TIMEOUT = 100000;

    private final ConfigDTO sftpConfig;

    private static Logger log = LoggerFactory.getLogger(WalmartFTPBusiniess.class);
    private final String prefix = "LXP_ASSOCIAT";

    private SSUtils ssUtils;

    public WalmartFTPBusiniess(ConfigDTO sftpConfig) {
        this.sftpConfig = sftpConfig;
    }

    public Path getTemporalFileFromFTP(){
        return this.getTemporalFileFromFTP(null);
    }

    public Path getTemporalFileFromFTP(String file) {
        Session session = null;
        Channel channel = null;
        ChannelSftp channelSftp = null;
        Path temporalFilePath = null;
        try {
            JSch jsch = new JSch();
            session = jsch.getSession(sftpConfig.getUser(), sftpConfig.getHost(), sftpConfig.getPort());
            session.setPassword(sftpConfig.getPassword());
            session.setTimeout(DEFAULT_TIMEOUT);
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            channel = session.openChannel("sftp");
            channel.connect();
            channelSftp = (ChannelSftp) channel;
            channelSftp.cd(sftpConfig.getBasePath());
            temporalFilePath = getFilePath(channelSftp,file);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (channel != null) {
                channel.disconnect();
            }
            if (session != null) {
                session.disconnect();
            }
        }
        return temporalFilePath;
    }

    private Path getFilePath(ChannelSftp channelSftp, String file) throws SftpException, IOException {
        Path temporalFilePath = ssUtils.getFolderPath();
        this.log.info("Temporal path/file to download the file from FTP: {}",temporalFilePath.toAbsolutePath().toString());
        if (file != null) {
            this.log.info("Downloading custom file {}...",file);
            channelSftp.get(sftpConfig.getBasePath().concat(file),temporalFilePath.toAbsolutePath().toString());
            this.log.info("Custom file {} downloaded...",file);
        } else {
            Map<Date, String> filesOnBasePathMap = new TreeMap<>((Date o1, Date o2) -> o2.compareTo(o1));
            Vector filesOnBasePath = channelSftp.ls("*");
            for (Object element : filesOnBasePath) {
                String[] linesLsCommand = element.toString().split(" ");
                String fileName = linesLsCommand[linesLsCommand.length - 1];
                if(fileName.startsWith(prefix)){
                    this.log.info("File found with prefix {} : {} ",prefix,fileName);
                    long modificationTime = channelSftp.lstat(sftpConfig.getBasePath().concat(fileName)).getMTime() * 1000L;
                    filesOnBasePathMap.put(new Date(modificationTime), fileName);
                }
            }
            Map.Entry<Date,String> entry = filesOnBasePathMap.entrySet().stream().findFirst().orElseThrow(()-> new FileNotFoundException("File Not found "+prefix+" based on the last modified"));
            this.log.info("The last file uploaded/modified was {}  with date {}",entry.getValue(),entry.getKey().toString());
            this.log.info("Downloading file {}...",entry.getValue());
            channelSftp.get(sftpConfig.getBasePath().concat(entry.getValue()),temporalFilePath.toAbsolutePath().toString());
            this.log.info("File {} downloaded...",entry.getValue());
        }
        return temporalFilePath;
    }

    @Autowired
    public void setSsUtils(SSUtils ssUtils) {
        this.ssUtils = ssUtils;
    }
}
