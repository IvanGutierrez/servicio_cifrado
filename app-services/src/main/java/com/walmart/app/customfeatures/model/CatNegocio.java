/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.model;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author alan
 */
@CustomTable(name = "CatNegocio")
public class CatNegocio {

    @CustomColumn(name = "CveNegocio")
    private Long CveNegocio;
    @CustomColumn(name = "CveZE")
    private Long CveZE;
    @CustomColumn(name = "NomNegocio")
    private String NomNegocio;
    @CustomColumn(name = "FecAlta")
    private Date FecAlta;
    @CustomColumn(name = "FecUMod")
    private Date FecUMod;
    @CustomColumn(name="SFCveNegocio")
    private String SFCveNegocio;

    public String getSFCveNegocio() {
        return SFCveNegocio;
    }

    public void setSFCveNegocio(String SFCveNegocio) {
        this.SFCveNegocio = SFCveNegocio;
    }

    public Long getCveNegocio() {
        return CveNegocio;
    }

    public void setCveNegocio(Long CveNegocio) {
        this.CveNegocio = CveNegocio;
    }

    public Long getCveZE() {
        return CveZE;
    }

    public void setCveZE(Long CveZE) {
        this.CveZE = CveZE;
    }

    public String getNomNegocio() {
        return NomNegocio;
    }

    public void setNomNegocio(String NomNegocio) {
        this.NomNegocio = NomNegocio;
    }

    public Date getFecAlta() {
        return FecAlta;
    }

    public void setFecAlta(Date FecAlta) {
        this.FecAlta = FecAlta;
    }

    public Date getFecUMod() {
        return FecUMod;
    }

    public void setFecUMod(Date FecUMod) {
        this.FecUMod = FecUMod;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.CveNegocio);
        hash = 97 * hash + Objects.hashCode(this.CveZE);
        hash = 97 * hash + Objects.hashCode(this.NomNegocio);
        hash = 97 * hash + Objects.hashCode(this.FecAlta);
        hash = 97 * hash + Objects.hashCode(this.FecUMod);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CatNegocio other = (CatNegocio) obj;
        if (!Objects.equals(this.NomNegocio, other.NomNegocio)) {
            return false;
        }
        if (!Objects.equals(this.CveNegocio, other.CveNegocio)) {
            return false;
        }
        if (!Objects.equals(this.CveZE, other.CveZE)) {
            return false;
        }
        if (!Objects.equals(this.FecAlta, other.FecAlta)) {
            return false;
        }
        if (!Objects.equals(this.FecUMod, other.FecUMod)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CatNegocio{" + "CveNegocio=" + CveNegocio + ", CveZE=" + CveZE + ", NomNegocio=" + NomNegocio + ", FecAlta=" + FecAlta + ", FecUMod=" + FecUMod + '}';
    }

}
