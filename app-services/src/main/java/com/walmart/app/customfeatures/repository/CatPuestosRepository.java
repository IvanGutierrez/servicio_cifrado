/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.repository;

import com.walmart.app.customfeatures.model.CatPuestos;
import java.util.List;

/**
 *
 * @author Alan
 */
public interface CatPuestosRepository extends GenericRepository{
    
    List<CatPuestos> findByNomPuesto(String nomPuesto);
    
    Long save(CatPuestos catPuestos);
    
}
