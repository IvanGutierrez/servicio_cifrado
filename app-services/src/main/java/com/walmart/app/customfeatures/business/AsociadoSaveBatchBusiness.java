package com.walmart.app.customfeatures.business;

import com.walmart.app.customfeatures.dto.AsociadoResponse;
import com.walmart.app.customfeatures.dto.BitacoraDTO;
import com.walmart.app.customfeatures.enums.PaisEnum;
import com.walmart.app.customfeatures.model.*;
import com.walmart.app.customfeatures.repository.*;
import com.walmart.app.customfeatures.util.ConstantesDefinidasPorElPatronConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import javax.annotation.PostConstruct;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import static com.walmart.app.customfeatures.util.AsociadoCSVConstants.*;

@Service
public class AsociadoSaveBatchBusiness {

    @Autowired
    private AsociadoRepository asociadoRepository;

    @Autowired
    private CatPuestosRepository catPuestosRepository;


    private GenericOperationsRepository genericRepository;

    @Autowired
    private RelAsocWmRepository relAsocWmRepository;

    @Autowired
    private AsociadoCargaMasivaBitacoraRepository asociadoCargaMasivaBitacoraRepository;

    private static final Logger log = LoggerFactory.getLogger(AsociadoSaveBatchBusiness.class);

    private final Map<String, Long> catPuestosMap = Collections.synchronizedMap(new HashMap<>());
    private final Map<String, Long> catNivDLMap = Collections.synchronizedMap(new HashMap<>());
    private final Map<String, Long> catDeptosMap = Collections.synchronizedMap(new HashMap<>());
    private final Map<String, Long> catDeterminanteMap = Collections.synchronizedMap(new HashMap<>());
    private final Map<String, Long> catDistritosMap = Collections.synchronizedMap(new HashMap<>());
    private final Map<String, Long> catRegionesMap = Collections.synchronizedMap(new HashMap<>());
    private final Map<String, Long> catNegocioMap = Collections.synchronizedMap(new HashMap<>());

    private AtomicLong catPuestosKey;
    private AtomicLong catNivDLKey;
    private AtomicLong catDeptosKey;
    private AtomicLong catDeterminanteKey;
    private AtomicLong catDistritosKey;
    private AtomicLong catRegionesKey;
    private AtomicLong catNegocioKey;

    private final Integer minColumnsOnCsv = 37;

    @PostConstruct
    public void init(){
        initCollectionsMap();
        initCatKeys();
    }

    private void initCatKeys(){
        this.log.info("++++++++++++ Init Keys +++++++++++");
        this.catNegocioKey = new AtomicLong(this.genericRepository.getLastIdReflection("CveNegocio",Long.class,CatNegocio.class) +1);
        this.catRegionesKey = new AtomicLong(this.genericRepository.getLastIdReflection("CveRegion",Long.class,CatRegiones.class) +1);
        this.catDistritosKey = new AtomicLong(this.genericRepository.getLastIdReflection("cveDistrito",Long.class,CatDistritos.class) +1);
        this.catDeterminanteKey = new AtomicLong(this.genericRepository.getLastIdReflection("cveDeter",Long.class,CatDeterminante.class) +1);
        this.catDeptosKey = new AtomicLong(this.genericRepository.getLastIdReflection("cveDepto",Long.class,CatDeptos.class) +1);
        this.catNivDLKey = new AtomicLong(this.genericRepository.getLastIdReflection("CveNivDL",Long.class,CatNivDL.class) +1);
        this.catPuestosKey = new AtomicLong(this.genericRepository.getLastIdReflection("CvePuesto",Long.class,CatPuestos.class) +1);
        this.log.info("++++++++++++ End Init Keys +++++++++++");
    }

    private void initCollectionsMap(){
        this.log.info("*********** Inicializando Catalogos *******************");
        this.genericRepository.findAll(CatNegocio.class).forEach(element ->{
            this.catNegocioMap.put(element.getNomNegocio(),element.getCveNegocio());
        });
        this.genericRepository.findAll(CatRegiones.class).forEach(element ->{
            this.catRegionesMap.put(element.getNomRegion(),element.getCveRegion());
        });
        this.genericRepository.findAll(CatDistritos.class).forEach(element ->{
            this.catDistritosMap.put(element.getNomDist(),element.getCveDistrito());
        });
        this.genericRepository.findAll(CatDeterminante.class).forEach(element ->{
            this.catDeterminanteMap.put(element.getNomDeter(),element.getCveDeter());
        });
        this.genericRepository.findAll(CatDeptos.class).forEach(element ->{
            this.catDeptosMap.put(element.getNomDepto(),element.getCveDepto());
        });
        this.genericRepository.findAll(CatNivDL.class).forEach(element ->{
            this.catNivDLMap.put(element.getNomNivDL(),element.getCveNivDL());
        });
        this.genericRepository.findAll(CatPuestos.class).forEach(element ->{
            this.catPuestosMap.put(element.getNomPuesto(),element.getCvePuesto());
        });
        this.log.info("*********** End Inicializando Catalogos *******************");
    }


    public void saveOrUpdateAsociadoBatch(final String newLineOnThread,final String separator) {
        AsociadoResponse asociadoResponse = new AsociadoResponse();
        try {
            final Asociado asociado = getAsociadoFromCsvLine(newLineOnThread,separator);
            asociadoResponse.setUsr(asociado.getUsr());
            asociadoResponse.setInsertOk(Boolean.TRUE);
            asociadoResponse.setIdAsoc(asociado.getIdAsoc());
        } catch (Exception e) {
            e.printStackTrace();
            this.log.error("Error at the moment to insert {} error: {}", asociadoResponse.getUsr());
            saveChangesOnBitacora(new StringBuilder("CSV Line: [").append(newLineOnThread).append("] , Error Message : ").append(e.getMessage()).toString());
        }

    }

    private Asociado getAsociadoFromCsvLine(final String line,final String separator) {
        Integer terDefaultValue = 1;//Quien sabe que sea ter = 1, pero así nacen todos los usuarios en el store procedure
        String[] csvColumns = line.split(separator.equals("|") ? "\\|" : separator);
        if(csvColumns.length>minColumnsOnCsv){
            this.log.info("xxxxxxx The line with THE NO_ASOC {} has more tan 37 columns, it will be ignored xxxxxxx", csvColumns[0]);
            return new Asociado(-1l);
        }
        Integer userEstatusActivated = tryToGetStatus(csvColumns);
        final Asociado newAsociado = new Asociado(terDefaultValue, userEstatusActivated);
        fillAsociado(newAsociado, csvColumns);
        List<Asociado> alreadyOnDB = this.asociadoRepository.findByUsr(newAsociado.getNoAsoc());
        if (!alreadyOnDB.isEmpty()) {
            updateInformationOfExistingUser(alreadyOnDB.stream().findFirst().get(), csvColumns, newAsociado);
            return newAsociado;
        }

        final RelAsocWm relAsocWm = fillRelAsocWm(csvColumns);

        newAsociado.setCvePuesto(relAsocWm.getCvePuesto());

        final Long idAsoc = this.asociadoRepository.save(newAsociado);
        newAsociado.setIdAsoc(idAsoc);
        relAsocWm.setIdAsoc(idAsoc);
        this.genericRepository.save(relAsocWm, RelAsocWm.class);
        return newAsociado;
    }

    private Integer tryToGetStatus(String[] csvColumns) {
        Integer defaultEstatus = -1;
        if (csvColumns.length >= minColumnsOnCsv) {
            String employeStatusColumnValue = getTrimmedValue(csvColumns[EMPLOYE_STATUS]);
            if (employeStatusColumnValue != null) {
                String employeStatusColumnValueUpperCase = employeStatusColumnValue.toUpperCase();
                switch (employeStatusColumnValueUpperCase) {
                    case "ACTIVO":
                        defaultEstatus = 1;
                        break;
                    case "INACTIVO":
                        defaultEstatus = 2;
                        break;
                    default:
                        defaultEstatus = 0;
                        break;
                }
            }
        }
        return defaultEstatus;
    }

    private void fillAsociado(Asociado as, String[] csvColumns) {
        PaisEnum paisEnum = PaisEnum.getInstance(getPaisId(csvColumns[PAIS]));
        String usr = csvColumns[EMAIL].trim().replaceAll("\\s", "").concat(paisEnum.getEmailDomain());
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        as.setUsr(usr);
        as.setNoAsoc(completarNumeroAsociado(csvColumns,getTrimmedValue(csvColumns[EMPLOYE_ID])));
        as.setNom(getTrimmedValue(csvColumns[NOMBRE]));
        as.setApa(getTrimmedValue(csvColumns[APELLIDO_PATERNO]));
        as.setAma(getTrimmedValue(csvColumns[APELLIDO_MATERNO]));
        as.setGen(getGenero(csvColumns[GENERO]));
        as.setEma(as.getUsr());
        as.setWinNumber(getTrimmedLongValue(csvColumns[WIN_NUMBER]));
        as.setSupervisor(getTrimmedLongValue(csvColumns[ID_JEFE]));
        try {
            DateFormat dateFormatPsw = new SimpleDateFormat("yyyy-MM-dd");
            as.setPswCad(dateFormatPsw.parse("2050-12-31"));
        } catch (Exception e) {
            //this.log.error("Error at the moment to parse the date PswCad: "+e.getMessage());
        }
        try {
            as.setFecNac(dateFormat.parse(getTrimmedValue(csvColumns[FECHA_NACIMIENTO])));
        } catch (Exception e) {
            //this.log.error("Error at the moment to parse the date FecNac: "+e.getMessage());
        }
        try {
            as.setFecIng(dateFormat.parse(getTrimmedValue(csvColumns[FECHA_INGRESO])));
        } catch (Exception e) {
            //this.log.error("Error at the moment to parse the date FecIng: "+e.getMessage());
        }
        try {
            as.setFecAct(dateFormat.parse(getTrimmedValue(csvColumns[FECHA_ULTIMA_MODIFICACION])));
        } catch (Exception e) {
            //this.log.error("Error at the moment to parse the date FecAct: "+e.getMessage());
        }

    }

    public String completarNumeroAsociado(String[] csvColumns,String noAsociado){
        Integer numeroMinimoCaracteresNoAsociado = 7;
        if(noAsociado.length()  >= numeroMinimoCaracteresNoAsociado){
            return noAsociado;
        }
        StringBuilder cadenaConCerosBuilder = new StringBuilder();
        Integer totalCerosIzquierda = numeroMinimoCaracteresNoAsociado - noAsociado.length();
        for (int counter = 0 ; counter < totalCerosIzquierda;counter++){
            cadenaConCerosBuilder.append("0");
        }
        return cadenaConCerosBuilder.toString().concat(noAsociado);
    }


    private String getGenero(String genero) {
        return genero != null && genero.length() > 0 ? genero.substring(0, 1) : "X";
    }

    private String getTrimmedValue(String value) {
        return value != null ? value.trim() : null;
    }

    private Long getTrimmedLongValue(String value) {
        return value != null ? Long.valueOf(value.trim()) : 0L;
    }

    private RelAsocWm fillRelAsocWm(String[] csvColumns) {
        final RelAsocWm relAsocWm = new RelAsocWm();
        final Long paisDefaultId = getPaisId(csvColumns[PAIS]);
        final Long zonaEconomicaDefaultId = ConstantesDefinidasPorElPatronConstants.ZONA_ECONOMICA_DEFAULT_ID;
        final Long negocioId = saveNegocio(getCatDescriptionFromColumnsArray(csvColumns[LINEA_NEGOCIO_ID], csvColumns[NOMBRE_LINEA_NEGOCIO]), zonaEconomicaDefaultId,getTrimmedValue(csvColumns[LINEA_NEGOCIO_ID]));
        final Long regionId = saveRegion(getCatDescriptionFromColumnsArray(csvColumns[REGION]), negocioId);
        final Long distritoId = saveDistrito(getCatDescriptionFromColumnsArray(csvColumns[DISTRITO]), regionId);
        final Long determinanteId = saveDeterminante(getCatDescriptionFromColumnsArray(csvColumns[DETERMINANTE_ID], csvColumns[NOMBRE_DETERMINANTE]), distritoId,getTrimmedValue(csvColumns[DETERMINANTE_ID]));
        final Long deptoId = saveDepartamento(getCatDescriptionFromColumnsArray(csvColumns[DEPTO_ID], csvColumns[NOMBRE_DEPTO]), determinanteId,getTrimmedValue(csvColumns[DEPTO_ID]));
        final Long nivelDL = saveNivelDL(getCatDescriptionFromColumnsArray(csvColumns[NIVEL_DL_ID], csvColumns[NIVEL_DL_DESCRIPCION]), deptoId);
        final Long puestoId = savePuesto(getCatDescriptionFromColumnsArray(csvColumns[PUESTO_ID], csvColumns[PUESTO_NOMBRE]), nivelDL,getTrimmedValue(csvColumns[PUESTO_ID]));

        relAsocWm.setCvePuesto(puestoId);
        relAsocWm.setCveNivDL(nivelDL);
        relAsocWm.setCveDepto(deptoId);
        relAsocWm.setCveDeter(determinanteId);
        relAsocWm.setCveDistrito(distritoId);
        relAsocWm.setCveRegion(regionId);
        relAsocWm.setCveNegocio(negocioId);
        relAsocWm.setCveZE(zonaEconomicaDefaultId);
        relAsocWm.setCvePais(paisDefaultId);

        return relAsocWm;
    }

    private Long getPaisId(String paisDesc){
        Long paisId = PaisEnum.MEXICO.getPaisId();
        if(paisDesc != null){
            paisDesc = paisDesc.trim();
            List<CatPais> result = this.genericRepository.findByCustomDesc("NomPais",paisDesc,CatPais.class);
            if(!result.isEmpty()){
                paisId = result.stream().findFirst().get().getCvePais();
            }
        }
        return paisId;
    }

    private String getCatDescriptionFromColumnsArray(String... descriptions) {
        return Arrays.asList(descriptions).stream().map(description -> description.trim()).collect(Collectors.joining("-"));
    }

    private Long updateInformationOfExistingUser(Asociado asociadoAlreadyOnDB, String[] csvColumns, Asociado newAsociado) {
        final RelAsocWm relAsocWm = fillRelAsocWm(csvColumns);
        relAsocWm.setIdAsoc(asociadoAlreadyOnDB.getIdAsoc());
        List<String> changesRelAsocWm = tryToUpdateRelAsocWm(relAsocWm);
        newAsociado.setCvePuesto(relAsocWm.getCvePuesto());
        List<String> changesAsociado = tryToUpdateAsociado(asociadoAlreadyOnDB, newAsociado, relAsocWm);
        if (!changesAsociado.isEmpty() || !changesRelAsocWm.isEmpty()) {
            saveChangesOnBitacora(newAsociado,changesRelAsocWm,changesAsociado);
        }
        return asociadoAlreadyOnDB.getIdAsoc();
    }

    private void saveChangesOnBitacora(Asociado newAsociado,List<String> changesRelAsocWm,List<String> changesAsociado){
        BitacoraDTO bitacoraDTO = new BitacoraDTO();
        bitacoraDTO.setEmail(newAsociado.getEma());
        bitacoraDTO.setNoAsociado(newAsociado.getNoAsoc());
        bitacoraDTO.setAsociadosChanges(changesAsociado);
        bitacoraDTO.setRelAsocWmChanges(changesRelAsocWm);
        this.asociadoCargaMasivaBitacoraRepository.save(bitacoraDTO);
    }

    private void saveChangesOnBitacora(String errorMessage){
        this.asociadoCargaMasivaBitacoraRepository.save(new BitacoraDTO(errorMessage));
    }

    private List<String> tryToUpdateRelAsocWm(RelAsocWm relAsocWmNew) {
        List<String> relAsocWmChanges = new ArrayList<>();
        Optional<RelAsocWm> relAsocWmAlreadyOnDbOptional = this.relAsocWmRepository.findByIdAsoc(relAsocWmNew.getIdAsoc());
        if(!relAsocWmAlreadyOnDbOptional.isPresent()){
            this.log.info("--> {} not present for the user {}, creating ",relAsocWmNew.toString(),relAsocWmNew.getIdAsoc());
            this.genericRepository.save(relAsocWmNew, RelAsocWm.class);
            relAsocWmChanges.add("New "+relAsocWmNew.toString());
        }else if (!relAsocWmAlreadyOnDbOptional.get().equals(relAsocWmNew)) {
            this.relAsocWmRepository.updateByIdAsoc(relAsocWmNew);
            relAsocWmChanges.addAll(getRelAsocWmChanges(relAsocWmAlreadyOnDbOptional.get(), relAsocWmNew));
        }
        return relAsocWmChanges;
    }

    private List<String> tryToUpdateAsociado(Asociado asociadoAlreadyOnDB, Asociado newAsociado, RelAsocWm relAsocWm) {
        List<String> asociadoChangesList = new ArrayList<>();
        newAsociado.setIdAsoc(asociadoAlreadyOnDB.getIdAsoc());
        if (!asociadoAlreadyOnDB.equals(newAsociado)) {
            this.asociadoRepository.updateAsociadoById(newAsociado);
            asociadoChangesList.addAll(getAsociadoChanges(asociadoAlreadyOnDB, newAsociado));
        }
        if (!asociadoAlreadyOnDB.getCvePuesto().equals(relAsocWm.getCvePuesto())) {
            this.asociadoRepository.updateCvePuestoById(asociadoAlreadyOnDB, relAsocWm.getCvePuesto());
            asociadoChangesList.add(String.format("[%s] change to [%s]", "CvePuesto", relAsocWm.getCvePuesto()));
        }
        return asociadoChangesList;
    }

    public List<String> getAsociadoChanges(Asociado oldAsociado, Asociado newAsociado) {
        List<String> changesList = new ArrayList<>();
        Map<String, Object> oldAsociadoValuesMap = new HashMap<>();
        Arrays.stream(oldAsociado.getClass().getDeclaredFields())
                .filter(this::filterAsociadoFields)
                .forEach(field -> {
                    fillOldAsociadoValuesMap(field, oldAsociado, oldAsociadoValuesMap);
                });
        Arrays.stream(newAsociado.getClass().getDeclaredFields())
                .filter(this::filterAsociadoFields)
                .forEach(field -> {
                    String key = field.getDeclaredAnnotation(CustomColumn.class).name();
                    Object newValue = getFieldValue(field, newAsociado);
                    Object oldValue = oldAsociadoValuesMap.get(key);
                    if (!Objects.equals(oldValue, newValue)) {
                        changesList.add(String.format("[%s]::(%s) change to (%s)", key, oldValue, newValue));
                    }
                });
        return changesList;
    }

    private void fillOldAsociadoValuesMap(Field field, Asociado asociadoAlreadyOnDB, Map<String, Object> oldAsociadoValuesMap) {
        String keyMap = field.getDeclaredAnnotation(CustomColumn.class).name();
        Object value = getFieldValue(field, asociadoAlreadyOnDB);
        oldAsociadoValuesMap.put(keyMap, value);
    }

    private List<String> getRelAsocWmChanges(RelAsocWm oldRelAsocWm, RelAsocWm newRelAsocWm) {
        List<String> changesList = new ArrayList<>();
        Map<String, Object> oldRelAsocWmValuesMap = new HashMap<>();
        Arrays.stream(oldRelAsocWm.getClass().getDeclaredFields())
                .filter(this::filterRelAsocWmFields)
                .forEach(field -> {
                    fillOldRelAsocWmValuesMap(field, oldRelAsocWm, oldRelAsocWmValuesMap);
                });
        Arrays.stream(newRelAsocWm.getClass().getDeclaredFields())
                .filter(this::filterRelAsocWmFields)
                .forEach(field -> {
                    String key = field.getDeclaredAnnotation(CustomColumn.class).name();
                    Object newValue = getFieldValue(field, newRelAsocWm);
                    Object oldValue = oldRelAsocWmValuesMap.get(key);
                    if (!Objects.equals(oldValue, newValue)) {
                        changesList.add(String.format("[%s]::(%s) change to (%s)", key, oldValue, newValue));
                    }
                });
        return changesList;
    }

    private void fillOldRelAsocWmValuesMap(Field field, RelAsocWm asociadoAlreadyOnDB, Map<String, Object> oldRelAsocWmValuesMap) {
        String keyMap = field.getDeclaredAnnotation(CustomColumn.class).name();
        Object value = getFieldValue(field, asociadoAlreadyOnDB);
        oldRelAsocWmValuesMap.put(keyMap, value);
    }

    private Object getFieldValue(Field field, Object object) {
        field.setAccessible(true);
        Object value = null;
        try {
            value = field.get(object);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            this.log.error(e.getMessage());
        }
        return value;
    }

    private Long savePuesto(String columnValue, Long nextRelationId, String clave) {
        Long id = this.catPuestosMap.get(columnValue);
        if (id == null) {
            List<CatPuestos> catPuestosList = this.catPuestosRepository.findByNomPuesto(columnValue);
            if (catPuestosList.isEmpty()) {
                Long nextId = this.catPuestosKey.getAndIncrement();
                CatPuestos catPuestos = new CatPuestos();
                catPuestos.setCveNivDL(nextRelationId);
                catPuestos.setFecAlta(new Date());
                catPuestos.setFechUMod(new Date());
                catPuestos.setNomPuesto(columnValue);
                catPuestos.setCvePuesto(nextId);
                catPuestos.setSFCvePuesto(clave);

                this.catPuestosRepository.save(catPuestos);
                this.catPuestosMap.put(columnValue, catPuestos.getCvePuesto());
                return catPuestos.getCvePuesto();
            } else {
                Long cvePuestoIdOnDb = catPuestosList.stream().findFirst().get().getCvePuesto();
                this.catPuestosMap.put(columnValue, cvePuestoIdOnDb);
                return cvePuestoIdOnDb;
            }
        }
        return id;
    }

    private Long saveNegocio(String columnValue, final Long nextRelationId, String clave) {
        Long id = this.catNegocioMap.get(columnValue);
        final String columnNameDesc = "NomNegocio";
        final String columnIdDesc = "CveNegocio";
        final Class clazz = CatNegocio.class;
        if (id == null) {
            List<CatNegocio> catGenericList = this.genericRepository.findByCustomDesc(columnNameDesc, columnValue, clazz);
            if (catGenericList.isEmpty()) {
                Long nextId = this.catNegocioKey.getAndIncrement();
                CatNegocio catalogo = new CatNegocio();
                catalogo.setFecAlta(new Date());
                catalogo.setFecUMod(new Date());
                catalogo.setNomNegocio(columnValue);
                catalogo.setCveZE(nextRelationId);
                catalogo.setCveNegocio(nextId);
                catalogo.setSFCveNegocio(clave);
                System.out.println("Salvando la nueva clave "+catalogo.getSFCveNegocio());
                this.genericRepository.save(catalogo, clazz);
                this.catNegocioMap.put(columnValue, nextId);

                id = nextId;
            } else {
                Long idOnDb = catGenericList.stream().findFirst().get().getCveNegocio();
                this.catNegocioMap.put(columnValue, idOnDb);
                id = idOnDb;
            }
        }
        return id;
    }

    private Long saveRegion(String columnValue, Long nextRelationId) {
        Long id = this.catRegionesMap.get(columnValue);
        final String columnNameDesc = "NomRegion";
        final String columnIdDesc = "CveRegion";
        final Class clazz = CatRegiones.class;
        if (id == null) {
            List<CatRegiones> catGenericList = this.genericRepository.findByCustomDesc(columnNameDesc, columnValue, clazz);
            if (catGenericList.isEmpty()) {
                Long nextId = this.catRegionesKey.getAndIncrement();
                CatRegiones catalogo = new CatRegiones();
                catalogo.setFecAlta(new Date());
                catalogo.setFecUMod(new Date());
                catalogo.setNomRegion(columnValue);
                catalogo.setCveNegocio(nextRelationId);
                catalogo.setCveRegion(nextId);

                this.genericRepository.save(catalogo, clazz);
                this.catRegionesMap.put(columnValue, nextId);

                id = nextId;
            } else {
                Long idOnDb = catGenericList.stream().findFirst().get().getCveRegion();
                this.catRegionesMap.put(columnValue, idOnDb);
                id = idOnDb;
            }
        }
        return id;
    }

    private Long saveDistrito(String columnValue, Long nextRelationId) {
        Long id = this.catDistritosMap.get(columnValue);
        final String columnNameDesc = "NomDist";
        final String columnIdDesc = "cveDistrito";
        final Class clazz = CatDistritos.class;
        if (id == null) {
            List<CatDistritos> catGenericList = this.genericRepository.findByCustomDesc(columnNameDesc, columnValue, clazz);
            if (catGenericList.isEmpty()) {
                Long nextId = this.catDistritosKey.getAndIncrement();
                CatDistritos catalogo = new CatDistritos();
                catalogo.setFecAlta(new Date());
                catalogo.setFecUMod(new Date());
                catalogo.setNomDist(columnValue);
                catalogo.setCveRegion(nextRelationId);
                catalogo.setCveDistrito(nextId);

                this.genericRepository.save(catalogo, clazz);
                this.catDistritosMap.put(columnValue, nextId);

                id = nextId;
            } else {
                Long idOnDb = catGenericList.stream().findFirst().get().getCveDistrito();
                this.catDistritosMap.put(columnValue, idOnDb);
                id = idOnDb;
            }
        }
        return id;
    }

    private Long saveDeterminante(String columnValue, Long nextRelationId,String clave) {
        Long id = this.catDeterminanteMap.get(columnValue);
        final String columnNameDesc = "NomDeter";
        final String columnIdDesc = "cveDeter";
        final Class clazz = CatDeterminante.class;
        if (id == null) {
            List<CatDeterminante> catGenericList = this.genericRepository.findByCustomDesc(columnNameDesc, columnValue, clazz);
            if (catGenericList.isEmpty()) {
                Long nextId = this.catDeterminanteKey.getAndIncrement();
                CatDeterminante catalogo = new CatDeterminante();
                catalogo.setFecAlta(new Date());
                catalogo.setFecUMod(new Date());
                catalogo.setNomDeter(columnValue);
                catalogo.setCveDistrito(nextRelationId);
                catalogo.setCveDeter(nextId);
                catalogo.setSFCveDeter(clave);

                this.genericRepository.save(catalogo, clazz);
                this.catDeterminanteMap.put(columnValue, nextId);

                id = nextId;
            } else {
                Long idOnDb = catGenericList.stream().findFirst().get().getCveDeter();
                this.catDeterminanteMap.put(columnValue, idOnDb);
                id = idOnDb;
            }
        }
        return id;
    }

    private Long saveDepartamento(String columnValue, Long nextRelationId,String clave) {
        Long id = this.catDeptosMap.get(columnValue);
        final String columnNameDesc = "NomDepto";
        final String columnIdDesc = "cveDepto";
        final Class clazz = CatDeptos.class;
        if (id == null) {
            List<CatDeptos> catGenericList = this.genericRepository.findByCustomDesc(columnNameDesc, columnValue, clazz);
            if (catGenericList.isEmpty()) {
                Long nextId = this.catDeptosKey.getAndIncrement();
                CatDeptos catalogo = new CatDeptos();
                catalogo.setFecAlta(new Date());
                catalogo.setFecUMod(new Date());
                catalogo.setNomDepto(columnValue);
                catalogo.setCveDeter(nextRelationId);
                catalogo.setCveDepto(nextId);
                catalogo.setSFCveDepto(clave);

                this.genericRepository.save(catalogo, clazz);
                this.catDeptosMap.put(columnValue, nextId);

                id = nextId;
            } else {
                Long idOnDb = catGenericList.stream().findFirst().get().getCveDepto();
                this.catDeptosMap.put(columnValue, idOnDb);
                id = idOnDb;
            }
        }
        return id;
    }

    private Long saveNivelDL(String columnValue, Long nextRelationId) {
        Long id = this.catNivDLMap.get(columnValue);
        final String columnNameDesc = "NomNivDL";
        final String columnIdDesc = "CveNivDL";
        final Class clazz = CatNivDL.class;
        if (id == null) {
            List<CatNivDL> catGenericList = this.genericRepository.findByCustomDesc(columnNameDesc, columnValue, clazz);
            if (catGenericList.isEmpty()) {
                Long nextId = this.catNivDLKey.getAndIncrement();
                CatNivDL catalogo = new CatNivDL();
                catalogo.setFecAlta(new Date());
                catalogo.setFecUMod(new Date());
                catalogo.setNomNivDL(columnValue);
                catalogo.setCveDepto(nextRelationId);
                catalogo.setCveNivDL(nextId);


                this.genericRepository.save(catalogo, clazz);
                this.catNivDLMap.put(columnValue, nextId);

                id = nextId;
            } else {
                Long idOnDb = catGenericList.stream().findFirst().get().getCveNivDL();
                this.catNivDLMap.put(columnValue, idOnDb);
                id = idOnDb;
            }
        }
        return id;
    }

    private boolean filterAsociadoFields(Field field) {
        CustomColumn customColumn = field.getDeclaredAnnotation(CustomColumn.class);
        boolean isAnnotationPresent = customColumn != null;
        boolean fieldIsAllowed = true;
        if (isAnnotationPresent && field.getType().equals(Date.class)) {
            String name = customColumn.name();
            fieldIsAllowed = Arrays.asList("FecIng", "FecAct").stream().filter(element -> element.equals(name)).findAny().isPresent();
        }
        return isAnnotationPresent && fieldIsAllowed;
    }

    private boolean filterRelAsocWmFields(Field field) {
        CustomColumn customColumn = field.getDeclaredAnnotation(CustomColumn.class);
        return customColumn != null;
    }

    @Autowired
    public void setGenericRepository(GenericOperationsRepository genericRepository) {
        this.genericRepository = genericRepository;
    }
}
