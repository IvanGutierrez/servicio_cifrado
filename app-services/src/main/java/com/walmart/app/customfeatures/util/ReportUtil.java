/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Alan
 */
public class ReportUtil {

    public static String getQueryFromResource(String path, String fileName) throws IOException {
        StringBuilder completePath = new StringBuilder();
        final String customSlash = "/";
        completePath.append(path.endsWith(customSlash) ? path : path.concat(customSlash)).append(fileName.endsWith(".sql") ? fileName : fileName.concat(".sql"));
        List<String> allLines = Files.readAllLines(new File(completePath.toString()).toPath());
        return allLines.stream().filter(line -> !line.trim().isEmpty()).collect(Collectors.joining(" "));
    }

    public static String getRowFromMap(Map<String, Object> map,Boolean addEndLine) {
        StringBuilder rowBuilder = new StringBuilder();
        map.entrySet().stream().forEach(entry -> {
            String value = entry.getValue() != null ? entry.getValue().toString() : "";
            rowBuilder.append("\"");
            rowBuilder.append(value.trim().replaceAll("\"", "'"));
            rowBuilder.append("\"");
            rowBuilder.append(",");
        });
        String row = rowBuilder.substring(0, rowBuilder.length() - 1);
        return addEndLine ? row.concat("\n") : row;
    }

    public static String getRowFromMap(Map<String, Object> map){
        return getRowFromMap(map,Boolean.FALSE);
    }

}
