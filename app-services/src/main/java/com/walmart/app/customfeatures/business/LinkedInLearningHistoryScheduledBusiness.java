package com.walmart.app.customfeatures.business;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class LinkedInLearningHistoryScheduledBusiness {

    private LinkedInLearningHistoryBusiness linkedInLearningHistoryBusiness;

    @Value("${procesos-masivos.linkedin-learning-enabled}")
    private boolean linkedinLearningEnabled;

    public LinkedInLearningHistoryScheduledBusiness(LinkedInLearningHistoryBusiness linkedInLearningHistoryBusiness) {
        this.linkedInLearningHistoryBusiness = linkedInLearningHistoryBusiness;
    }

    @Scheduled(cron = "${procesos-masivos.cron.linkedin-learning}", zone = "Mexico/General")
    public void executeLinkedInReport() {
        if (linkedinLearningEnabled) {
            this.linkedInLearningHistoryBusiness.getLinkedInReportFromLinkedInApiREST();
        }
    }

}
