/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.repository;

import com.walmart.app.customfeatures.model.RelAsocWm;

import java.util.Optional;

/**
 *
 * @author alan
 */
public interface RelAsocWmRepository extends GenericRepository{
    
    int updateByIdAsoc(RelAsocWm relAsocWm);

    Optional<RelAsocWm> findByIdAsoc(Long IdAsoc);
    
}
