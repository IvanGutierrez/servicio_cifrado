/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.repository.impl;

import com.walmart.app.customfeatures.model.RelAsocWm;
import com.walmart.app.customfeatures.repository.RelAsocWmRepository;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;


/**
 *
 * @author alan
 */
@Repository
public class RelAsocWmRepositoryImpl implements RelAsocWmRepository{
    
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    
    @Override
    public int updateByIdAsoc(RelAsocWm relAsocWm){
        final String query = "UPDATE RelAsocWm SET CvePais = :CvePais, CveZE = :CveZE ,CveNegocio = :CveNegocio, CveRegion =:CveRegion, cveDistrito = :cveDistrito, cveDeter = :cveDeter, cveDepto = :cveDepto, CveNivDL = :CveNivDL, CvePuesto = :CvePuesto WHERE IdAsoc = :IdAsoc";
        Map<String,Object> params = new HashMap<>();
        params.put("CvePais", relAsocWm.getCvePais());
        params.put("CveZE", relAsocWm.getCveZE());
        params.put("CveNegocio", relAsocWm.getCveNegocio());
        params.put("CveRegion", relAsocWm.getCveRegion());
        params.put("cveDistrito", relAsocWm.getCveDistrito());
        params.put("cveDeter", relAsocWm.getCveDeter());
        params.put("cveDepto", relAsocWm.getCveDepto());
        params.put("CveNivDL", relAsocWm.getCveNivDL());
        params.put("CvePuesto", relAsocWm.getCvePuesto());
        params.put("IdAsoc", relAsocWm.getIdAsoc());
        return this.namedParameterJdbcTemplate.update(query, params);
    }
    
    @Override
    public Optional<RelAsocWm> findByIdAsoc(Long IdAsoc){
        final String query = "SELECT * FROM RelAsocWm WHERE IdAsoc = :IdAsoc";
        Map<String,Object> params = new HashMap<>();
        params.put("IdAsoc", IdAsoc);
        List<RelAsocWm> result = this.namedParameterJdbcTemplate.query(query,params,new BeanPropertyRowMapper<>(RelAsocWm.class));
        return result.stream().findFirst();
    }
    
    @Autowired
    @Override
    public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }
    
}
