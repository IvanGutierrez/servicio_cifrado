/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.repository;

import java.util.List;
import java.util.Map;

/**
 *
 * @author alan
 */
public interface PathsRepository extends GenericRepository {

    List<Map<String, Object>> findKeyRutaFormacion(Map<String, Long> params);
    
    List<Map<String, Object>> findCursoAndPatIdOnRelPathCur(Object idPat);

}
