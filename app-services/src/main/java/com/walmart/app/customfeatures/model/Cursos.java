/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.model;

import java.sql.Timestamp;
import java.util.Objects;

/**
 *
 * @author alan
 */
@CustomTable(name = "Cursos")
public class Cursos {

    @CustomColumn(name = "IdCur")
    private Long IdCur;
    @CustomColumn(name = "NomCur")
    private String NomCur;
    @CustomColumn(name = "Des")
    private String Des;
    @CustomColumn(name = "DesCor")
    private String DesCor;
    @CustomColumn(name = "IdCat")
    private Long IdCat;
    @CustomColumn(name = "IdSCat")
    private Long IdSCat;
    @CustomColumn(name = "NRecu")
    private Long NRecu;
    @CustomColumn(name = "DurCur")
    private String DurCur;
    @CustomColumn(name = "IdAut")
    private Long IdAut;
    @CustomColumn(name = "FecIng")
    private Timestamp FecIng;
    @CustomColumn(name = "FecUMod")
    private Timestamp FecUMod;
    @CustomColumn(name = "ReqCur")
    private String ReqCur;
    @CustomColumn(name = "Que")
    private String Que;
    @CustomColumn(name = "ImgLog")
    private String ImgLog;
    @CustomColumn(name = "CurDes")
    private Long CurDes;
    @CustomColumn(name = "ClvEst")
    private Integer ClvEst;
    @CustomColumn(name = "IdTCur")
    private Long IdTCur;
    @CustomColumn(name = "IdflImg")
    private String IdflImg;
    @CustomColumn(name = "IdRef")
    private String IdRef;
    @CustomColumn(name = "Eve")
    private Integer Eve;

    public Long getIdCur() {
        return IdCur;
    }

    public void setIdCur(Long IdCur) {
        this.IdCur = IdCur;
    }

    public String getNomCur() {
        return NomCur;
    }

    public void setNomCur(String NomCur) {
        this.NomCur = NomCur;
    }

    public String getDes() {
        return Des;
    }

    public void setDes(String Des) {
        this.Des = Des;
    }

    public String getDesCor() {
        return DesCor;
    }

    public void setDesCor(String DesCor) {
        this.DesCor = DesCor;
    }

    public Long getIdCat() {
        return IdCat;
    }

    public void setIdCat(Long IdCat) {
        this.IdCat = IdCat;
    }

    public Long getIdSCat() {
        return IdSCat;
    }

    public void setIdSCat(Long IdSCat) {
        this.IdSCat = IdSCat;
    }

    public Long getNRecu() {
        return NRecu;
    }

    public void setNRecu(Long NRecu) {
        this.NRecu = NRecu;
    }

    public String getDurCur() {
        return DurCur;
    }

    public void setDurCur(String DurCur) {
        this.DurCur = DurCur;
    }

    public Long getIdAut() {
        return IdAut;
    }

    public void setIdAut(Long IdAut) {
        this.IdAut = IdAut;
    }

    public Timestamp getFecIng() {
        return FecIng;
    }

    public void setFecIng(Timestamp FecIng) {
        this.FecIng = FecIng;
    }

    public Timestamp getFecUMod() {
        return FecUMod;
    }

    public void setFecUMod(Timestamp FecUMod) {
        this.FecUMod = FecUMod;
    }

    public String getReqCur() {
        return ReqCur;
    }

    public void setReqCur(String ReqCur) {
        this.ReqCur = ReqCur;
    }

    public String getQue() {
        return Que;
    }

    public void setQue(String Que) {
        this.Que = Que;
    }

    public String getImgLog() {
        return ImgLog;
    }

    public void setImgLog(String ImgLog) {
        this.ImgLog = ImgLog;
    }

    public Long getCurDes() {
        return CurDes;
    }

    public void setCurDes(Long CurDes) {
        this.CurDes = CurDes;
    }

    public Integer getClvEst() {
        return ClvEst;
    }

    public void setClvEst(Integer ClvEst) {
        this.ClvEst = ClvEst;
    }

    public Long getIdTCur() {
        return IdTCur;
    }

    public void setIdTCur(Long IdTCur) {
        this.IdTCur = IdTCur;
    }

    public String getIdflImg() {
        return IdflImg;
    }

    public void setIdflImg(String IdflImg) {
        this.IdflImg = IdflImg;
    }

    public String getIdRef() {
        return IdRef;
    }

    public void setIdRef(String IdRef) {
        this.IdRef = IdRef;
    }

    public Integer getEve() {
        return Eve;
    }

    public void setEve(Integer Eve) {
        this.Eve = Eve;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.IdCur);
        hash = 29 * hash + Objects.hashCode(this.NomCur);
        hash = 29 * hash + Objects.hashCode(this.Des);
        hash = 29 * hash + Objects.hashCode(this.DesCor);
        hash = 29 * hash + Objects.hashCode(this.IdCat);
        hash = 29 * hash + Objects.hashCode(this.IdSCat);
        hash = 29 * hash + Objects.hashCode(this.NRecu);
        hash = 29 * hash + Objects.hashCode(this.DurCur);
        hash = 29 * hash + Objects.hashCode(this.IdAut);
        hash = 29 * hash + Objects.hashCode(this.FecIng);
        hash = 29 * hash + Objects.hashCode(this.FecUMod);
        hash = 29 * hash + Objects.hashCode(this.ReqCur);
        hash = 29 * hash + Objects.hashCode(this.Que);
        hash = 29 * hash + Objects.hashCode(this.ImgLog);
        hash = 29 * hash + Objects.hashCode(this.CurDes);
        hash = 29 * hash + Objects.hashCode(this.ClvEst);
        hash = 29 * hash + Objects.hashCode(this.IdTCur);
        hash = 29 * hash + Objects.hashCode(this.IdflImg);
        hash = 29 * hash + Objects.hashCode(this.IdRef);
        hash = 29 * hash + Objects.hashCode(this.Eve);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cursos other = (Cursos) obj;
        if (!Objects.equals(this.NomCur, other.NomCur)) {
            return false;
        }
        if (!Objects.equals(this.Des, other.Des)) {
            return false;
        }
        if (!Objects.equals(this.DesCor, other.DesCor)) {
            return false;
        }
        if (!Objects.equals(this.DurCur, other.DurCur)) {
            return false;
        }
        if (!Objects.equals(this.ReqCur, other.ReqCur)) {
            return false;
        }
        if (!Objects.equals(this.Que, other.Que)) {
            return false;
        }
        if (!Objects.equals(this.ImgLog, other.ImgLog)) {
            return false;
        }
        if (!Objects.equals(this.IdflImg, other.IdflImg)) {
            return false;
        }
        if (!Objects.equals(this.IdRef, other.IdRef)) {
            return false;
        }
        if (!Objects.equals(this.IdCur, other.IdCur)) {
            return false;
        }
        if (!Objects.equals(this.IdCat, other.IdCat)) {
            return false;
        }
        if (!Objects.equals(this.IdSCat, other.IdSCat)) {
            return false;
        }
        if (!Objects.equals(this.NRecu, other.NRecu)) {
            return false;
        }
        if (!Objects.equals(this.IdAut, other.IdAut)) {
            return false;
        }
        if (!Objects.equals(this.FecIng, other.FecIng)) {
            return false;
        }
        if (!Objects.equals(this.FecUMod, other.FecUMod)) {
            return false;
        }
        if (!Objects.equals(this.CurDes, other.CurDes)) {
            return false;
        }
        if (!Objects.equals(this.ClvEst, other.ClvEst)) {
            return false;
        }
        if (!Objects.equals(this.IdTCur, other.IdTCur)) {
            return false;
        }
        if (!Objects.equals(this.Eve, other.Eve)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Cursos{" + "IdCur=" + IdCur + ", NomCur=" + NomCur + ", Des=" + Des + ", DesCor=" + DesCor + ", IdCat=" + IdCat + ", IdSCat=" + IdSCat + ", NRecu=" + NRecu + ", DurCur=" + DurCur + ", IdAut=" + IdAut + ", FecIng=" + FecIng + ", FecUMod=" + FecUMod + ", ReqCur=" + ReqCur + ", Que=" + Que + ", ImgLog=" + ImgLog + ", CurDes=" + CurDes + ", ClvEst=" + ClvEst + ", IdTCur=" + IdTCur + ", IdflImg=" + IdflImg + ", IdRef=" + IdRef + ", Eve=" + Eve + '}';
    }
    

}
