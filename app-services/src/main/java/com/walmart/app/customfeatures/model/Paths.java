/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.model;

import java.sql.Timestamp;
import java.util.Objects;

/**
 *
 * @author alan
 */
@CustomTable(name = "Paths")
public class Paths {

    
    private Long IdPat;
    @CustomColumn(name = "NomPat")
    private String NomPat;
    @CustomColumn(name = "ImgPat")
    private String ImgPat;
    @CustomColumn(name = "OrdPat")
    private Long OrdPat;
    @CustomColumn(name = "Eva")
    private Long Eva;
    @CustomColumn(name = "CalMin")
    private Long CalMin;
    @CustomColumn(name = "OffOn")
    private Long OffOn;
    @CustomColumn(name = "CurPer")
    private Long CurPer;
    @CustomColumn(name = "FecIni")
    private Timestamp FecIni;
    @CustomColumn(name = "FecFin")
    private Timestamp FecFin;
    @CustomColumn(name = "Ptos")
    private Integer Ptos;
    @CustomColumn(name = "Blog")
    private Long Blog;
    @CustomColumn(name = "FecUMod")
    private Timestamp FecUMod;
    @CustomColumn(name = "ClvEst")
    private Integer ClvEst;
    @CustomColumn(name = "Pri")
    private Integer Pri;
    @CustomColumn(name = "CvePuesto")
    private Long CvePuesto;
    @CustomColumn(name = "CveNivDL")
    private Long CveNivDL;
    @CustomColumn(name = "CveDepto")
    private Long CveDepto;
    @CustomColumn(name = "CveDeter")
    private Long CveDeter;
    @CustomColumn(name = "CveDistrito")
    private Long CveDistrito;
    @CustomColumn(name = "CveRegion")
    private Long CveRegion;
    @CustomColumn(name = "CveNegocio")
    private Long CveNegocio;
    @CustomColumn(name = "CveZE")
    private Long CveZE;
    @CustomColumn(name = "CvePais")
    private Long CvePais;

    public Long getIdPat() {
        return IdPat;
    }

    public void setIdPat(Long IdPat) {
        this.IdPat = IdPat;
    }

    public String getNomPat() {
        return NomPat;
    }

    public void setNomPat(String NomPat) {
        this.NomPat = NomPat;
    }

    public String getImgPat() {
        return ImgPat;
    }

    public void setImgPat(String ImgPat) {
        this.ImgPat = ImgPat;
    }

    public Long getOrdPat() {
        return OrdPat;
    }

    public void setOrdPat(Long OrdPat) {
        this.OrdPat = OrdPat;
    }

    public Long getEva() {
        return Eva;
    }

    public void setEva(Long Eva) {
        this.Eva = Eva;
    }

    public Long getCalMin() {
        return CalMin;
    }

    public void setCalMin(Long CalMin) {
        this.CalMin = CalMin;
    }

    public Long getOffOn() {
        return OffOn;
    }

    public void setOffOn(Long OffOn) {
        this.OffOn = OffOn;
    }

    public Long getCurPer() {
        return CurPer;
    }

    public void setCurPer(Long CurPer) {
        this.CurPer = CurPer;
    }

    public Timestamp getFecIni() {
        return FecIni;
    }

    public void setFecIni(Timestamp FecIni) {
        this.FecIni = FecIni;
    }

    public Timestamp getFecFin() {
        return FecFin;
    }

    public void setFecFin(Timestamp FecFin) {
        this.FecFin = FecFin;
    }

    public Integer getPtos() {
        return Ptos;
    }

    public void setPtos(Integer Ptos) {
        this.Ptos = Ptos;
    }

    public Long getBlog() {
        return Blog;
    }

    public void setBlog(Long Blog) {
        this.Blog = Blog;
    }

    public Timestamp getFecUMod() {
        return FecUMod;
    }

    public void setFecUMod(Timestamp FecUMod) {
        this.FecUMod = FecUMod;
    }

    public Integer getClvEst() {
        return ClvEst;
    }

    public void setClvEst(Integer ClvEst) {
        this.ClvEst = ClvEst;
    }

    public Integer getPri() {
        return Pri;
    }

    public void setPri(Integer Pri) {
        this.Pri = Pri;
    }

    public Long getCvePuesto() {
        return CvePuesto;
    }

    public void setCvePuesto(Long CvePuesto) {
        this.CvePuesto = CvePuesto;
    }

    public Long getCveNivDL() {
        return CveNivDL;
    }

    public void setCveNivDL(Long CveNivDL) {
        this.CveNivDL = CveNivDL;
    }

    public Long getCveDepto() {
        return CveDepto;
    }

    public void setCveDepto(Long CveDepto) {
        this.CveDepto = CveDepto;
    }

    public Long getCveDeter() {
        return CveDeter;
    }

    public void setCveDeter(Long CveDeter) {
        this.CveDeter = CveDeter;
    }

    public Long getCveDistrito() {
        return CveDistrito;
    }

    public void setCveDistrito(Long CveDistrito) {
        this.CveDistrito = CveDistrito;
    }

    public Long getCveRegion() {
        return CveRegion;
    }

    public void setCveRegion(Long CveRegion) {
        this.CveRegion = CveRegion;
    }

    public Long getCveNegocio() {
        return CveNegocio;
    }

    public void setCveNegocio(Long CveNegocio) {
        this.CveNegocio = CveNegocio;
    }

    public Long getCveZE() {
        return CveZE;
    }

    public void setCveZE(Long CveZE) {
        this.CveZE = CveZE;
    }

    public Long getCvePais() {
        return CvePais;
    }

    public void setCvePais(Long CvePais) {
        this.CvePais = CvePais;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.IdPat);
        hash = 29 * hash + Objects.hashCode(this.NomPat);
        hash = 29 * hash + Objects.hashCode(this.ImgPat);
        hash = 29 * hash + Objects.hashCode(this.OrdPat);
        hash = 29 * hash + Objects.hashCode(this.Eva);
        hash = 29 * hash + Objects.hashCode(this.CalMin);
        hash = 29 * hash + Objects.hashCode(this.OffOn);
        hash = 29 * hash + Objects.hashCode(this.CurPer);
        hash = 29 * hash + Objects.hashCode(this.FecIni);
        hash = 29 * hash + Objects.hashCode(this.FecFin);
        hash = 29 * hash + Objects.hashCode(this.Ptos);
        hash = 29 * hash + Objects.hashCode(this.Blog);
        hash = 29 * hash + Objects.hashCode(this.FecUMod);
        hash = 29 * hash + Objects.hashCode(this.ClvEst);
        hash = 29 * hash + Objects.hashCode(this.Pri);
        hash = 29 * hash + Objects.hashCode(this.CvePuesto);
        hash = 29 * hash + Objects.hashCode(this.CveNivDL);
        hash = 29 * hash + Objects.hashCode(this.CveDepto);
        hash = 29 * hash + Objects.hashCode(this.CveDeter);
        hash = 29 * hash + Objects.hashCode(this.CveDistrito);
        hash = 29 * hash + Objects.hashCode(this.CveRegion);
        hash = 29 * hash + Objects.hashCode(this.CveNegocio);
        hash = 29 * hash + Objects.hashCode(this.CveZE);
        hash = 29 * hash + Objects.hashCode(this.CvePais);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Paths other = (Paths) obj;
        if (!Objects.equals(this.NomPat, other.NomPat)) {
            return false;
        }
        if (!Objects.equals(this.ImgPat, other.ImgPat)) {
            return false;
        }
        if (!Objects.equals(this.IdPat, other.IdPat)) {
            return false;
        }
        if (!Objects.equals(this.OrdPat, other.OrdPat)) {
            return false;
        }
        if (!Objects.equals(this.Eva, other.Eva)) {
            return false;
        }
        if (!Objects.equals(this.CalMin, other.CalMin)) {
            return false;
        }
        if (!Objects.equals(this.OffOn, other.OffOn)) {
            return false;
        }
        if (!Objects.equals(this.CurPer, other.CurPer)) {
            return false;
        }
        if (!Objects.equals(this.FecIni, other.FecIni)) {
            return false;
        }
        if (!Objects.equals(this.FecFin, other.FecFin)) {
            return false;
        }
        if (!Objects.equals(this.Ptos, other.Ptos)) {
            return false;
        }
        if (!Objects.equals(this.Blog, other.Blog)) {
            return false;
        }
        if (!Objects.equals(this.FecUMod, other.FecUMod)) {
            return false;
        }
        if (!Objects.equals(this.ClvEst, other.ClvEst)) {
            return false;
        }
        if (!Objects.equals(this.Pri, other.Pri)) {
            return false;
        }
        if (!Objects.equals(this.CvePuesto, other.CvePuesto)) {
            return false;
        }
        if (!Objects.equals(this.CveNivDL, other.CveNivDL)) {
            return false;
        }
        if (!Objects.equals(this.CveDepto, other.CveDepto)) {
            return false;
        }
        if (!Objects.equals(this.CveDeter, other.CveDeter)) {
            return false;
        }
        if (!Objects.equals(this.CveDistrito, other.CveDistrito)) {
            return false;
        }
        if (!Objects.equals(this.CveRegion, other.CveRegion)) {
            return false;
        }
        if (!Objects.equals(this.CveNegocio, other.CveNegocio)) {
            return false;
        }
        if (!Objects.equals(this.CveZE, other.CveZE)) {
            return false;
        }
        if (!Objects.equals(this.CvePais, other.CvePais)) {
            return false;
        }
        return true;
    }
    
    

}
