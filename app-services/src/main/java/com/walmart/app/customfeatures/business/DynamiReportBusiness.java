/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.business;

import com.walmart.app.customfeatures.dto.DynamicReportAsyncResponseDTO;
import com.walmart.app.customfeatures.dto.DynamicReportRequestDTO;
import com.walmart.app.customfeatures.repository.DynamicReportsRepository;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;

import com.walmart.app.customfeatures.util.ReportUtil;
import com.walmart.app.customfeatures.util.SSUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Alan
 */
@Service
public class DynamiReportBusiness {

    private String pathSqls;

    private DynamicReportsRepository dynamicReportsRepository;

    private TaskExecutor taskExecutor;

    private final Logger log = LoggerFactory.getLogger(DynamiReportBusiness.class);

    private SSUtils ssUtils;

    public void uploadSqlForGenericReport(MultipartFile file) {
        StringBuilder completePath = new StringBuilder(this.pathSqls);
        if (!completePath.toString().endsWith("/")) {
            completePath.append("/");
            completePath.append(file.getOriginalFilename());
        } else {
            completePath.append(file.getOriginalFilename());
        }
        try {
            Files.write(new File(completePath.toString()).toPath(), file.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }

    public void generateDynamicReport(DynamicReportRequestDTO dynamicReportRequestDTO, HttpServletResponse response) throws IOException {
        System.gc();
        List<Map<String, Object>> result = this.dynamicReportsRepository.getGenericListMapForDynamicReport(dynamicReportRequestDTO);
        StringBuilder csv = new StringBuilder(getHeaders(result));
        csv.append(getBody(result));
        response.getWriter().print(csv.toString());
    }

    public List<Map<String,Object>> getReportesAsincronos(){
        return this.dynamicReportsRepository.findAllReportesAsincronos();
    }

    public DynamicReportAsyncResponseDTO generateDynamicReportAsync(final DynamicReportRequestDTO dynamicReportRequestDTO) throws IOException {
        final DynamicReportAsyncResponseDTO dynamicReportAsyncResponseDTO = new DynamicReportAsyncResponseDTO();
        final Path temporalPath = ssUtils.getTemporalRandomFile(dynamicReportRequestDTO.getReportFileName(), ".csv");
        final String pathStr = temporalPath.toAbsolutePath().toString();
        final Integer id = this.dynamicReportsRepository.saveReportRecord(pathStr);
        dynamicReportAsyncResponseDTO.setId(id);
        dynamicReportAsyncResponseDTO.setPath(pathStr);
        this.taskExecutor.execute(() -> {
            try {
                this.log.info("Generating report " + dynamicReportRequestDTO.getReportFileName());
                final List<Map<String, Object>> result = this.dynamicReportsRepository.getGenericListMapForDynamicReport(dynamicReportRequestDTO);
                final String headers = getHeaders(result);
                this.dynamicReportsRepository.writeFileOnDisk(headers, result, temporalPath);
                this.dynamicReportsRepository.updateStatusReportToAvailable(id);
            } catch (Exception e) {
                e.printStackTrace();
                this.dynamicReportsRepository.updateAsyncReportsWithErrorMessage(id,e.getMessage());
            }finally {
                System.gc();;
            }
        });
        return dynamicReportAsyncResponseDTO;
    }

    public DynamicReportAsyncResponseDTO getReportAsyncInformation(Integer id){
        Optional<DynamicReportAsyncResponseDTO> result = this.dynamicReportsRepository.findReportAsyncById(id);
        return result.orElse(null);
    }


    public List<Map<String, Object>> generateDynamicReportJson(DynamicReportRequestDTO dynamicReportRequestDTO) throws IOException {
        List<Map<String, Object>> result = this.dynamicReportsRepository.getGenericListMapForDynamicReport(dynamicReportRequestDTO);
        return result;
    }

    public DynamicReportRequestDTO buildRequestDTO(Map<String, String> queryMap) {
        final String reportFileName = "reportFileName";
        final String sqlFileName = "sqlFileName";
        DynamicReportRequestDTO dynamicReportRequestDTO = new DynamicReportRequestDTO();
        dynamicReportRequestDTO.setReportFileName(queryMap.get(reportFileName));
        dynamicReportRequestDTO.setPath(this.pathSqls);
        dynamicReportRequestDTO.setFileNameSql(queryMap.get(sqlFileName));
        queryMap.remove(reportFileName);
        queryMap.remove(sqlFileName);
        dynamicReportRequestDTO.setParams(queryMap);
        return dynamicReportRequestDTO;
    }

    private String getBody(List<Map<String, Object>> result) {
        StringBuilder body = new StringBuilder();
        result.forEach(resultRow -> {
            String row = ReportUtil.getRowFromMap(resultRow);
            body.append(row.concat("\n"));
        });
        return body.toString();
    }

    private String getHeaders(List<Map<String, Object>> result) {
        return result
          .stream()
          .findFirst()
          .orElse(defaultHeaders())
          .entrySet()
          .stream()
          .map(entry -> entry.getKey())
          .collect(Collectors.joining(","))
          .concat("\n");
    }

    private Map<String, Object> defaultHeaders() {
        Map<String, Object> defaultHeaders = new HashMap<>();
        defaultHeaders.put("Empty", "Empty");
        return defaultHeaders;
    }

    @Autowired
    public void setDynamicReportsRepository(DynamicReportsRepository dynamicReportsRepository) {
        this.dynamicReportsRepository = dynamicReportsRepository;
    }

    @Value("${path.sqls}")
    public void setPathSqls(String pathSqls) {
        this.pathSqls = pathSqls;
    }

    @Autowired
    @Qualifier("threadPoolTaskExecutorReports")
    public void setTaskExecutor(TaskExecutor taskExecutor) {
        this.taskExecutor = taskExecutor;
    }

    @Autowired
    public void setSsUtils(SSUtils ssUtils) {
        this.ssUtils = ssUtils;
    }
}
