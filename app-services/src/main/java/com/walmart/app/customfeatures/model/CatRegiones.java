/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.model;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author alan
 */
@CustomTable(name = "CatRegiones")
public class CatRegiones {

    @CustomColumn(name = "CveRegion")
    private Long CveRegion;
    @CustomColumn(name = "CveNegocio")
    private Long CveNegocio;
    @CustomColumn(name = "NomRegion")
    private String NomRegion;
    @CustomColumn(name = "FecAlta")
    private Date FecAlta;
    @CustomColumn(name = "FecUMod")
    private Date FecUMod;

    public Long getCveRegion() {
        return CveRegion;
    }

    public void setCveRegion(Long CveRegion) {
        this.CveRegion = CveRegion;
    }

    public Long getCveNegocio() {
        return CveNegocio;
    }

    public void setCveNegocio(Long CveNegocio) {
        this.CveNegocio = CveNegocio;
    }

    public String getNomRegion() {
        return NomRegion;
    }

    public void setNomRegion(String NomRegion) {
        this.NomRegion = NomRegion;
    }

    public Date getFecAlta() {
        return FecAlta;
    }

    public void setFecAlta(Date FecAlta) {
        this.FecAlta = FecAlta;
    }

    public Date getFecUMod() {
        return FecUMod;
    }

    public void setFecUMod(Date FecUMod) {
        this.FecUMod = FecUMod;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.CveRegion);
        hash = 67 * hash + Objects.hashCode(this.CveNegocio);
        hash = 67 * hash + Objects.hashCode(this.NomRegion);
        hash = 67 * hash + Objects.hashCode(this.FecAlta);
        hash = 67 * hash + Objects.hashCode(this.FecUMod);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CatRegiones other = (CatRegiones) obj;
        if (!Objects.equals(this.NomRegion, other.NomRegion)) {
            return false;
        }
        if (!Objects.equals(this.CveRegion, other.CveRegion)) {
            return false;
        }
        if (!Objects.equals(this.CveNegocio, other.CveNegocio)) {
            return false;
        }
        if (!Objects.equals(this.FecAlta, other.FecAlta)) {
            return false;
        }
        if (!Objects.equals(this.FecUMod, other.FecUMod)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CatRegiones{" + "CveRegion=" + CveRegion + ", CveNegocio=" + CveNegocio + ", NomRegion=" + NomRegion + ", FecAlta=" + FecAlta + ", FecUMod=" + FecUMod + '}';
    }

}
