/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.model;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author Alan
 */
@CustomTable(name = "CatPuestos")
public class CatPuestos {

    @CustomColumn(name = "CvePuesto")
    private Long CvePuesto;
    @CustomColumn(name = "CveNivDL")
    private Long CveNivDL;
    @CustomColumn(name = "NomPuesto")
    private String NomPuesto;
    @CustomColumn(name = "FecAlta")
    private Date FecAlta;
    @CustomColumn(name = "FechUMod")
    private Date FechUMod;
    @CustomColumn(name="SFCvePuesto")
    private String SFCvePuesto;

    public String getSFCvePuesto() {
        return SFCvePuesto;
    }

    public void setSFCvePuesto(String SFCvePuesto) {
        this.SFCvePuesto = SFCvePuesto;
    }

    public Long getCvePuesto() {
        return CvePuesto;
    }

    public void setCvePuesto(Long CvePuesto) {
        this.CvePuesto = CvePuesto;
    }

    public Long getCveNivDL() {
        return CveNivDL;
    }

    public void setCveNivDL(Long CveNivDL) {
        this.CveNivDL = CveNivDL;
    }

    public String getNomPuesto() {
        return NomPuesto;
    }

    public void setNomPuesto(String NomPuesto) {
        this.NomPuesto = NomPuesto;
    }

    public Date getFecAlta() {
        return FecAlta;
    }

    public void setFecAlta(Date FecAlta) {
        this.FecAlta = FecAlta;
    }

    public Date getFechUMod() {
        return FechUMod;
    }

    public void setFechUMod(Date FechUMod) {
        this.FechUMod = FechUMod;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.CvePuesto);
        hash = 79 * hash + Objects.hashCode(this.CveNivDL);
        hash = 79 * hash + Objects.hashCode(this.NomPuesto);
        hash = 79 * hash + Objects.hashCode(this.FecAlta);
        hash = 79 * hash + Objects.hashCode(this.FechUMod);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CatPuestos other = (CatPuestos) obj;
        if (!Objects.equals(this.NomPuesto, other.NomPuesto)) {
            return false;
        }
        if (!Objects.equals(this.CvePuesto, other.CvePuesto)) {
            return false;
        }
        if (!Objects.equals(this.CveNivDL, other.CveNivDL)) {
            return false;
        }
        if (!Objects.equals(this.FecAlta, other.FecAlta)) {
            return false;
        }
        if (!Objects.equals(this.FechUMod, other.FechUMod)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CatPuestos{" + "CvePuesto=" + CvePuesto + ", CveNivDL=" + CveNivDL + ", NomPuesto=" + NomPuesto + ", FecAlta=" + FecAlta + ", FechUMod=" + FechUMod + '}';
    }

}
