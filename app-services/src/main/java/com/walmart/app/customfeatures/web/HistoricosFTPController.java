package com.walmart.app.customfeatures.web;

import com.walmart.app.customfeatures.business.HistoricFTPBusiness;
import com.walmart.app.customfeatures.dto.FTPFileDetailsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/historicos-ftp")
public class HistoricosFTPController {

    @Autowired
    private HistoricFTPBusiness historicFTPBusiness;

    @GetMapping
    public ResponseEntity<List<FTPFileDetailsDTO>> list(){
        List<FTPFileDetailsDTO> result = null;
        try{
            result = this.historicFTPBusiness.getAllHistoricFTP();
        }catch(Exception e){
            e.printStackTrace();
        }
        return new ResponseEntity<>(result, result != null ?  HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @GetMapping("/{fileName}")
    public void downloadPDFResource(HttpServletRequest request, HttpServletResponse response,
                                    @PathVariable("fileName") String fileName) throws IOException {
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", String.format("inline; filename=\"" + fileName + "\""));
        FileCopyUtils.copy(this.historicFTPBusiness.getFileInputStream(fileName), response.getOutputStream());
    }
}
