/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.repository.config;

import com.walmart.app.customfeatures.dto.DynamicReportAsyncResponseDTO;
import com.walmart.app.customfeatures.dto.DynamicReportRequestDTO;
import com.walmart.app.customfeatures.repository.DynamicReportsRepository;
import com.walmart.app.customfeatures.util.ReportUtil;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Alan
 */
@Repository
public class DynamicReportsRepositoryImpl implements DynamicReportsRepository {

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private static final Logger log = LoggerFactory.getLogger(DynamicReportsRepositoryImpl.class);

    @Override
    public List<Map<String, Object>> getGenericListMapForDynamicReport(DynamicReportRequestDTO dynamicReportRequestDTO) throws IOException {
        final String query = ReportUtil.getQueryFromResource(dynamicReportRequestDTO.getPath(), dynamicReportRequestDTO.getFileNameSql());
        return this.namedParameterJdbcTemplate.queryForList(query, dynamicReportRequestDTO.getParams());
    }

    public Integer saveReportRecord(String path){
        String sentence = "INSERT INTO reportes_asincronos(path,is_available,created_date) VALUES(:path,0,:createdDate)";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("path",path);
        params.addValue("createdDate",new Date());
        KeyHolder generatedKeyHolder = new GeneratedKeyHolder();
        this.namedParameterJdbcTemplate.update(sentence,params,generatedKeyHolder);
        return Integer.valueOf(generatedKeyHolder.getKeys().get("GENERATED_KEYS").toString());
    }

    public int updateStatusReportToAvailable(final Integer id){
        String sentence = "UPDATE reportes_asincronos SET is_available = 1 WHERE id = :id ";
        Map<String,Object> params = new HashMap<>();
        params.put("id",id);
        return this.namedParameterJdbcTemplate.update(sentence,params);
    }

    public int updateAsyncReportsWithErrorMessage(final Integer id,final String errorMessage){
        String sentence = "UPDATE reportes_asincronos SET error_message = :errorMessage, is_available = 0 WHERE id = :id ";
        Map<String,Object> params = new HashMap<>();
        params.put("id",id);
        params.put("errorMessage",errorMessage);
        return this.namedParameterJdbcTemplate.update(sentence,params);
    }

    public List<Map<String,Object>> findAllReportesAsincronos(){
        String query = "SELECT TOP 20 id,path,is_available,created_date,error_message FROM reportes_asincronos ORDER BY id DESC";
        return this.namedParameterJdbcTemplate.queryForList(query,new HashMap<>());
    }

    public Optional<DynamicReportAsyncResponseDTO> findReportAsyncById(Integer id){
        String query = "SELECT id,path,is_available,error_message  FROM reportes_asincronos where id = :id";
        Map<String,Object> params = new HashMap<>();
        params.put("id",id);
        List<DynamicReportAsyncResponseDTO> result = this.namedParameterJdbcTemplate.query(query, params, (rs, i) -> {
            DynamicReportAsyncResponseDTO dynamicReportAsyncResponseDTO = new DynamicReportAsyncResponseDTO();
            dynamicReportAsyncResponseDTO.setId(rs.getInt("id"));
            dynamicReportAsyncResponseDTO.setPath(rs.getString("path"));
            dynamicReportAsyncResponseDTO.setAvailable(rs.getInt("is_available") == 1);
            dynamicReportAsyncResponseDTO.setError(rs.getString("error_message"));
            return dynamicReportAsyncResponseDTO;
        });
        return result.stream().findFirst();
    }

    public void writeFileOnDisk(String headers,List<Map<String, Object>> result, Path temporalPath) throws IOException {
        String reportName = temporalPath.getFileName().toString();
        this.log.info("> Writing report on disk {}...",reportName);
        Files.write(temporalPath,headers.getBytes());
        for(Map<String,Object> map : result){
            String row = ReportUtil.getRowFromMap(map,Boolean.TRUE);
            Files.write(temporalPath,row.getBytes(),StandardOpenOption.APPEND);
        }
        this.log.info("< Finish write report on disk {}...",reportName);
    }

    @Autowired
    @Override
    public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

}
