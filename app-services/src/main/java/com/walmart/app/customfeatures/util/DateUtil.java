/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author alan
 */
public class DateUtil {

    public static boolean compareDate(Date date1, Date date2) {
        if (date1 == date2) {
            return true;
        }
        LocalDate localDate1 = null;
        if (date1 != null) {
            localDate1 = date1.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        }
        LocalDate localDate2 = null;
        if (date2 != null) {
            localDate2 = date2.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        }
        if(localDate1 == null || localDate2 == null){
            return false;
        }
        return localDate1.getYear() == localDate2.getYear() && localDate1.getMonthValue() == localDate2.getMonthValue() && localDate1.getDayOfYear() == localDate2.getDayOfYear();
    }

    public static String getDateStrFromMilliSeconds(Long milliSeconds){
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss:SSS Z");
        Date date = new Date(milliSeconds);
        return dateFormat.format(date);
    }

    public static Long getNowMinusOneYearInMilliseconds(){
        Calendar latYear = Calendar.getInstance();
        latYear.add(Calendar.YEAR,-1);
        return latYear.getTimeInMillis();
    }

}
