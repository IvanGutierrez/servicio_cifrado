/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.repository.impl;

import com.walmart.app.customfeatures.model.CustomColumn;
import com.walmart.app.customfeatures.model.CustomTable;
import com.walmart.app.customfeatures.repository.GenericOperationsRepository;
import com.walmart.app.customfeatures.repository.config.CustomBeanProperty;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;


/**
 *
 * @author alan
 */
@Repository
public class GenericOperationsRepositoryImpl implements GenericOperationsRepository {

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private final Logger log = LoggerFactory.getLogger(GenericOperationsRepositoryImpl.class);

    @Override
    public <T extends Object> List<T> findByCustomDesc(String column, Object columnValue, Class<T> classTypeT) {
        Field[] fields = classTypeT.getDeclaredFields();
        StringBuilder query = new StringBuilder("SELECT TOP 1 ");
        query.append(Arrays.asList(fields).stream().filter(field -> field.getAnnotation(CustomColumn.class) != null).map(field -> field.getAnnotation(CustomColumn.class).name()).collect(Collectors.joining(",")));
        query.append(" FROM ");
        query.append(classTypeT.getDeclaredAnnotation(CustomTable.class).name());
        query.append(" WHERE ");
        query.append(column);
        query.append(" = :columnValue");
        Map<String, Object> params = new HashMap<>();
        params.put("columnValue", columnValue);
        return this.namedParameterJdbcTemplate.query(query.toString(), params, new BeanPropertyRowMapper<>(classTypeT));
    }

    @Override
    public <T extends Object> List<T> findAll(Class<T> classTypeT) {
        Field[] fields = classTypeT.getDeclaredFields();
        StringBuilder query = new StringBuilder("SELECT ");
        query.append(Arrays.asList(fields).stream().filter(field -> field.getAnnotation(CustomColumn.class) != null).map(field -> field.getAnnotation(CustomColumn.class).name()).collect(Collectors.joining(",")));
        query.append(" FROM ");
        query.append(classTypeT.getDeclaredAnnotation(CustomTable.class).name());
        Map<String, Object> params = new HashMap<>();
        return this.namedParameterJdbcTemplate.query(query.toString(), params, new BeanPropertyRowMapper<>(classTypeT));
    }
    
    @Override
    public <T extends Object> List<Map<String,Object>> findByCustomDesc(String column, Object columnValue, Class<T> classTypeT,String... extraColumns) {
        Field[] fields = classTypeT.getDeclaredFields();
        StringBuilder query = new StringBuilder("SELECT TOP 1 ");
        query.append(Arrays.asList(fields).stream().filter(field -> field.getAnnotation(CustomColumn.class) != null).map(field -> field.getAnnotation(CustomColumn.class).name()).collect(Collectors.joining(",")));
        if(extraColumns != null && extraColumns.length > 0){
            query.append(", ");
            query.append(Arrays.asList(extraColumns).stream().collect(Collectors.joining(", ")));
        }
        query.append(" FROM ");
        query.append(classTypeT.getDeclaredAnnotation(CustomTable.class).name());
        query.append(" WHERE ");
        query.append(column);
        query.append(" = :columnValue");
        Map<String, Object> params = new HashMap<>();
        params.put("columnValue", columnValue);
        
        return this.namedParameterJdbcTemplate.queryForList(query.toString(), params);
    }
    
    @Override
    public <T extends Object> List<T> findByCustomDescLike(String column, Object columnValue, Class<T> classTypeT) {
        Field[] fields = classTypeT.getDeclaredFields();
        StringBuilder query = new StringBuilder("SELECT TOP 1 ");
        query.append(Arrays.asList(fields).stream().filter(field -> field.getAnnotation(CustomColumn.class) != null).map(field -> field.getAnnotation(CustomColumn.class).name()).collect(Collectors.joining(",")));
        query.append(" FROM ");
        query.append(classTypeT.getDeclaredAnnotation(CustomTable.class).name());
        query.append(" WHERE ");
        query.append(column);
        query.append(" LIKE :columnValue");
        Map<String, Object> params = new HashMap<>();
        params.put("columnValue", columnValue);
        return this.namedParameterJdbcTemplate.query(query.toString(), params, new BeanPropertyRowMapper<>(classTypeT));
    }

    @Override
    public <R,T extends Object> R getLastIdReflection(String column, Class<R> classTypeReturn,Class<T> classTypeTForGetAnnotations) {
        StringBuilder query = new StringBuilder("SELECT TOP 1 ");
        query.append(column);
        query.append(" FROM ");
        query.append(classTypeTForGetAnnotations.getDeclaredAnnotation(CustomTable.class).name());
        query.append(" ORDER BY ");
        query.append(column);
        query.append(" desc");
        return this.namedParameterJdbcTemplate.queryForObject(query.toString(), new HashMap<>(), classTypeReturn);
    }

    @Override
    public <T extends Object> int save(Object object, Class<T> classTypeTForGetAnnotations) {
        Field[] fields = classTypeTForGetAnnotations.getDeclaredFields();
        StringBuilder sentence = new StringBuilder("INSERT INTO ");
        sentence.append(classTypeTForGetAnnotations.getDeclaredAnnotation(CustomTable.class).name());
        sentence.append("(");
        sentence.append(Arrays.asList(fields).stream().filter(field -> field.getAnnotation(CustomColumn.class) != null).map(field -> field.getAnnotation(CustomColumn.class).name()).collect(Collectors.joining(",")));
        sentence.append(") VALUES(");
        sentence.append(Arrays.asList(fields).stream().filter(field -> field.getAnnotation(CustomColumn.class) != null).map(field -> ":".concat(field.getAnnotation(CustomColumn.class).name())).collect(Collectors.joining(",")));
        sentence.append(")");
        this.log.info("Sentence[{}]", sentence.toString());
        return this.namedParameterJdbcTemplate.update(sentence.toString(), new CustomBeanProperty((T)object));
    }

    @Override
    public <T extends Object> int[] batchSave(List<Map<String,Object>> paramsList, Class<T> classTypeTForGetAnnotations) {
        Field[] fields = classTypeTForGetAnnotations.getDeclaredFields();
        StringBuilder sentence = new StringBuilder("INSERT INTO ");
        sentence.append(classTypeTForGetAnnotations.getDeclaredAnnotation(CustomTable.class).name());
        sentence.append("(");
        sentence.append(Arrays.asList(fields).stream().filter(field -> field.getAnnotation(CustomColumn.class) != null).map(field -> field.getAnnotation(CustomColumn.class).name()).collect(Collectors.joining(",")));
        sentence.append(") VALUES(");
        sentence.append(Arrays.asList(fields).stream().filter(field -> field.getAnnotation(CustomColumn.class) != null).map(field -> ":".concat(field.getAnnotation(CustomColumn.class).name())).collect(Collectors.joining(",")));
        sentence.append(")");
        this.log.info("Sentence[{}]", sentence.toString());
        return this.namedParameterJdbcTemplate.batchUpdate(sentence.toString(),paramsList.toArray(new Map[paramsList.size()]));
    }
    
    @Override
    public <T extends Object> Long saveAndGetId(Object object, Class<T> classTypeTForGetAnnotations) {
        KeyHolder generatedKeyHolder = new GeneratedKeyHolder();
        Field[] fields = classTypeTForGetAnnotations.getDeclaredFields();
        StringBuilder sentence = new StringBuilder("INSERT INTO ");
        sentence.append(classTypeTForGetAnnotations.getDeclaredAnnotation(CustomTable.class).name());
        sentence.append("(");
        sentence.append(Arrays.asList(fields).stream().filter(field -> field.getAnnotation(CustomColumn.class) != null).map(field -> field.getAnnotation(CustomColumn.class).name()).collect(Collectors.joining(",")));
        sentence.append(") VALUES(");
        sentence.append(Arrays.asList(fields).stream().filter(field -> field.getAnnotation(CustomColumn.class) != null).map(field -> ":".concat(field.getAnnotation(CustomColumn.class).name())).collect(Collectors.joining(",")));
        sentence.append(")");
        this.log.info("Sentence[{}]", sentence.toString());
        this.namedParameterJdbcTemplate.update(sentence.toString(), new CustomBeanProperty((T)object),generatedKeyHolder);
        return Long.valueOf(generatedKeyHolder.getKeys().get("GENERATED_KEYS").toString());
    }

    @Autowired
    @Override
    public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

}
