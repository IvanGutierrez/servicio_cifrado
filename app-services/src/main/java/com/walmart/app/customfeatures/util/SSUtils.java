package com.walmart.app.customfeatures.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

@Component
public class SSUtils {
    private String outputDir;

    public Path getFolderPath() throws IOException {
        StringBuilder pathBuilder = new StringBuilder(getFTPHistoricPath());

        if (!Files.exists(Paths.get(pathBuilder.toString()))) {
            Files.createDirectory(Paths.get(pathBuilder.toString()));
        }
        pathBuilder.append(Long.valueOf(Calendar.getInstance().getTimeInMillis()).toString().concat(".dat"));
        return new File(pathBuilder.toString()).toPath();
    }

    public String getFTPHistoricPath(){
        StringBuilder pathBuilder = new StringBuilder(this.outputDir);
        pathBuilder.append("/downloadFiles/");
        return pathBuilder.toString();
    }

    public Path getTemporalRandomFile(String fileName, String ext) throws IOException {
        StringBuilder pathBuilder = new StringBuilder(this.outputDir);
        pathBuilder.append("/temporalReports/");
        if (!Files.exists(Paths.get(pathBuilder.toString()))) {
            Files.createDirectory(Paths.get(pathBuilder.toString()));
        }
        pathBuilder.append(fileName)
        .append("_")
        .append(ThreadLocalRandom.current().nextInt())
        .append("_")
        .append(Long.valueOf(Calendar.getInstance().getTimeInMillis()).toString())
        .append(ext);

        return new File(pathBuilder.toString()).toPath();
    }

    @Value("${path.outputdir}")
    public void setOutputDir(String outputDir) {
        this.outputDir = outputDir;
    }
}
