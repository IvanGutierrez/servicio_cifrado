package com.walmart.app.customfeatures.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class BitacoraDTO implements Serializable {

    private String noAsociado;
    private String email;
    private String errorMessage;
    private  List<String> asociadosChanges;
    private List<String> relAsocWmChanges;

    public BitacoraDTO() {
    }

    public BitacoraDTO(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getNoAsociado() {
        return noAsociado;
    }

    public void setNoAsociado(String noAsociado) {
        this.noAsociado = noAsociado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<String> getAsociadosChanges() {
        return asociadosChanges;
    }

    public void setAsociadosChanges(List<String> asociadosChanges) {
        this.asociadosChanges = asociadosChanges;
    }

    public List<String> getRelAsocWmChanges() {
        return relAsocWmChanges;
    }

    public void setRelAsocWmChanges(List<String> relAsocWmChanges) {
        this.relAsocWmChanges = relAsocWmChanges;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BitacoraDTO that = (BitacoraDTO) o;
        return Objects.equals(noAsociado, that.noAsociado) &&
                Objects.equals(email, that.email) &&
                Objects.equals(errorMessage, that.errorMessage) &&
                Objects.equals(asociadosChanges, that.asociadosChanges) &&
                Objects.equals(relAsocWmChanges, that.relAsocWmChanges);
    }

    @Override
    public int hashCode() {
        return Objects.hash(noAsociado, email, errorMessage, asociadosChanges, relAsocWmChanges);
    }

    @Override
    public String toString() {
        return "BitacoraDTO{" +
                "noAsociado='" + noAsociado + '\'' +
                ", email='" + email + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                ", asociadosChanges=" + asociadosChanges +
                ", relAsocWmChanges=" + relAsocWmChanges +
                '}';
    }
}
