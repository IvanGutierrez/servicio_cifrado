/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.repository;

import java.util.List;
import java.util.Map;

/**
 *
 * @author alan
 */
public interface GenericOperationsRepository extends GenericRepository{

    <T extends Object> List<T> findAll(Class<T> classTypeT);
    
    <T extends Object> List<T> findByCustomDesc(String column, Object columnValue, Class<T> clazz);
    
    <T extends Object> List<Map<String,Object>> findByCustomDesc(String column, Object columnValue, Class<T> classTypeT,String... extraColumns);
    
    <T extends Object> List<T> findByCustomDescLike(String column, Object columnValue, Class<T> classTypeT);
    
    <R,T extends Object> R getLastIdReflection(String column, Class<R> classTypeReturn,Class<T> classTypeTForGetAnnotations);
    
    <T extends Object> int save(Object object, Class<T> classTypeTForGetAnnotations) ;

    <T extends Object> int[] batchSave(List<Map<String,Object>> paramsList, Class<T> classTypeTForGetAnnotations);
    
    <T extends Object> Long saveAndGetId(Object object, Class<T> classTypeTForGetAnnotations);
    
}
