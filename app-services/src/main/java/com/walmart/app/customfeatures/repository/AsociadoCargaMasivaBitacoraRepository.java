/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.repository;

import com.walmart.app.customfeatures.dto.BitacoraDTO;

import java.util.List;

/**
 *
 * @author alan
 */
public interface AsociadoCargaMasivaBitacoraRepository extends GenericRepository{
    
    int save(BitacoraDTO bitacoraDTO);
    
}
