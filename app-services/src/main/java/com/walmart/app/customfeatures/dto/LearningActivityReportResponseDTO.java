package com.walmart.app.customfeatures.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LearningActivityReportResponseDTO implements Serializable {

    private PagingDTO paging;
    private List<ElementsDTO> elements;

    public PagingDTO getPaging() {
        return paging;
    }

    public void setPaging(PagingDTO paging) {
        this.paging = paging;
    }

    public List<ElementsDTO> getElements() {
        return elements;
    }

    public void setElements(List<ElementsDTO> elements) {
        this.elements = elements;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LearningActivityReportResponseDTO that = (LearningActivityReportResponseDTO) o;
        return Objects.equals(paging, that.paging) &&
                Objects.equals(elements, that.elements);
    }

    @Override
    public int hashCode() {
        return Objects.hash(paging, elements);
    }

    @Override
    public String toString() {
        return "LearningActivityReportResponseDTO{" +
                "paging=" + paging +
                ", elements=" + elements +
                '}';
    }
}
