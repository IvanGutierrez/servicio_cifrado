package com.walmart.app.customfeatures.model;

import java.util.Objects;

@CustomTable(name = "LinkedInLearningHistoryX")
public class LinkedInLearningHistory {

    @CustomColumn(name="NombreUsuario")
    private String nombreUsuario;
    @CustomColumn(name="Email")
    private String email;
    @CustomColumn(name="NombreCurso")
    private String nombreCurso;
    @CustomColumn(name="Completado")
    private String completado;
    @CustomColumn(name="WinNumber")
    private String winNumber;
    @CustomColumn(name="PorcentajeRealizacion")
    private Integer porcentajeRealizacion;
    @CustomColumn(name="SegundosVisualizacion")
    private Long segundosVisualizacion;
    @CustomColumn(name="ContentUrn")
    private String contentUrn;
    @CustomColumn(name="FechaHit")
    private String fechaHit;

    public LinkedInLearningHistory() {
    }

    public LinkedInLearningHistory(String nombreUsuario, String email, String winNumber, String contentUrn) {
        this.nombreUsuario = nombreUsuario;
        this.email = email;
        this.winNumber = winNumber;
        this.contentUrn = contentUrn;
    }

    public String getFechaHit() {
        return fechaHit;
    }

    public void setFechaHit(String fechaHit) {
        this.fechaHit = fechaHit;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNombreCurso() {
        return nombreCurso;
    }

    public void setNombreCurso(String nombreCurso) {
        this.nombreCurso = nombreCurso;
    }

    public String getCompletado() {
        return completado;
    }

    public void setCompletado(String completado) {
        this.completado = completado;
    }

    public String getWinNumber() {
        return winNumber;
    }

    public void setWinNumber(String winNumber) {
        this.winNumber = winNumber;
    }

    public Integer getPorcentajeRealizacion() {
        return porcentajeRealizacion;
    }

    public void setPorcentajeRealizacion(Integer porcentajeRealizacion) {
        this.porcentajeRealizacion = porcentajeRealizacion;
    }

    public Long getSegundosVisualizacion() {
        return segundosVisualizacion;
    }

    public void setSegundosVisualizacion(Long segundosVisualizacion) {
        this.segundosVisualizacion = segundosVisualizacion;
    }

    public String getContentUrn() {
        return contentUrn;
    }

    public void setContentUrn(String contentUrn) {
        this.contentUrn = contentUrn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LinkedInLearningHistory that = (LinkedInLearningHistory) o;
        return Objects.equals(nombreUsuario, that.nombreUsuario) &&
                Objects.equals(email, that.email) &&
                Objects.equals(nombreCurso, that.nombreCurso) &&
                Objects.equals(completado, that.completado) &&
                Objects.equals(winNumber, that.winNumber) &&
                Objects.equals(porcentajeRealizacion, that.porcentajeRealizacion) &&
                Objects.equals(segundosVisualizacion, that.segundosVisualizacion) &&
                Objects.equals(contentUrn, that.contentUrn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nombreUsuario, email, nombreCurso, completado, winNumber, porcentajeRealizacion, segundosVisualizacion, contentUrn);
    }

    @Override
    public String toString() {
        return "LinkedInLearningHistory{" +
                "nombreUsuario='" + nombreUsuario + '\'' +
                ", email='" + email + '\'' +
                ", nombreCurso='" + nombreCurso + '\'' +
                ", completado='" + completado + '\'' +
                ", winNumber='" + winNumber + '\'' +
                ", porcentajeRealizacion=" + porcentajeRealizacion +
                ", segundosVisualizacion=" + segundosVisualizacion +
                ", contentUrn='" + contentUrn + '\'' +
                '}';
    }
}
