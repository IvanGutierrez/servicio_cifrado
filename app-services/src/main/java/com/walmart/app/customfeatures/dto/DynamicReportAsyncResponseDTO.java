package com.walmart.app.customfeatures.dto;

import java.io.Serializable;
import java.util.Objects;

public class DynamicReportAsyncResponseDTO implements Serializable {

    private Integer id;
    private String path;
    private boolean available;
    private String error;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DynamicReportAsyncResponseDTO that = (DynamicReportAsyncResponseDTO) o;
        return available == that.available &&
                Objects.equals(id, that.id) &&
                Objects.equals(path, that.path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, path, available);
    }

    @Override
    public String toString() {
        return "DynamicReportAsyncResponseDTO{" +
                "id=" + id +
                ", path='" + path + '\'' +
                ", available=" + available +
                '}';
    }
}
