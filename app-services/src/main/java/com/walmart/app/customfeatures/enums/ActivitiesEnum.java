package com.walmart.app.customfeatures.enums;

public enum ActivitiesEnum {
    SECONDS_VIEWED,
    PROGRESS_PERCENTAGE,
    COMPLETIONS;
}
