package com.walmart.app.customfeatures.dto;

import java.util.Objects;

public class LearningActivityReportRequestDTO {

    private String token;
    private String q = "criteria";
    private String timeOffsetUnit = "DAY";
    private Integer start = 0;
    private Integer count = 1000;
    private String assetType = "COURSE";
    private Long startedAt;
    private String aggregationCriteriaPrimary = "INDIVIDUAL";
    private String aggregationCriteriaSecondary = "CONTENT";
    private String sortByEngagementMetricType = "PROGRESS_PERCENTAGE";
    private Integer timeOffsetDuration = 2;

    public LearningActivityReportRequestDTO() {
    }

    public LearningActivityReportRequestDTO(Long startedAt) {
        this.startedAt = startedAt;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getQ() {
        return q;
    }

    public void setQ(String q) {
        this.q = q;
    }

    public String getTimeOffsetUnit() {
        return timeOffsetUnit;
    }

    public void setTimeOffsetUnit(String timeOffsetUnit) {
        this.timeOffsetUnit = timeOffsetUnit;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public String getAssetType() {
        return assetType;
    }

    public void setAssetType(String assetType) {
        this.assetType = assetType;
    }

    public Long getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(Long startedAt) {
        this.startedAt = startedAt;
    }

    public String getAggregationCriteriaPrimary() {
        return aggregationCriteriaPrimary;
    }

    public void setAggregationCriteriaPrimary(String aggregationCriteriaPrimary) {
        this.aggregationCriteriaPrimary = aggregationCriteriaPrimary;
    }

    public String getAggregationCriteriaSecondary() {
        return aggregationCriteriaSecondary;
    }

    public void setAggregationCriteriaSecondary(String aggregationCriteriaSecondary) {
        this.aggregationCriteriaSecondary = aggregationCriteriaSecondary;
    }

    public String getSortByEngagementMetricType() {
        return sortByEngagementMetricType;
    }

    public void setSortByEngagementMetricType(String sortByEngagementMetricType) {
        this.sortByEngagementMetricType = sortByEngagementMetricType;
    }

    public Integer getTimeOffsetDuration() {
        return timeOffsetDuration;
    }

    public void setTimeOffsetDuration(Integer timeOffsetDuration) {
        this.timeOffsetDuration = timeOffsetDuration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LearningActivityReportRequestDTO that = (LearningActivityReportRequestDTO) o;
        return Objects.equals(token, that.token) &&
                Objects.equals(q, that.q) &&
                Objects.equals(timeOffsetUnit, that.timeOffsetUnit) &&
                Objects.equals(start, that.start) &&
                Objects.equals(assetType, that.assetType) &&
                Objects.equals(startedAt, that.startedAt) &&
                Objects.equals(aggregationCriteriaPrimary, that.aggregationCriteriaPrimary) &&
                Objects.equals(aggregationCriteriaSecondary, that.aggregationCriteriaSecondary) &&
                Objects.equals(sortByEngagementMetricType, that.sortByEngagementMetricType) &&
                Objects.equals(timeOffsetDuration, that.timeOffsetDuration);
    }

    @Override
    public int hashCode() {
        return Objects.hash(token, q, timeOffsetUnit, start, assetType, startedAt, aggregationCriteriaPrimary, aggregationCriteriaSecondary, sortByEngagementMetricType, timeOffsetDuration);
    }

    @Override
    public String toString() {
        return "LearningActivityReportRequestDTO{" +
                "token='" + token + '\'' +
                ", q='" + q + '\'' +
                ", timeOffsetUnit='" + timeOffsetUnit + '\'' +
                ", start=" + start +
                ", assetType='" + assetType + '\'' +
                ", startedAt=" + startedAt +
                ", aggregationCriteriaPrimary='" + aggregationCriteriaPrimary + '\'' +
                ", aggregationCriteriaSecondary='" + aggregationCriteriaSecondary + '\'' +
                ", sortByEngagementMetricType='" + sortByEngagementMetricType + '\'' +
                ", timeOffsetDuration=" + timeOffsetDuration +
                '}';
    }
}
