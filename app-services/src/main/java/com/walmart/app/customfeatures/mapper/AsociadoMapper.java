/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.mapper;

import com.walmart.app.customfeatures.model.Asociado;
import java.sql.ResultSet;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

/**
 *
 * @author alan
 */
@Service
public class AsociadoMapper {

    public RowMapper<Asociado> asociadoRowMapper() {
        return (ResultSet rs, int i) -> {
            Asociado asociado = new Asociado();
            asociado.setIdAsoc(rs.getLong("IdAsoc"));
            asociado.setCvePuesto(rs.getLong("CvePuesto"));
            asociado.setNoAsoc(rs.getString("NoAsoc"));
            asociado.setNom(rs.getString("Nom"));
            asociado.setGen(rs.getString("Gen"));
            asociado.setFecNac(rs.getTimestamp("FecNac"));
            asociado.setFecIng(rs.getTimestamp("FecIng"));
            asociado.setFecAlta(rs.getTimestamp("FecAlta"));
            asociado.setFecUMod(rs.getTimestamp("FecUMod"));
            asociado.setUsr(rs.getString("Usr"));
            asociado.setPsw(rs.getString("Psw"));
            asociado.setPswCad(rs.getTimestamp("PswCad"));
            asociado.setInt(rs.getInt("Int"));
            asociado.setSes(rs.getString("Ses"));
            asociado.setApa(rs.getString("Apa"));
            asociado.setAma(rs.getString("Ama"));
            asociado.setEma(rs.getString("Ema"));
            asociado.setTer(rs.getInt("Ter"));
            asociado.setFecUVis(rs.getTimestamp("FecUVis"));
            asociado.setFecAct(rs.getTimestamp("FecAct"));
            asociado.settFij(rs.getString("TFij"));
            asociado.settCel(rs.getString("TCel"));
            asociado.setEciv(rs.getString("Eciv"));
            asociado.setEstatus(rs.getInt("Estatus"));
            asociado.setImgAsoc(rs.getString("ImgAsoc"));
            asociado.setIdPerfil(rs.getLong("IdPerfil"));
            return asociado;
        };
    }

}
