package com.walmart.app.customfeatures.web;

import com.walmart.app.customfeatures.business.LinkedInLearningHistoryBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/linkedin-learning")
public class LinkedInLearninController {

    @Autowired
    private LinkedInLearningHistoryBusiness linkedInLearningHistoryBusiness;

    @GetMapping("/execute-report")
    public void executeReport(){
        this.linkedInLearningHistoryBusiness.getLinkedInReportFromLinkedInApiREST();
    }

}
