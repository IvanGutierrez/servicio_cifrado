/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.business;

import com.walmart.app.customfeatures.enums.PaisEnum;
import com.walmart.app.customfeatures.model.Asociado;
import com.walmart.app.customfeatures.repository.AsociadoRepository;
import com.walmart.app.customfeatures.util.AsociadoCSVUpdateStatusConstants;
import com.walmart.app.customfeatures.util.ConstantesDefinidasPorElPatronConstants;
import com.walmart.app.customfeatures.util.SSUtils;
import com.walmart.app.securitytool.dto.KeyDTO;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

/**
 * @author Alan
 */
@Service
public class AsociadoBusiness {

    private final TaskExecutor taskExecutor;

    private final TaskExecutor taskExecutorAux;

    @Autowired
    private AsociadoRepository asociadoRepository;

    @Autowired
    private AsociadoSaveBatchBusiness asociadoSaveBatchBusiness;

    private final Logger log = LoggerFactory.getLogger(AsociadoBusiness.class);

    private SSUtils ssUtils;

    public AsociadoBusiness(@Qualifier("CustomThreadPoolTaskExecutor") TaskExecutor taskExecutor, @Qualifier("threadPoolTaskExecutorAux") TaskExecutor taskExecutorAux) {
        this.taskExecutor = taskExecutor;
        this.taskExecutorAux = taskExecutorAux;
    }

    public Integer updateWinNumber(MultipartFile file) throws IOException {
        Boolean csvHead = Boolean.TRUE;
        Integer updatedRows = 0;
        for (String line : IOUtils.readLines(file.getInputStream(), StandardCharsets.UTF_8)) {
            if (csvHead) {
                csvHead = Boolean.FALSE;
                continue;
            }
            this.taskExecutorAux.execute(() -> {
                final String[] csvColumns = line.replaceAll("\"", "").split(",");
                updateWinNumberInDetail(csvColumns);
            });
        }
        return updatedRows;
    }

    private void updateWinNumberInDetail(final String[] csvColumns) {
        final Integer numeroAsociadoCsv = 0;
        final Integer winNumberCsv = 1;
        if (csvColumns.length > 1) {
            String noAsocTrimmed = csvColumns[numeroAsociadoCsv].trim();
            if (!noAsocTrimmed.isEmpty()) {
                Long winNumber = 0L;
                try {
                    winNumber = Long.valueOf(csvColumns[winNumberCsv].trim());
                } catch (NumberFormatException e) {
                    this.log.error("Unable to parse the value, error: "+e.getMessage());
                }
                if (winNumber != 0L) {
                    Boolean verifyIfAsociadoExists = this.asociadoRepository.findWinNumberByNoAsoc(noAsocTrimmed);
                    if (verifyIfAsociadoExists) {
                        Asociado asociado = new Asociado(noAsocTrimmed, winNumber);
                        this.asociadoRepository.updateWinNumberByAsoc(Arrays.asList(asociado));
                    } else {
                        this.log.info("The Asociado {} doesnt exist", noAsocTrimmed);
                    }
                } else {
                    this.log.info("It was not posible to update the noAsoc {}, the winNumber has a wrong format", noAsocTrimmed);
                }
            } else {
                this.log.info("It was not posible to get the numAsoc");
            }
        } else {
            this.log.info("The row {} doesnt have enough information", csvColumns);
        }
    }

    public Integer updateStatus(MultipartFile file, KeyDTO keys) throws IOException {
        Boolean csvHead = Boolean.TRUE;
        List<Asociado> asociadosList = new ArrayList<>();
        for (String line : IOUtils.readLines(file.getInputStream(), StandardCharsets.UTF_8)) {
            if (csvHead) {
                csvHead = Boolean.FALSE;
                continue;
            }
            String[] csvColumns = line.split(",");
            Asociado asociado = new Asociado();
            asociado.setUsr(csvColumns[AsociadoCSVUpdateStatusConstants.EMAIL].trim());
            if (!asociado.getUsr().endsWith(PaisEnum.MEXICO.getEmailDomain())) {
                asociado.setUsr(asociado.getUsr().concat(PaisEnum.MEXICO.getEmailDomain()));
            }
            String estatusStr = csvColumns[AsociadoCSVUpdateStatusConstants.EMPLOYE_STATUS].trim().toLowerCase();
            asociado.setEstatus(estatusStr.equals("active") ? 1 : 0);
            asociadosList.add(asociado);
        }


        int[] updatedRows = this.asociadoRepository.updateStatus(new ArrayList<>(asociadosList));
        Integer updted = 0;
        for (int i : updatedRows) {
            updted += i;
        }

        return asociadosList.size();
    }

    public void saveBatch(MultipartFile multipartFile,String separator) throws IOException{
        Path temporalUsersFile = saveTemporalUserFile(multipartFile);
        Boolean csvHead = Boolean.TRUE;
        this.log.info("Reading for the temporal file: "+temporalUsersFile.toString());
        for (String line : Files.readAllLines(temporalUsersFile)) {
            if (csvHead) {
                csvHead = Boolean.FALSE;
                continue;
            }
            final String newLineOnThread = new String(line.replaceAll("\"", "").getBytes());

                this.asociadoSaveBatchBusiness.saveOrUpdateAsociadoBatch(newLineOnThread,separator);

        }
    }

    private Path saveTemporalUserFile(MultipartFile file) throws IOException {
        Path temporalFile = this.ssUtils.getFolderPath();
        Files.write(temporalFile, file.getBytes());
        return temporalFile;
    }

    @Autowired
    public void setSsUtils(SSUtils ssUtils) {
        this.ssUtils = ssUtils;
    }
}
