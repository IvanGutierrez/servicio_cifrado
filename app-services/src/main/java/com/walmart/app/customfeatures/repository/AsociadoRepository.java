/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.repository;

import com.walmart.app.customfeatures.model.Asociado;
import java.util.List;


/**
 *
 * @author Alan
 */public interface AsociadoRepository extends GenericRepository{
    
    Long save(Asociado asociado);
    
    int[] updateStatus(List<Asociado> asociadoList);
    
    List<Asociado> getAllAsociados();
    
    List<Asociado> findByUsr(String noAsoc);
    
    int updateCvePuestoById(Asociado asociado,Long cvePuesto);
    
    int updateAsociadoById(Asociado asociado);
    
    int[] updateWinNumberByAsoc(List<Asociado> asociadoList);
    
    Boolean findWinNumberByNoAsoc(String noAsoc);
    
}
