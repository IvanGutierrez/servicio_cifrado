/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.repository;

import com.walmart.app.customfeatures.dto.DynamicReportAsyncResponseDTO;
import com.walmart.app.customfeatures.dto.DynamicReportRequestDTO;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 *
 * @author Alan
 */
public interface DynamicReportsRepository extends GenericRepository{

    List<Map<String, Object>> getGenericListMapForDynamicReport(DynamicReportRequestDTO dynamicReportRequestDTO) throws IOException;

    void writeFileOnDisk(String headers,List<Map<String, Object>> result, Path temporalPath) throws IOException;

    Integer saveReportRecord(String path);

    int updateStatusReportToAvailable(Integer id);

    int updateAsyncReportsWithErrorMessage(Integer id, String errorMessage);

    List<Map<String,Object>> findAllReportesAsincronos();

    Optional<DynamicReportAsyncResponseDTO> findReportAsyncById(Integer id);

}
