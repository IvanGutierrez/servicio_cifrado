package com.walmart.app.customfeatures.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ActivitiesDTO implements Serializable {

    private String engagementType;
    private Long firstEngagedAt;
    private Long lastEngagedAt;
    private String engagementMetricQualifier;
    private Long engagementValue;
    private String assetType;

    public String getEngagementType() {
        return engagementType;
    }

    public void setEngagementType(String engagementType) {
        this.engagementType = engagementType;
    }

    public Long getFirstEngagedAt() {
        return firstEngagedAt;
    }

    public void setFirstEngagedAt(Long firstEngagedAt) {
        this.firstEngagedAt = firstEngagedAt;
    }

    public Long getLastEngagedAt() {
        return lastEngagedAt;
    }

    public void setLastEngagedAt(Long lastEngagedAt) {
        this.lastEngagedAt = lastEngagedAt;
    }

    public String getEngagementMetricQualifier() {
        return engagementMetricQualifier;
    }

    public void setEngagementMetricQualifier(String engagementMetricQualifier) {
        this.engagementMetricQualifier = engagementMetricQualifier;
    }

    public Long getEngagementValue() {
        return engagementValue;
    }

    public void setEngagementValue(Long engagementValue) {
        this.engagementValue = engagementValue;
    }

    public String getAssetType() {
        return assetType;
    }

    public void setAssetType(String assetType) {
        this.assetType = assetType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActivitiesDTO that = (ActivitiesDTO) o;
        return Objects.equals(engagementType, that.engagementType) &&
                Objects.equals(firstEngagedAt, that.firstEngagedAt) &&
                Objects.equals(lastEngagedAt, that.lastEngagedAt) &&
                Objects.equals(engagementMetricQualifier, that.engagementMetricQualifier) &&
                Objects.equals(engagementValue, that.engagementValue) &&
                Objects.equals(assetType, that.assetType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(engagementType, firstEngagedAt, lastEngagedAt, engagementMetricQualifier, engagementValue, assetType);
    }

    @Override
    public String toString() {
        return "ActivitiesDTO{" +
                "engagementType='" + engagementType + '\'' +
                ", firstEngagedAt=" + firstEngagedAt +
                ", lastEngagedAt=" + lastEngagedAt +
                ", engagementMetricQualifier='" + engagementMetricQualifier + '\'' +
                ", engagementValue=" + engagementValue +
                ", assetType='" + assetType + '\'' +
                '}';
    }
}
