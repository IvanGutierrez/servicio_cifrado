package com.walmart.app.customfeatures.business;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Component
public class AsociadoScheduledBusiness {

    @Autowired
    private WalmartFTPBusiniess walmartFTPBusiniess;

    private final TaskExecutor taskExecutor;

    private final AsociadoSaveBatchBusiness asociadoSaveBatchBusiness;

    private final Logger log = LoggerFactory.getLogger(AsociadoScheduledBusiness.class);

    public AsociadoScheduledBusiness(@Qualifier("CustomThreadPoolTaskExecutor") TaskExecutor taskExecutor,AsociadoSaveBatchBusiness asociadoSaveBatchBusiness) {
        this.taskExecutor = taskExecutor;
        this.asociadoSaveBatchBusiness = asociadoSaveBatchBusiness;
    }

    // @Scheduled(cron = "${procesos-masivos.cron.asociados}", zone = "Mexico/General")
    public void executeAsociadoScheduled() throws IOException {
        Boolean firstRow = Boolean.TRUE ;
        this.log.info("****** Start FTP Asociados Masivos ******");
        Path temporalFile =  walmartFTPBusiniess.getTemporalFileFromFTP();
        if(!temporalFile.toFile().exists()){
            this.log.error("It was an ERROR at the moment to download the file, it doesn't exist, please se the above log details");
            return ;
        }
        for(String line : Files.readAllLines(temporalFile)){
            if(firstRow){
                firstRow = Boolean.FALSE;
                continue;
            }

                this.asociadoSaveBatchBusiness.saveOrUpdateAsociadoBatch(line,"|");

        }

    }

}
