/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.model;

import com.walmart.app.customfeatures.util.DateUtil;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author Alan
 */
@CustomTable(name="Asociado")
public class Asociado implements Serializable {

    
    @CustomColumn(name = "IdAsoc")
    private Long idAsoc;
    @CustomColumn(name = "CvePuesto")
    private Long cvePuesto;
    @CustomColumn(name = "NoAsoc")
    private String noAsoc;
    @CustomColumn(name = "Nom")
    private String nom;
    @CustomColumn(name = "Gen")
    private String gen;
    @CustomColumn(name = "FecNac")
    private Date fecNac;
    @CustomColumn(name = "FecIng")
    private Date fecIng;    
    @CustomColumn(name = "FecAlta")    
    private Date fecAlta;    
    @CustomColumn(name = "FecUMod")    
    private Date fecUMod;    
    @CustomColumn(name = "Usr")
    private String usr;    
    @CustomColumn(name = "Psw")
    private String psw;
    @CustomColumn(name = "PswCad")    
    private Date pswCad;    
    @CustomColumn(name = "Int")
    private int Int;
    @CustomColumn(name = "Ses")
    private String ses;    
    @CustomColumn(name = "Apa")
    private String apa;    
    @CustomColumn(name = "Ama")
    private String ama;    
    @CustomColumn(name = "Ema")
    private String ema;    
    @CustomColumn(name = "Ter")
    private int ter;
    @CustomColumn(name = "FecUVis")    
    private Date fecUVis;
    @CustomColumn(name = "FecAct")    
    private Date fecAct;    
    @CustomColumn(name = "TFij")
    private String tFij;    
    @CustomColumn(name = "TCel")
    private String tCel;    
    @CustomColumn(name = "Eciv")
    private String eciv;
    @CustomColumn(name = "Estatus")
    private Integer estatus;
    @CustomColumn(name = "ImgAsoc")
    private String imgAsoc;
    @CustomColumn(name = "IdPerfil")
    private Long idPerfil;
    @CustomColumn(name = "winNumber")
    private Long winNumber;
    @CustomColumn(name = "supervisor")
    private Long supervisor;
    
    private RelAsocWm relAsocWm;
    
    public Asociado(){
    
    }

    public Asociado(String noAsoc){
        this.noAsoc = noAsoc;
    }

    public Asociado(String noAsoc, Long winNumber) {
        this.noAsoc = noAsoc;
        this.winNumber = winNumber;
    }
    
    public Asociado(Long idAsoc) {
        this.idAsoc = idAsoc;
    }
    
    public Asociado(int ter,Integer estatus){
        this.ter = ter;
        this.estatus = estatus;
        this.psw = "N/A";
        this.gen = "X";
        this.fecNac = Calendar.getInstance().getTime();
        this.fecIng = Calendar.getInstance().getTime();
        this.fecAlta = Calendar.getInstance().getTime();
        this.fecUMod = Calendar.getInstance().getTime();
        this.Int = 0;
        this.apa = "N/A";
        this.ama = "N/A";
        this.fecUVis = Calendar.getInstance().getTime();;
        this.fecAct = Calendar.getInstance().getTime();
        this.tFij = "N/A";
        this.tCel = "N/A";
        this.ses = String.valueOf(hashCode());
        this.eciv = "N/A";
        this.idPerfil = 1L;
        this.imgAsoc = "N/A";
    }

    public Long getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(Long supervisor) {
        this.supervisor = supervisor;
    }

    public Long getWinNumber() {
        return winNumber;
    }

    public void setWinNumber(Long winNumber) {
        this.winNumber = winNumber;
    }
    
    public RelAsocWm getRelAsocWm() {
        return relAsocWm;
    }

    public void setRelAsocWm(RelAsocWm relAsocWm) {
        this.relAsocWm = relAsocWm;
    }
    
    public Long getIdAsoc() {
        return idAsoc;
    }

    public void setIdAsoc(Long idAsoc) {
        this.idAsoc = idAsoc;
    }

    public Long getCvePuesto() {
        return cvePuesto;
    }

    public void setCvePuesto(Long cvePuesto) {
        this.cvePuesto = cvePuesto;
    }

    public String getNoAsoc() {
        return noAsoc;
    }

    public void setNoAsoc(String noAsoc) {
        this.noAsoc = noAsoc;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getGen() {
        return gen;
    }

    public void setGen(String gen) {
        this.gen = gen;
    }

    public Date getFecNac() {
        return fecNac;
    }

    public void setFecNac(Date fecNac) {
        this.fecNac = fecNac;
    }

    public Date getFecIng() {
        return fecIng;
    }

    public void setFecIng(Date fecIng) {
        this.fecIng = fecIng;
    }

    public Date getFecAlta() {
        return fecAlta;
    }

    public void setFecAlta(Date fecAlta) {
        this.fecAlta = fecAlta;
    }

    public Date getFecUMod() {
        return fecUMod;
    }

    public void setFecUMod(Date fecUMod) {
        this.fecUMod = fecUMod;
    }

    public String getUsr() {
        return usr;
    }

    public void setUsr(String usr) {
        this.usr = usr;
    }

    public String getPsw() {
        return psw;
    }

    public void setPsw(String psw) {
        this.psw = psw;
    }

    public Date getPswCad() {
        return pswCad;
    }

    public void setPswCad(Date pswCad) {
        this.pswCad = pswCad;
    }

    public int getInt() {
        return Int;
    }

    public void setInt(int Int) {
        this.Int = Int;
    }
    
    
    public String getSes() {
        return ses;
    }

    public void setSes(String ses) {
        this.ses = ses;
    }

    public String getApa() {
        return apa;
    }

    public void setApa(String apa) {
        this.apa = apa;
    }

    public String getAma() {
        return ama;
    }

    public void setAma(String ama) {
        this.ama = ama;
    }

    public String getEma() {
        return ema;
    }

    public void setEma(String ema) {
        this.ema = ema;
    }

    public int getTer() {
        return ter;
    }

    public void setTer(int ter) {
        this.ter = ter;
    }

    public Date getFecUVis() {
        return fecUVis;
    }

    public void setFecUVis(Date fecUVis) {
        this.fecUVis = fecUVis;
    }

    public Date getFecAct() {
        return fecAct;
    }

    public void setFecAct(Date fecAct) {
        this.fecAct = fecAct;
    }

    public String gettFij() {
        return tFij;
    }

    public void settFij(String tFij) {
        this.tFij = tFij;
    }

    public String gettCel() {
        return tCel;
    }

    public void settCel(String tCel) {
        this.tCel = tCel;
    }

    public String getEciv() {
        return eciv;
    }

    public void setEciv(String eciv) {
        this.eciv = eciv;
    }

    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(Integer estatus) {
        this.estatus = estatus;
    }

    public String getImgAsoc() {
        return imgAsoc;
    }

    public void setImgAsoc(String imgAsoc) {
        this.imgAsoc = imgAsoc;
    }

    public Long getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(Long idPerfil) {
        this.idPerfil = idPerfil;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.idAsoc);
        hash = 37 * hash + Objects.hashCode(this.noAsoc);
        hash = 37 * hash + Objects.hashCode(this.nom);
        hash = 37 * hash + Objects.hashCode(this.gen);
        hash = 37 * hash + Objects.hashCode(this.fecIng);
        hash = 37 * hash + Objects.hashCode(this.usr);
        hash = 37 * hash + Objects.hashCode(this.psw);
        hash = 37 * hash + this.Int;
        hash = 37 * hash + Objects.hashCode(this.apa);
        hash = 37 * hash + Objects.hashCode(this.ama);
        hash = 37 * hash + Objects.hashCode(this.ema);
        hash = 37 * hash + this.ter;
        hash = 37 * hash + Objects.hashCode(this.fecAct);
        hash = 37 * hash + Objects.hashCode(this.tFij);
        hash = 37 * hash + Objects.hashCode(this.tCel);
        hash = 37 * hash + Objects.hashCode(this.eciv);
        hash = 37 * hash + Objects.hashCode(this.estatus);
        hash = 37 * hash + Objects.hashCode(this.winNumber);
        hash = 37 * hash + Objects.hashCode(this.supervisor);
        hash = 37 * hash + Objects.hashCode(this.imgAsoc);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Asociado other = (Asociado) obj;
        if (this.Int != other.Int) {
            return false;
        }
        if (this.ter != other.ter) {
            return false;
        }
        if (!Objects.equals(this.noAsoc, other.noAsoc)) {
            return false;
        }
        if (!Objects.equals(this.nom, other.nom)) {
            return false;
        }
        if (!Objects.equals(this.gen, other.gen)) {
            return false;
        }
        if (!Objects.equals(this.usr, other.usr)) {
            return false;
        }
        if (!Objects.equals(this.psw, other.psw)) {
            return false;
        }
        if (!Objects.equals(this.apa, other.apa)) {
            return false;
        }
        if (!Objects.equals(this.ama, other.ama)) {
            return false;
        }
        if (!Objects.equals(this.ema, other.ema)) {
            return false;
        }
        if (!Objects.equals(this.tFij, other.tFij)) {
            return false;
        }
        if (!Objects.equals(this.tCel, other.tCel)) {
            return false;
        }
        if (!Objects.equals(this.eciv, other.eciv)) {
            return false;
        }
        if (!Objects.equals(this.imgAsoc, other.imgAsoc)) {
            return false;
        }
        if (!Objects.equals(this.idAsoc, other.idAsoc)) {
            return false;
        }
        if (!DateUtil.compareDate(this.fecIng, other.fecIng)) {
            return false;
        }
        if (!DateUtil.compareDate(this.fecAct, other.fecAct)) {
            return false;
        }
        if (!Objects.equals(this.estatus, other.estatus)) {
            return false;
        }
        if (!Objects.equals(this.winNumber, other.winNumber)) {
            return false;
        }
        if (!Objects.equals(this.supervisor, other.supervisor)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Asociado{" + "idAsoc=" + idAsoc + ", cvePuesto=" + cvePuesto + ", noAsoc=" + noAsoc + ", nom=" + nom + ", gen=" + gen + ", fecNac=" + fecNac + ", fecIng=" + fecIng + ", fecAlta=" + fecAlta + ", fecUMod=" + fecUMod + ", usr=" + usr + ", psw=" + psw + ", pswCad=" + pswCad + ", int1=" + Int + ", ses=" + ses + ", apa=" + apa + ", ama=" + ama + ", ema=" + ema + ", ter=" + ter + ", fecUVis=" + fecUVis + ", fecAct=" + fecAct + ", tFij=" + tFij + ", tCel=" + tCel + ", eciv=" + eciv + ", estatus=" + estatus + ", imgAsoc=" + imgAsoc + ", idPerfil=" + idPerfil + '}';
    }

    

    
    
}
