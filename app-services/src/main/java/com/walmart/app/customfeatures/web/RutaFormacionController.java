/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.web;

import com.walmart.app.customfeatures.business.RutaFormacionBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author alan
 */
@RestController
@RequestMapping("/ruta-formacion")
public class RutaFormacionController {
    
    @Autowired
    private RutaFormacionBusiness rutaFormacionBusiness;
    
    @PostMapping("/upload")
    public ResponseEntity<Integer> uploadRutaFormacionMasiva(@RequestParam("file") MultipartFile file){
        Integer responseValue = 0;
        try{
            this.rutaFormacionBusiness.cargaMasivaRutaFormacion(file);
            responseValue = 1;
        }catch(Exception e){
            e.printStackTrace();
        }
        return new ResponseEntity<>(responseValue,HttpStatus.OK);
    }
    
}
