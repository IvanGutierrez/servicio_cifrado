package com.walmart.app.customfeatures.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ContentDetailsDTO implements Serializable {

    private String name;
    private String contentSource;
    private String contentUrn;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContentSource() {
        return contentSource;
    }

    public void setContentSource(String contentSource) {
        this.contentSource = contentSource;
    }

    public String getContentUrn() {
        return contentUrn;
    }

    public void setContentUrn(String contentUrn) {
        this.contentUrn = contentUrn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContentDetailsDTO that = (ContentDetailsDTO) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(contentSource, that.contentSource) &&
                Objects.equals(contentUrn, that.contentUrn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, contentSource, contentUrn);
    }

    @Override
    public String toString() {
        return "ContentDetailsDTO{" +
                "name='" + name + '\'' +
                ", contentSource='" + contentSource + '\'' +
                ", contentUrn='" + contentUrn + '\'' +
                '}';
    }

}
