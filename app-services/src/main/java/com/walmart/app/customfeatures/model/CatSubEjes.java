/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.model;

import java.util.Objects;

/**
 *
 * @author alan
 */
@CustomTable(name = "CatSubEjes")
public class CatSubEjes {

    
    private Long IdSubEje;
    @CustomColumn(name = "IdEje")
    private Long IdEje;
    @CustomColumn(name = "DesSubeje")
    private String DesSubeje;
    @CustomColumn(name = "IdEst")
    private Integer IdEst;

    public Long getIdSubEje() {
        return IdSubEje;
    }

    public void setIdSubEje(Long IdSubEje) {
        this.IdSubEje = IdSubEje;
    }

    public Long getIdEje() {
        return IdEje;
    }

    public void setIdEje(Long IdEje) {
        this.IdEje = IdEje;
    }

    public String getDesSubeje() {
        return DesSubeje;
    }

    public void setDesSubeje(String DesSubeje) {
        this.DesSubeje = DesSubeje;
    }

    public Integer getIdEst() {
        return IdEst;
    }

    public void setIdEst(Integer IdEst) {
        this.IdEst = IdEst;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.IdSubEje);
        hash = 97 * hash + Objects.hashCode(this.IdEje);
        hash = 97 * hash + Objects.hashCode(this.DesSubeje);
        hash = 97 * hash + Objects.hashCode(this.IdEst);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CatSubEjes other = (CatSubEjes) obj;
        if (!Objects.equals(this.DesSubeje, other.DesSubeje)) {
            return false;
        }
        if (!Objects.equals(this.IdSubEje, other.IdSubEje)) {
            return false;
        }
        if (!Objects.equals(this.IdEje, other.IdEje)) {
            return false;
        }
        if (!Objects.equals(this.IdEst, other.IdEst)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CatSubEjes{" + "IdSubEje=" + IdSubEje + ", IdEje=" + IdEje + ", DesSubeje=" + DesSubeje + ", IdEst=" + IdEst + '}';
    }

}
