/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.model;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author alan
 */
@CustomTable(name = "CatPais")
public class CatPais {

    @CustomColumn(name = "CvePais")
    private Long CvePais;
    @CustomColumn(name = "NomPais")
    private String NomPais;
    @CustomColumn(name = "FecAlta")
    private Date FecAlta;
    @CustomColumn(name = "FecUMod")
    private Date FecUMod;
    @CustomColumn(name = "IdPais")
    private String IdPais;

    public Long getCvePais() {
        return CvePais;
    }

    public void setCvePais(Long CvePais) {
        this.CvePais = CvePais;
    }

    public String getNomPais() {
        return NomPais;
    }

    public void setNomPais(String nomPais) {
        NomPais = nomPais;
    }

    public Date getFecAlta() {
        return FecAlta;
    }

    public void setFecAlta(Date FecAlta) {
        this.FecAlta = FecAlta;
    }

    public Date getFecUMod() {
        return FecUMod;
    }

    public void setFecUMod(Date FecUMod) {
        this.FecUMod = FecUMod;
    }

    public String getIdPais() {
        return IdPais;
    }

    public void setIdPais(String IdPais) {
        this.IdPais = IdPais;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.CvePais);
        hash = 97 * hash + Objects.hashCode(this.NomPais);
        hash = 97 * hash + Objects.hashCode(this.FecAlta);
        hash = 97 * hash + Objects.hashCode(this.FecUMod);
        hash = 97 * hash + Objects.hashCode(this.IdPais);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CatPais other = (CatPais) obj;
        if (!Objects.equals(this.IdPais, other.IdPais)) {
            return false;
        }
        if (!Objects.equals(this.CvePais, other.CvePais)) {
            return false;
        }
        if (!Objects.equals(this.NomPais, other.NomPais)) {
            return false;
        }
        if (!Objects.equals(this.FecAlta, other.FecAlta)) {
            return false;
        }
        if (!Objects.equals(this.FecUMod, other.FecUMod)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CatPais{" + "CvePais=" + CvePais + ", NomPais=" + NomPais + ", FecAlta=" + FecAlta + ", FecUMod=" + FecUMod + ", IdPais=" + IdPais + '}';
    }

}
