/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.repository.impl;

import com.walmart.app.customfeatures.model.CatPuestos;
import com.walmart.app.customfeatures.repository.CatPuestosRepository;
import com.walmart.app.customfeatures.repository.config.CustomBeanProperty;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;


/**
 *
 * @author Alan
 */
@Repository
public class CatPuestosRepositoryImpl implements CatPuestosRepository {

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public List<CatPuestos> findByNomPuesto(String nomPuesto) {
        String query = "SELECT CvePuesto, CveNivDL, NomPuesto, FecAlta,FechUMod FROM CatPuestos WHERE NomPuesto = :NomPuesto";
        Map<String, Object> params = new HashMap<>();
        params.put("NomPuesto", nomPuesto);
        return this.namedParameterJdbcTemplate.query(query, params, new BeanPropertyRowMapper<>(CatPuestos.class));
    }

    public Long getLastId() {
        String query = "SELECT TOP 1 CvePuesto FROM CatPuestos ORDER BY CvePuesto desc";
        return this.namedParameterJdbcTemplate.queryForObject(query, new HashMap<>(), Long.class);
    }

    public Long save(CatPuestos catPuestos) {
        String sentence = "INSERT INTO CatPuestos(CvePuesto,CveNivDL,NomPuesto,FecAlta,FechUMod,SFCvePuesto) VALUES (:CvePuesto,:CveNivDL,:NomPuesto,:FecAlta,:FechUMod,:SFCvePuesto)";
        this.namedParameterJdbcTemplate.update(sentence, new CustomBeanProperty(catPuestos));
        return catPuestos.getCvePuesto();
    }

    @Autowired
    @Override
    public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

}
