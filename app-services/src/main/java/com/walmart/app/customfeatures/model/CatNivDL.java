/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.model;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author alan
 */
@CustomTable(name = "CatNivDL")
public class CatNivDL {

    @CustomColumn(name = "CveNivDL")
    private Long CveNivDL;
    @CustomColumn(name = "cveDepto")
    private Long cveDepto;
    @CustomColumn(name = "NomNivDL")
    private String NomNivDL;
    @CustomColumn(name = "FecAlta")
    private Date FecAlta;
    @CustomColumn(name = "FecUMod")
    private Date FecUMod;

    public Long getCveNivDL() {
        return CveNivDL;
    }

    public void setCveNivDL(Long CveNivDL) {
        this.CveNivDL = CveNivDL;
    }

    public Long getCveDepto() {
        return cveDepto;
    }

    public void setCveDepto(Long cveDepto) {
        this.cveDepto = cveDepto;
    }

    public String getNomNivDL() {
        return NomNivDL;
    }

    public void setNomNivDL(String NomNivDL) {
        this.NomNivDL = NomNivDL;
    }

    public Date getFecAlta() {
        return FecAlta;
    }

    public void setFecAlta(Date FecAlta) {
        this.FecAlta = FecAlta;
    }

    public Date getFecUMod() {
        return FecUMod;
    }

    public void setFecUMod(Date FecUMod) {
        this.FecUMod = FecUMod;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.CveNivDL);
        hash = 79 * hash + Objects.hashCode(this.cveDepto);
        hash = 79 * hash + Objects.hashCode(this.NomNivDL);
        hash = 79 * hash + Objects.hashCode(this.FecAlta);
        hash = 79 * hash + Objects.hashCode(this.FecUMod);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CatNivDL other = (CatNivDL) obj;
        if (!Objects.equals(this.NomNivDL, other.NomNivDL)) {
            return false;
        }
        if (!Objects.equals(this.CveNivDL, other.CveNivDL)) {
            return false;
        }
        if (!Objects.equals(this.cveDepto, other.cveDepto)) {
            return false;
        }
        if (!Objects.equals(this.FecAlta, other.FecAlta)) {
            return false;
        }
        if (!Objects.equals(this.FecUMod, other.FecUMod)) {
            return false;
        }
        return true;
    }
    
    

}
