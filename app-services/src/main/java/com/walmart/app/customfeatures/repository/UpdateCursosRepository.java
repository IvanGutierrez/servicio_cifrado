package com.walmart.app.customfeatures.repository;

public interface UpdateCursosRepository {

    int updateCursosIncompletos();
}
