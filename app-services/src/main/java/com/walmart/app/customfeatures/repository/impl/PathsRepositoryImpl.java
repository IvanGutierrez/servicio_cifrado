/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.repository.impl;

import com.walmart.app.customfeatures.repository.PathsRepository;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author alan
 */
@Repository
public class PathsRepositoryImpl implements PathsRepository {

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public List<Map<String, Object>> findKeyRutaFormacion(Map<String, Long> params) {
        final StringBuilder query = new StringBuilder("SELECT IdPat FROM Paths WHERE ");
        String queryConditions = params.entrySet().stream().map(entry -> entry.getKey().concat(" = :").concat(entry.getKey())).collect(Collectors.joining(" AND "));
        query.append(queryConditions);
        return this.namedParameterJdbcTemplate.queryForList(query.toString(), params);
    }
    
    @Override
    public List<Map<String, Object>> findCursoAndPatIdOnRelPathCur(Object idPat) {
        final StringBuilder query = new StringBuilder("SELECT IdPat,IdCur FROM RelPatCur WHERE IdPat = :IdPat");
        Map<String,Object> params = new HashMap<>();
        params.put("IdPat", idPat);
        return this.namedParameterJdbcTemplate.queryForList(query.toString(), params);
    }

    @Autowired
    @Override
    public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

}
