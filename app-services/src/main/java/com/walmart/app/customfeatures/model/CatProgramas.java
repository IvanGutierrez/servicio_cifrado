/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.model;

import java.util.Objects;

/**
 *
 * @author alan
 */
@CustomTable(name = "CatProgramas")
public class CatProgramas {

    
    private Long IdPro;
    @CustomColumn(name = "Pro")
    private String Pro;
    @CustomColumn(name = "DesPro")
    private String DesPro;
    @CustomColumn(name = "IdEst")
    private Integer IdEst;

    public Long getIdPro() {
        return IdPro;
    }

    public void setIdPro(Long IdPro) {
        this.IdPro = IdPro;
    }

    public String getPro() {
        return Pro;
    }

    public void setPro(String Pro) {
        this.Pro = Pro;
    }

    public String getDesPro() {
        return DesPro;
    }

    public void setDesPro(String DesPro) {
        this.DesPro = DesPro;
    }

    public Integer getIdEst() {
        return IdEst;
    }

    public void setIdEst(Integer IdEst) {
        this.IdEst = IdEst;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(this.IdPro);
        hash = 37 * hash + Objects.hashCode(this.Pro);
        hash = 37 * hash + Objects.hashCode(this.DesPro);
        hash = 37 * hash + Objects.hashCode(this.IdEst);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CatProgramas other = (CatProgramas) obj;
        if (!Objects.equals(this.Pro, other.Pro)) {
            return false;
        }
        if (!Objects.equals(this.DesPro, other.DesPro)) {
            return false;
        }
        if (!Objects.equals(this.IdPro, other.IdPro)) {
            return false;
        }
        if (!Objects.equals(this.IdEst, other.IdEst)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CatProgramas{" + "IdPro=" + IdPro + ", Pro=" + Pro + ", DesPro=" + DesPro + ", IdEst=" + IdEst + '}';
    }

}
