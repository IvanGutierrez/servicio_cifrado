package com.walmart.app.customfeatures.enums;

import java.util.Arrays;

public enum PaisEnum {
    MEXICO(2L,"@MX.wal-mart.com"),
    CAM(5L,"@CAM.wal-mart.com");
    private final Long paisId;
    private final String emailDomain;

    PaisEnum(Long paisId, String emailDomain) {
        this.paisId = paisId;
        this.emailDomain = emailDomain;
    }

    public static PaisEnum getInstance(Long paisId){
        return Arrays.stream(PaisEnum.values()).filter(e -> e.getPaisId().equals(paisId)).findFirst().orElseThrow(() -> new RuntimeException("Unable to get the domain based on the pais id: "+paisId));
    }

    public Long getPaisId() {
        return paisId;
    }

    public String getEmailDomain() {
        return emailDomain;
    }
}
