package com.walmart.app.customfeatures.business;

import com.walmart.app.customfeatures.repository.UpdateCursosRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class UpdateCursosScheduled {


    private UpdateCursosRepository updateCursosRepository;

    private Logger log = LoggerFactory.getLogger(UpdateCursosScheduled.class);

    // @Scheduled(cron = "${procesos-masivos.cron.update-cursos}", zone = "Mexico/General")
    public void executeCursosIncompletos(){
        this.log.info("executeCursosIncompletos desactivado");
        this.log.info("Actualizando cursos que están al 99%...");
        int result = this.updateCursosRepository.updateCursosIncompletos();
        this.log.info("Actualizacion completa!. Rows Afectados {}",result);
    }

    @Autowired
    public void setUpdateCursosRepository(UpdateCursosRepository updateCursosRepository) {
        this.updateCursosRepository = updateCursosRepository;
    }
}
