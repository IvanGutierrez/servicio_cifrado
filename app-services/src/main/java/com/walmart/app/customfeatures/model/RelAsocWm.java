/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.model;

import java.util.Objects;

/**
 *
 * @author alan
 */
@CustomTable(name = "RelAsocWm")
public class RelAsocWm {

    private Long IdRelAsocWm;
    @CustomColumn(name = "IdAsoc")
    private Long IdAsoc;
    @CustomColumn(name = "CvePuesto")
    private Long CvePuesto;
    @CustomColumn(name = "CveNivDL")
    private Long CveNivDL;
    @CustomColumn(name = "cveDepto")
    private Long cveDepto;
    @CustomColumn(name = "cveDeter")
    private Long cveDeter;
    @CustomColumn(name = "cveDistrito")
    private Long cveDistrito;
    @CustomColumn(name = "CveRegion")
    private Long CveRegion;
    @CustomColumn(name = "CveNegocio")
    private Long CveNegocio;
    @CustomColumn(name = "CveZE")
    private Long CveZE;
    @CustomColumn(name = "CvePais")
    private Long CvePais;

    public RelAsocWm() {

    }

    public RelAsocWm(Long IdAsoc, Long CvePuesto, Long CveNivDL, Long cveDepto, Long cveDeter, Long cveDistrito, Long CveRegion, Long CveNegocio, Long CveZE, Long CvePais) {
        this.IdAsoc = IdAsoc;
        this.CvePuesto = CvePuesto;
        this.CveNivDL = CveNivDL;
        this.cveDepto = cveDepto;
        this.cveDeter = cveDeter;
        this.cveDistrito = cveDistrito;
        this.CveRegion = CveRegion;
        this.CveNegocio = CveNegocio;
        this.CveZE = CveZE;
        this.CvePais = CvePais;
    }

    public Long getIdRelAsocWm() {
        return IdRelAsocWm;
    }

    public void setIdRelAsocWm(Long IdRelAsocWm) {
        this.IdRelAsocWm = IdRelAsocWm;
    }

    public Long getIdAsoc() {
        return IdAsoc;
    }

    public void setIdAsoc(Long IdAsoc) {
        this.IdAsoc = IdAsoc;
    }

    public Long getCvePuesto() {
        return CvePuesto;
    }

    public void setCvePuesto(Long CvePuesto) {
        this.CvePuesto = CvePuesto;
    }

    public Long getCveNivDL() {
        return CveNivDL;
    }

    public void setCveNivDL(Long CveNivDL) {
        this.CveNivDL = CveNivDL;
    }

    public Long getCveDepto() {
        return cveDepto;
    }

    public void setCveDepto(Long cveDepto) {
        this.cveDepto = cveDepto;
    }

    public Long getCveDeter() {
        return cveDeter;
    }

    public void setCveDeter(Long cveDeter) {
        this.cveDeter = cveDeter;
    }

    public Long getCveDistrito() {
        return cveDistrito;
    }

    public void setCveDistrito(Long cveDistrito) {
        this.cveDistrito = cveDistrito;
    }

    public Long getCveRegion() {
        return CveRegion;
    }

    public void setCveRegion(Long CveRegion) {
        this.CveRegion = CveRegion;
    }

    public Long getCveNegocio() {
        return CveNegocio;
    }

    public void setCveNegocio(Long CveNegocio) {
        this.CveNegocio = CveNegocio;
    }

    public Long getCveZE() {
        return CveZE;
    }

    public void setCveZE(Long CveZE) {
        this.CveZE = CveZE;
    }

    public Long getCvePais() {
        return CvePais;
    }

    public void setCvePais(Long CvePais) {
        this.CvePais = CvePais;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.IdAsoc);
        hash = 97 * hash + Objects.hashCode(this.CvePuesto);
        hash = 97 * hash + Objects.hashCode(this.CveNivDL);
        hash = 97 * hash + Objects.hashCode(this.cveDepto);
        hash = 97 * hash + Objects.hashCode(this.cveDeter);
        hash = 97 * hash + Objects.hashCode(this.cveDistrito);
        hash = 97 * hash + Objects.hashCode(this.CveRegion);
        hash = 97 * hash + Objects.hashCode(this.CveNegocio);
        hash = 97 * hash + Objects.hashCode(this.CveZE);
        hash = 97 * hash + Objects.hashCode(this.CvePais);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RelAsocWm other = (RelAsocWm) obj;
        if (!Objects.equals(this.IdAsoc, other.IdAsoc)) {
            return false;
        }
        if (!Objects.equals(this.CvePuesto, other.CvePuesto)) {
            return false;
        }
        if (!Objects.equals(this.CveNivDL, other.CveNivDL)) {
            return false;
        }
        if (!Objects.equals(this.cveDepto, other.cveDepto)) {
            return false;
        }
        if (!Objects.equals(this.cveDeter, other.cveDeter)) {
            return false;
        }
        if (!Objects.equals(this.cveDistrito, other.cveDistrito)) {
            return false;
        }
        if (!Objects.equals(this.CveRegion, other.CveRegion)) {
            return false;
        }
        if (!Objects.equals(this.CveNegocio, other.CveNegocio)) {
            return false;
        }
        if (!Objects.equals(this.CveZE, other.CveZE)) {
            return false;
        }
        if (!Objects.equals(this.CvePais, other.CvePais)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "RelAsocWm{" + "IdRelAsocWm=" + IdRelAsocWm + ", IdAsoc=" + IdAsoc + ", CvePuesto=" + CvePuesto + ", CveNivDL=" + CveNivDL + ", cveDepto=" + cveDepto + ", cveDeter=" + cveDeter + ", cveDistrito=" + cveDistrito + ", CveRegion=" + CveRegion + ", CveNegocio=" + CveNegocio + ", CveZE=" + CveZE + ", CvePais=" + CvePais + '}';
    }

}
