/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.model;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author alan
 */
@CustomTable(name = "CatDeptos")
public class CatDeptos {

    @CustomColumn(name = "cveDepto")
    private Long cveDepto;
    @CustomColumn(name = "cveDeter")
    private Long cveDeter;
    @CustomColumn(name = "NomDepto")
    private String NomDepto;
    @CustomColumn(name = "FecAlta")
    private Date FecAlta;
    @CustomColumn(name = "FecUMod")
    private Date FecUMod;
    @CustomColumn(name="SFCveDepto")
    private String SFCveDepto;

    public String getSFCveDepto() {
        return SFCveDepto;
    }

    public void setSFCveDepto(String SFCveDepto) {
        this.SFCveDepto = SFCveDepto;
    }

    public Long getCveDepto() {
        return cveDepto;
    }

    public void setCveDepto(Long cveDepto) {
        this.cveDepto = cveDepto;
    }

    public Long getCveDeter() {
        return cveDeter;
    }

    public void setCveDeter(Long cveDeter) {
        this.cveDeter = cveDeter;
    }

    public String getNomDepto() {
        return NomDepto;
    }

    public void setNomDepto(String NomDepto) {
        this.NomDepto = NomDepto;
    }

    public Date getFecAlta() {
        return FecAlta;
    }

    public void setFecAlta(Date FecAlta) {
        this.FecAlta = FecAlta;
    }

    public Date getFecUMod() {
        return FecUMod;
    }

    public void setFecUMod(Date FecUMod) {
        this.FecUMod = FecUMod;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.cveDepto);
        hash = 79 * hash + Objects.hashCode(this.cveDeter);
        hash = 79 * hash + Objects.hashCode(this.NomDepto);
        hash = 79 * hash + Objects.hashCode(this.FecAlta);
        hash = 79 * hash + Objects.hashCode(this.FecUMod);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CatDeptos other = (CatDeptos) obj;
        if (!Objects.equals(this.NomDepto, other.NomDepto)) {
            return false;
        }
        if (!Objects.equals(this.cveDepto, other.cveDepto)) {
            return false;
        }
        if (!Objects.equals(this.cveDeter, other.cveDeter)) {
            return false;
        }
        if (!Objects.equals(this.FecAlta, other.FecAlta)) {
            return false;
        }
        if (!Objects.equals(this.FecUMod, other.FecUMod)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CatDeptos{" + "cveDepto=" + cveDepto + ", cveDeter=" + cveDeter + ", NomDepto=" + NomDepto + ", FecAlta=" + FecAlta + ", FecUMod=" + FecUMod + '}';
    }

}
