package com.walmart.app.customfeatures.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LearnerDetailsDTO implements Serializable {

    private String name;
    private String email;
    private String uniqueUserId;
    private Map<String, List<String>> customAttributes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUniqueUserId() {
        return uniqueUserId;
    }

    public void setUniqueUserId(String uniqueUserId) {
        this.uniqueUserId = uniqueUserId;
    }

    public Map<String, List<String>> getCustomAttributes() {
        return customAttributes;
    }

    public void setCustomAttributes(Map<String, List<String>> customAttributes) {
        this.customAttributes = customAttributes;
    }

}
