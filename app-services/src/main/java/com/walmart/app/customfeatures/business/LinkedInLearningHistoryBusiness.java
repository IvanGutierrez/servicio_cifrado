package com.walmart.app.customfeatures.business;

import com.walmart.app.customfeatures.dto.ActivitiesDTO;
import com.walmart.app.customfeatures.dto.ElementsDTO;
import com.walmart.app.customfeatures.dto.LearningActivityReportRequestDTO;
import com.walmart.app.customfeatures.dto.LearningActivityReportResponseDTO;
import com.walmart.app.customfeatures.enums.ActivitiesEnum;
import com.walmart.app.customfeatures.enums.CustomAttributeEnum;
import com.walmart.app.customfeatures.model.LinkedInLearningHistory;
import com.walmart.app.customfeatures.repository.LinkedInLearningHistoryRepository;
import com.walmart.app.customfeatures.util.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URISyntaxException;
import java.util.*;

@Service
public class LinkedInLearningHistoryBusiness {

    private LinkedInLearningHistoryRepository linkedInLearningRepository;

    private Logger log = LoggerFactory.getLogger(LinkedInLearningHistoryBusiness.class);

    private volatile String tokenOnMemory;

    public void getLinkedInReportFromLinkedInApiREST(){
        Calendar initDate = Calendar.getInstance();
        initDate.clear(Calendar.HOUR);
        initDate.clear(Calendar.MINUTE);
        initDate.clear(Calendar.SECOND);
        initDate.clear(Calendar.MILLISECOND);

        Integer currentDay = 1;
        Integer twoYears = 550;
        Integer days = 5;
        Integer totalDaysInTheYear = this.linkedInLearningRepository.isLinkedInLearningEmpty() ? twoYears : days;
        this.log.info("Getting {} days of history ",totalDaysInTheYear);
        initDate.add(Calendar.DAY_OF_YEAR , (totalDaysInTheYear*-1));
        this.log.info("Getting the information since {} ",DateUtil.getDateStrFromMilliSeconds(initDate.getTimeInMillis()));
        do{
            try{
                loadLinkedInLearningHistory(initDate.getTimeInMillis());
            }catch(Exception e){
                this.log.error(e.getMessage());
            }
            currentDay++;
            initDate.add(Calendar.DAY_OF_YEAR , 1);
        }while (currentDay <= totalDaysInTheYear);
    }

    public void loadLinkedInLearningHistory(Long timeInMillis) throws URISyntaxException {
        this.log.info("-------> Date {}",DateUtil.getDateStrFromMilliSeconds(timeInMillis));
        this.log.info("-------> Date in millis {} ",timeInMillis);
        this.log.info("First Hit...");
        LearningActivityReportResponseDTO firstHit =
                this.linkedInLearningRepository.getLearningActivityFromLinkedInService(buildLinkedinReportRequestDTO(null,timeInMillis));
        processElementsFromLinkedinReport(firstHit,timeInMillis);
        if(firstHit.getPaging().getTotal()>firstHit.getPaging().getCount()){
            Integer nextStartItem = firstHit.getPaging().getStart() + firstHit.getPaging().getCount();
            LearningActivityReportResponseDTO responseDTO = null;
            do{
                this.log.info("Page number {} of {}",nextStartItem,firstHit.getPaging().getTotal());
                responseDTO =
                        this.linkedInLearningRepository.getLearningActivityFromLinkedInService(buildLinkedinReportRequestDTO(nextStartItem,timeInMillis));
                processElementsFromLinkedinReport(responseDTO,timeInMillis);
                nextStartItem += firstHit.getPaging().getCount();
            }while(!responseDTO.getElements().isEmpty());
        }else{
            this.log.warn("Total elements {} is <= than count {}",firstHit.getPaging().getTotal(),firstHit.getPaging().getCount());
        }
    }

    private LearningActivityReportRequestDTO buildLinkedinReportRequestDTO(Integer start,Long startedAt){
        LearningActivityReportRequestDTO request = new LearningActivityReportRequestDTO(startedAt);
        if(this.tokenOnMemory == null ){
            this.tokenOnMemory = this.linkedInLearningRepository.getToken();
        }
        request.setToken(this.tokenOnMemory);
        if(start != null){
            request.setStart(start);
        }
        return request;
    }

    private void processElementsFromLinkedinReport(LearningActivityReportResponseDTO responseDTO,Long startedAtInMillis){
        this.log.info("Total Rows {}",responseDTO.getPaging().getTotal());
        this.log.info("Count per page {}",responseDTO.getPaging().getCount());
        this.log.info("Page {}",responseDTO.getPaging().getStart());
        for(ElementsDTO element : responseDTO.getElements()){
            try{
                saveOrUpdateLinkedInReport(getLinkedInLearningHistoryFromElementDTO(element,startedAtInMillis));
            }catch(Exception e){
                this.log.error(e.getMessage());
            }

        }
    }

    private void saveOrUpdateLinkedInReport(LinkedInLearningHistory linkedInLearningHistory){
        Optional<LinkedInLearningHistory> linkedInLearningHistoryOptional = this.linkedInLearningRepository.findByWinNumberAndContentUrn(linkedInLearningHistory);
        if(linkedInLearningHistoryOptional.isPresent()){
            LinkedInLearningHistory inLearningHistoryOnDB = linkedInLearningHistoryOptional.get();
            if(linkedInLearningHistory.getPorcentajeRealizacion() > inLearningHistoryOnDB.getPorcentajeRealizacion()){
                this.linkedInLearningRepository.update(linkedInLearningHistory);
            }
        }else{
            this.linkedInLearningRepository.save(linkedInLearningHistory);
        }
    }

    private LinkedInLearningHistory getLinkedInLearningHistoryFromElementDTO(ElementsDTO element, Long startedAtInMillis){
        LinkedInLearningHistory linkedInLearningHistoryBase = new LinkedInLearningHistory();
        linkedInLearningHistoryBase.setEmail(element.getLearnerDetails().getEmail());
        linkedInLearningHistoryBase.setNombreUsuario(element.getLearnerDetails().getName());
        linkedInLearningHistoryBase.setWinNumber(getCustomAttributeFromElementDTO(element));
        linkedInLearningHistoryBase.setNombreCurso(element.getContentDetails().getName());
        linkedInLearningHistoryBase.setContentUrn(element.getContentDetails().getContentUrn());
        linkedInLearningHistoryBase.setFechaHit(DateUtil.getDateStrFromMilliSeconds(startedAtInMillis));

        ActivitiesDTO progress = element.getActivities().stream().filter(activityDTO -> activityDTO.getEngagementType().equals(ActivitiesEnum.PROGRESS_PERCENTAGE.toString())).findFirst().get();
        linkedInLearningHistoryBase.setCompletado(DateUtil.getDateStrFromMilliSeconds(progress.getLastEngagedAt()));
        linkedInLearningHistoryBase.setPorcentajeRealizacion(progress.getEngagementValue().intValue());

        ActivitiesDTO viewed = element.getActivities().stream().filter(activityDTO -> activityDTO.getEngagementType().equals(ActivitiesEnum.SECONDS_VIEWED.toString())).findFirst().get();
        linkedInLearningHistoryBase.setSegundosVisualizacion(viewed.getEngagementValue());
        return linkedInLearningHistoryBase;
    }

    private String getCustomAttributeFromElementDTO(ElementsDTO element){
        return element
                .getLearnerDetails()
                .getCustomAttributes()
                .entrySet()
                .stream()
                .filter(this::filterCustomAttributes)
                .findFirst()
                .orElseThrow(()-> new RuntimeException("WinNumber no present for the user "+element.getLearnerDetails().getEmail()))
                .getValue()
                .stream()
                .findFirst()
                .get();
    }

    private Boolean filterCustomAttributes(Map.Entry<String,List<String>> customAttributeEntryMap){
        if(customAttributeEntryMap.getKey().equals(CustomAttributeEnum.WIN_NUMBER.getValue())){
            Optional<String> valueOptional = customAttributeEntryMap.getValue().stream().findFirst();
            if(valueOptional.isPresent() && !valueOptional.get().equals("NOT_FOUND")){
                return true;
            }
        }
        return false;
    }

    @Autowired
    public void setLinkedInLearningRepository(LinkedInLearningHistoryRepository linkedInLearningRepository) {
        this.linkedInLearningRepository = linkedInLearningRepository;
    }
}
