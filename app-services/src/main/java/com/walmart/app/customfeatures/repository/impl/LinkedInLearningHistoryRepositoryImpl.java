package com.walmart.app.customfeatures.repository.impl;

import com.walmart.app.customfeatures.dto.LearningActivityReportRequestDTO;
import com.walmart.app.customfeatures.dto.LearningActivityReportResponseDTO;
import com.walmart.app.customfeatures.model.CustomColumn;
import com.walmart.app.customfeatures.model.CustomTable;
import com.walmart.app.customfeatures.model.LinkedInLearningHistory;
import com.walmart.app.customfeatures.repository.LinkedInLearningHistoryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.lang.reflect.Field;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;

@Repository
public class LinkedInLearningHistoryRepositoryImpl implements LinkedInLearningHistoryRepository {

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private String urlReport;

    private String urlToken;

    private String clientSecret;

    private String clientId;

    private final Logger log = LoggerFactory.getLogger(LinkedInLearningHistoryRepositoryImpl.class);

    @Override
    public String getToken(){

        RestTemplate restTemplate = new RestTemplate();
        String url = buildTokenURL();
        this.log.info("Getting token... URL[{}]",url);
        ResponseEntity<Map> response = restTemplate.getForEntity(url,Map.class);
        if(response.getStatusCode() != HttpStatus.OK){
            throw new RuntimeException("It was not posible toe get the token "+response.getStatusCode());
        }
        return response.getBody().get("access_token").toString();
    }

    private String buildTokenURL(){
       return new StringBuilder(this.urlToken)
       .append("?grant_type=client_credentials&client_id=")
               .append(this.clientId)
               .append("&client_secret=")
               .append(this.clientSecret).toString();
    }

    private String buildReportURL(LearningActivityReportRequestDTO requestDTO){
        return this.urlReport + "?q="+requestDTO.getQ()
                +"&timeOffset.unit="+requestDTO.getTimeOffsetUnit()
                +"&start="+requestDTO.getStart()
                +"&count="+requestDTO.getCount()
                +"&assetType="+requestDTO.getAssetType()
                +"&startedAt="+requestDTO.getStartedAt()
                +"&aggregationCriteria.primary="+requestDTO.getAggregationCriteriaPrimary()
                +"&aggregationCriteria.secondary="+requestDTO.getAggregationCriteriaSecondary()
                +"&sortBy.engagementMetricType="+requestDTO.getSortByEngagementMetricType()
                +"&timeOffset.duration="+requestDTO.getTimeOffsetDuration();
    }


    @Override
    public LearningActivityReportResponseDTO getLearningActivityFromLinkedInService(LearningActivityReportRequestDTO requestDTO) throws URISyntaxException {
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(requestDTO.getToken());
        RestTemplate restTemplate = new RestTemplate();
        this.log.info("Hitting with {} ",requestDTO.toString());
        ResponseEntity<LearningActivityReportResponseDTO> response = restTemplate.exchange(
                RequestEntity.get(new URI(buildReportURL(requestDTO))).headers(headers).build(),LearningActivityReportResponseDTO.class );
        return response.getBody();
    }


    @Override
    public int save(LinkedInLearningHistory linkedInLearningHistory) {
        Field[] fields = LinkedInLearningHistory.class.getDeclaredFields();
        StringBuilder sentence = new StringBuilder("INSERT INTO LinkedInLearningHistoryX (");
        sentence.append(Arrays.asList(fields).stream().filter(field -> field.getAnnotation(CustomColumn.class) != null).map(field -> field.getAnnotation(CustomColumn.class).name()).collect(Collectors.joining(",")));
        sentence.append(") VALUES (");
        sentence.append(Arrays.asList(fields).stream().filter(field -> field.getAnnotation(CustomColumn.class) != null).map(field -> ":".concat(field.getAnnotation(CustomColumn.class).name())).collect(Collectors.joining(",")));
        sentence.append(")");
        return this.namedParameterJdbcTemplate.update(sentence.toString(), getParamsFromList(linkedInLearningHistory));
    }

    @Override
    public Optional<LinkedInLearningHistory> findByWinNumberAndContentUrn(LinkedInLearningHistory linkedRow){
        String query = "SELECT * FROM LinkedInLearningHistoryX WHERE WinNumber = :WinNumber AND ContentUrn = :ContentUrn";
        List<LinkedInLearningHistory> result = namedParameterJdbcTemplate.query(query,getParamsFromList(linkedRow),new BeanPropertyRowMapper<>(LinkedInLearningHistory.class));
        return result.stream().findFirst();
    }

    @Override
    public int update(LinkedInLearningHistory linkedInLearningHistory){
        String sentence = "UPDATE LinkedInLearningHistoryX SET Completado = :Completado, PorcentajeRealizacion = :PorcentajeRealizacion, SegundosVisualizacion = :SegundosVisualizacion WHERE WinNumber = :WinNumber AND ContentUrn = :ContentUrn";
        return this.namedParameterJdbcTemplate.update(sentence, getParamsFromList(linkedInLearningHistory));
    }

    @Override
    public void deleteHistory(){
        String sentence = "delete from LinkedInLearningHistoryX where 1=1";
        this.namedParameterJdbcTemplate.update(sentence,new HashMap<>());
    }

    @Override
    public Boolean isLinkedInLearningEmpty(){
        String query = "select * from LinkedInLearningHistoryX";
        List<Map<String,Object>> result = this.namedParameterJdbcTemplate.queryForList(query,new HashMap<>());
        Boolean isEmpty = result.isEmpty();
        this.log.info("LinkedInLearningX is empty ? {}",isEmpty);
        return isEmpty;
    }

    private Map<String, Object> getParamsFromList(LinkedInLearningHistory currentParam) {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("Completado", currentParam.getCompletado());
        parameter.put("ContentUrn", currentParam.getContentUrn());
        parameter.put("Email", currentParam.getEmail());
        parameter.put("NombreCurso", currentParam.getNombreCurso());
        parameter.put("NombreUsuario", currentParam.getNombreUsuario());
        parameter.put("PorcentajeRealizacion", currentParam.getPorcentajeRealizacion());
        parameter.put("SegundosVisualizacion", currentParam.getSegundosVisualizacion());
        parameter.put("WinNumber", currentParam.getWinNumber());
        parameter.put("FechaHit", currentParam.getFechaHit());
        return parameter;
    }



    @Value("${linkedin.url.report}")
    public void setUrlReport(String urlReport) {
        this.urlReport = urlReport;
    }

    @Value("${linkedin.url.token}")
    public void setUrlToken(String urlToken) {
        this.urlToken = urlToken;
    }

    @Value("${linkedin.clientSecret}")
    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    @Value("${linkedin.clientId}")
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @Autowired
    public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }
}
