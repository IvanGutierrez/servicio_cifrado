/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.model;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author alan
 */
@CustomTable(name = "catDistritos")
public class CatDistritos {

    @CustomColumn(name = "cveDistrito")
    private Long cveDistrito;
    @CustomColumn(name = "CveRegion")
    private Long CveRegion;
    @CustomColumn(name = "NomDist")
    private String NomDist;
    @CustomColumn(name = "FecAlta")
    private Date FecAlta;
    @CustomColumn(name = "FecUMod")
    private Date FecUMod;

    public Long getCveDistrito() {
        return cveDistrito;
    }

    public void setCveDistrito(Long cveDistrito) {
        this.cveDistrito = cveDistrito;
    }

    public Long getCveRegion() {
        return CveRegion;
    }

    public void setCveRegion(Long CveRegion) {
        this.CveRegion = CveRegion;
    }

    public String getNomDist() {
        return NomDist;
    }

    public void setNomDist(String NomDist) {
        this.NomDist = NomDist;
    }

    public Date getFecAlta() {
        return FecAlta;
    }

    public void setFecAlta(Date FecAlta) {
        this.FecAlta = FecAlta;
    }

    public Date getFecUMod() {
        return FecUMod;
    }

    public void setFecUMod(Date FecUMod) {
        this.FecUMod = FecUMod;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.cveDistrito);
        hash = 79 * hash + Objects.hashCode(this.CveRegion);
        hash = 79 * hash + Objects.hashCode(this.NomDist);
        hash = 79 * hash + Objects.hashCode(this.FecAlta);
        hash = 79 * hash + Objects.hashCode(this.FecUMod);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CatDistritos other = (CatDistritos) obj;
        if (!Objects.equals(this.NomDist, other.NomDist)) {
            return false;
        }
        if (!Objects.equals(this.cveDistrito, other.cveDistrito)) {
            return false;
        }
        if (!Objects.equals(this.CveRegion, other.CveRegion)) {
            return false;
        }
        if (!Objects.equals(this.FecAlta, other.FecAlta)) {
            return false;
        }
        if (!Objects.equals(this.FecUMod, other.FecUMod)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CatDistritos{" + "cveDistrito=" + cveDistrito + ", CveRegion=" + CveRegion + ", NomDist=" + NomDist + ", FecAlta=" + FecAlta + ", FecUMod=" + FecUMod + '}';
    }

}
