/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.model;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author alan
 */
@CustomTable(name = "catDeterminante")
public class CatDeterminante {

    @CustomColumn(name = "cveDeter")
    private Long cveDeter;
    @CustomColumn(name = "cveDistrito")
    private Long cveDistrito;
    @CustomColumn(name = "NomDeter")
    private String NomDeter;
    @CustomColumn(name = "FecAlta")
    private Date FecAlta;
    @CustomColumn(name = "FecUMod")
    private Date FecUMod;
    @CustomColumn(name="SFCveDeter")
    private String SFCveDeter;

    public String getSFCveDeter() {
        return SFCveDeter;
    }

    public void setSFCveDeter(String SFCveDeter) {
        this.SFCveDeter = SFCveDeter;
    }

    public Long getCveDeter() {
        return cveDeter;
    }

    public void setCveDeter(Long cveDeter) {
        this.cveDeter = cveDeter;
    }

    public Long getCveDistrito() {
        return cveDistrito;
    }

    public void setCveDistrito(Long cveDistrito) {
        this.cveDistrito = cveDistrito;
    }

    public String getNomDeter() {
        return NomDeter;
    }

    public void setNomDeter(String NomDeter) {
        this.NomDeter = NomDeter;
    }

    public Date getFecAlta() {
        return FecAlta;
    }

    public void setFecAlta(Date FecAlta) {
        this.FecAlta = FecAlta;
    }

    public Date getFecUMod() {
        return FecUMod;
    }

    public void setFecUMod(Date FecUMod) {
        this.FecUMod = FecUMod;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.cveDeter);
        hash = 83 * hash + Objects.hashCode(this.cveDistrito);
        hash = 83 * hash + Objects.hashCode(this.NomDeter);
        hash = 83 * hash + Objects.hashCode(this.FecAlta);
        hash = 83 * hash + Objects.hashCode(this.FecUMod);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CatDeterminante other = (CatDeterminante) obj;
        if (!Objects.equals(this.NomDeter, other.NomDeter)) {
            return false;
        }
        if (!Objects.equals(this.cveDeter, other.cveDeter)) {
            return false;
        }
        if (!Objects.equals(this.cveDistrito, other.cveDistrito)) {
            return false;
        }
        if (!Objects.equals(this.FecAlta, other.FecAlta)) {
            return false;
        }
        if (!Objects.equals(this.FecUMod, other.FecUMod)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CatDeterminante{" + "cveDeter=" + cveDeter + ", cveDistrito=" + cveDistrito + ", NomDeter=" + NomDeter + ", FecAlta=" + FecAlta + ", FecUMod=" + FecUMod + '}';
    }

}
