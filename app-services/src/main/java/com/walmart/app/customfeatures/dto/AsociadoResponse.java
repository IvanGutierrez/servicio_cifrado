/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.dto;

/**
 *
 * @author Alan
 */
public class AsociadoResponse {
    
    private String usr;
    private Boolean insertOk;
    private Long idAsoc;
    private String errorMessage;

    public Long getIdAsoc() {
        return idAsoc;
    }

    public void setIdAsoc(Long idAsoc) {
        this.idAsoc = idAsoc;
    }
    
    public String getUsr() {
        return usr;
    }

    public void setUsr(String usr) {
        this.usr = usr;
    }

    public Boolean getInsertOk() {
        return insertOk;
    }

    public void setInsertOk(Boolean insertOk) {
        this.insertOk = insertOk;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
    
}
