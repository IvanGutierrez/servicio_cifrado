/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.business;

import com.walmart.app.customfeatures.enums.PaisEnum;
import com.walmart.app.customfeatures.model.CatDeptos;
import com.walmart.app.customfeatures.model.CatDeterminante;
import com.walmart.app.customfeatures.model.CatDistritos;
import com.walmart.app.customfeatures.model.CatEjes;
import com.walmart.app.customfeatures.model.CatNegocio;
import com.walmart.app.customfeatures.model.CatNivDL;
import com.walmart.app.customfeatures.model.CatProgramas;
import com.walmart.app.customfeatures.model.CatPuestos;
import com.walmart.app.customfeatures.model.CatRegiones;
import com.walmart.app.customfeatures.model.CatSubEjes;
import com.walmart.app.customfeatures.model.Cursos;
import com.walmart.app.customfeatures.model.Paths;
import com.walmart.app.customfeatures.model.RelPatCur;
import com.walmart.app.customfeatures.repository.GenericOperationsRepository;
import com.walmart.app.customfeatures.repository.PathsRepository;
import com.walmart.app.customfeatures.util.ConstantesDefinidasPorElPatronConstants;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.apache.commons.io.IOUtils;
import static com.walmart.app.customfeatures.util.RutaFormacionCSVConstants.*;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 *
 * @author alan
 */
@Service
public class RutaFormacionBusiness {

    private TaskExecutor taskExecutor;

    private PathsRepository pathsRepository;

    private GenericOperationsRepository genericRepository;

    private final Long defaultValueCero = 0L;
    private final Long defaultValueUno = 1L;
    private final Long defaultValueDiez = 10L;
    private final Long defaultValueCien = 100L;

    private final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    private Logger log = LoggerFactory.getLogger(RutaFormacionBusiness.class);

    public void cargaMasivaRutaFormacion(MultipartFile file) throws IOException {
        List<String> lines = IOUtils.readLines(file.getInputStream(), StandardCharsets.UTF_8);
        Boolean csvHead = Boolean.TRUE;
        for (String line : lines) {
            if (csvHead) {
                csvHead = Boolean.FALSE;
                continue;
            }
            this.taskExecutor.execute(() -> {
                try {
                    String newLineOnThread = new String(line.replaceAll("\"", "").getBytes());
                    inserRutaFormacion(newLineOnThread);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            });
        }
    }

    public synchronized void inserRutaFormacion(final String line) {
        final String[] values = line.split(",");
        Optional<Cursos> optionalCurso = checkIfCursoExists(values);
        if (!optionalCurso.isPresent()) {
            this.log.info("The course {} does not exist, rutaFormacion wont be inserted", values[CURSO]);
            return;
        }
        final Map<String, Long> keyRutaFormacionMap = buildRutaFormacionKey(values);
        final List<Map<String, Object>> resultRutaFormacionList = this.pathsRepository.findKeyRutaFormacion(keyRutaFormacionMap);
        Paths path = new Paths();
        if (resultRutaFormacionList.isEmpty()) {
            String keyRutaFormacionStr = keyRutaFormacionMap.entrySet().stream().map(entry -> entry.getKey().concat(":").concat(entry.getValue().toString())).collect(Collectors.joining(", "));
            this.log.info("The rutaFormacion {} does not exist, creating...", keyRutaFormacionStr);
            path = savePath(values, keyRutaFormacionMap);
            saveRelPatCur(path, values, optionalCurso.get());
            this.log.info("RelPatCur inserted successfully");
        } else if (!validateIfRutaFormacionIsAlreadyAssociateToCurso(resultRutaFormacionList, optionalCurso.get())) {
            Long patId = Long.valueOf(resultRutaFormacionList.stream().findFirst().get().get("IdPat").toString());
            path = this.genericRepository.findByCustomDesc("IdPat", patId, Paths.class).stream().findFirst().get();
            path.setIdPat(patId);
            saveRelPatCur(path, values, optionalCurso.get());
            this.log.info("RelPatCur inserted successfully");
        } else {
            this.log.info("rutaFormacion is already asigned to the course {}", optionalCurso.get().getIdCur());
        }
    }

    private Boolean validateIfRutaFormacionIsAlreadyAssociateToCurso(final List<Map<String, Object>> resultRutaFormacionList, final Cursos curso) {
        final Map<String, Object> map = resultRutaFormacionList.stream().findFirst().get();
        final List<Map<String, Object>> relPatCurMap = this.pathsRepository.findCursoAndPatIdOnRelPathCur(map.get("IdPat"));
        if (relPatCurMap.isEmpty()) {
            return Boolean.FALSE;
        }
        Long idCursoOnRutaFormacion = Long.valueOf(relPatCurMap.stream().findFirst().get().get("IdCur").toString());
        return idCursoOnRutaFormacion.equals(curso.getIdCur());
    }

    private void saveRelPatCur(final Paths path, final String[] values, Cursos curso) {
        RelPatCur relPatCur = new RelPatCur();
        relPatCur.setIdPat(path.getIdPat());
        relPatCur.setIdCur(curso.getIdCur());
        relPatCur.setConSec(getValue(values[SECUENCIA], Long.class));
        relPatCur.setOrdPat(this.defaultValueCero);
        relPatCur.setClvEst(this.defaultValueUno.intValue());
        relPatCur.setNivCur(getValue(values[NIVEL], Long.class));
        relPatCur.setIdEje(getValue(saveOrGetEje(values[EJE]), Long.class));
        relPatCur.setIdSubEje(getValue(saveOrGetSubEje(values[SUB_EJE], relPatCur.getIdEje()), Long.class));
        relPatCur.setIdPro(getValue(saveOrGetProgram(values[PROGRAMA]), Long.class));
        if (PRIORIDAD < values.length) {
            relPatCur.setPrioridad(getValue(values[PRIORIDAD], Long.class));
        }
        relPatCur.setCvePuesto(path.getCvePuesto());
        try {
            relPatCur.setFecIni(new Timestamp(this.dateFormat.parse(values[FECHA_INICIO]).getTime()));
        } catch (ParseException e) {
            relPatCur.setFecIni(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        }
        relPatCur.setCreditos(getValue(values[CREDITOS], Long.class));
        relPatCur.setCveNivDL(path.getCveNivDL());
        relPatCur.setCveDepto(path.getCveDepto());
        relPatCur.setCveDeter(path.getCveDeter());
        relPatCur.setCveDistrito(path.getCveDistrito());
        relPatCur.setCveRegion(path.getCveRegion());
        relPatCur.setCveNegocio(path.getCveNegocio());
        relPatCur.setCveZE(path.getCveZE());
        relPatCur.setCvePais(path.getCvePais());
        relPatCur.setObligatoria(values[OBLIGATORIA]);
        this.log.info("Saving RelPatCur");
        this.genericRepository.save(relPatCur, RelPatCur.class);
    }

    private Object saveOrGetProgram(String value) {
        List<Map<String, Object>> programasList = this.genericRepository.findByCustomDesc("Pro", value, CatProgramas.class, "IdPro");
        if (!programasList.isEmpty()) {
            this.log.info("Program {} on db", value);
            return programasList.stream().findFirst().get().get("IdPro");
        }
        CatProgramas nuevoPrograma = new CatProgramas();
        nuevoPrograma.setPro(value);
        nuevoPrograma.setDesPro(value);
        nuevoPrograma.setIdEst(this.defaultValueUno.intValue());
        return this.genericRepository.saveAndGetId(nuevoPrograma, CatProgramas.class);
    }

    private Object saveOrGetEje(String value) {
        List<Map<String, Object>> ejesList = this.genericRepository.findByCustomDesc("Eje", value, CatEjes.class, "IdEje");
        if (!ejesList.isEmpty()) {
            this.log.info("Eje {} on db", value);
            return ejesList.stream().findFirst().get().get("IdEje");
        }
        CatEjes eje = new CatEjes();
        eje.setDesEje(value);
        eje.setEje(value);
        eje.setBanSubeje(this.defaultValueCero.intValue());
        eje.setIdEst(this.defaultValueUno.intValue());
        return this.genericRepository.saveAndGetId(eje, CatEjes.class);
    }

    private Object saveOrGetSubEje(String value, Long ejeId) {
        List<Map<String, Object>> subEjesList = this.genericRepository.findByCustomDesc("DesSubeje", value, CatSubEjes.class, "IdSubEje");
        if (!subEjesList.isEmpty()) {
            this.log.info("Subeje {} on db", value);
            return subEjesList.stream().findFirst().get().get("IdSubEje");
        }
        CatSubEjes subEje = new CatSubEjes();
        subEje.setDesSubeje(value);
        subEje.setIdEje(ejeId);
        subEje.setIdEst(this.defaultValueUno.intValue());
        return this.genericRepository.saveAndGetId(subEje, CatSubEjes.class);
    }

    private <T extends Object> T getValue(Object value, Class<T> clazz) {
        return value != null ? clazz.equals(Long.class) ? (T) Long.valueOf(value.toString()) : clazz.cast(value) : null;
    }

    private Paths savePath(final String[] values, final Map<String, Long> keyRutaFormacionMap) {
        final Paths path = new Paths();
        path.setBlog(defaultValueUno);
        path.setCalMin(defaultValueDiez);
        path.setClvEst(defaultValueUno.intValue());
        path.setCurPer(defaultValueUno);
        path.setEva(defaultValueDiez);
        try {
            path.setFecIni(new Timestamp(dateFormat.parse(values[FECHA_INICIO]).getTime()));
        } catch (ParseException e) {
            path.setFecIni(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        }
        path.setFecFin(path.getFecIni());
        path.setFecUMod(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        path.setImgPat("default.png");
        path.setNomPat(values[PROGRAMA]);
        path.setOffOn(defaultValueUno);
        path.setOrdPat(defaultValueCero);
        path.setPtos(defaultValueCien.intValue());
        path.setCvePais(PaisEnum.MEXICO.getPaisId());
        path.setCveZE(ConstantesDefinidasPorElPatronConstants.ZONA_ECONOMICA_DEFAULT_ID);
        path.setCveNegocio(keyRutaFormacionMap.get("CveNegocio"));
        path.setCveRegion(getValueFromCatDB("CveNegocio", path.getCveNegocio(), CatRegiones.class, Boolean.TRUE).getCveRegion());
        path.setCveDistrito(getValueFromCatDB("CveRegion", path.getCveRegion(), CatDistritos.class, Boolean.TRUE).getCveDistrito());
        path.setCveDeter(keyRutaFormacionMap.get("CveDeter"));
        path.setCveDepto(keyRutaFormacionMap.get("CveDepto"));
        path.setCveNivDL(getValueFromCatDB("cveDepto", path.getCveDepto(), CatNivDL.class, Boolean.TRUE).getCveNivDL());
        path.setCvePuesto(keyRutaFormacionMap.get("CvePuesto"));
        this.log.info("Saving Path...");
        Long idPat = this.genericRepository.saveAndGetId(path, Paths.class);
        this.log.info("Path saved []...", idPat);
        path.setIdPat(idPat);
        return path;
    }

    private Optional<Cursos> checkIfCursoExists(final String[] values) {
        List<Cursos> cursos = this.genericRepository.findByCustomDesc("IdRef", values[CURSO], Cursos.class);
        return cursos.stream().findFirst();
    }

    private Map<String, Long> buildRutaFormacionKey(final String[] values) {
        Map<String, Long> keyRutaFormacionMap = new HashMap<>();
        keyRutaFormacionMap.put("CvePais", PaisEnum.MEXICO.getPaisId());
        keyRutaFormacionMap.put("CveNegocio", getValueFromCatDB("NomNegocio", values[NEGOCIO].concat("%"), CatNegocio.class, Boolean.FALSE).getCveNegocio());
        keyRutaFormacionMap.put("CveDeter", getValueFromCatDB("NomDeter", values[DETERMINANTE].concat("%"), CatDeterminante.class, Boolean.FALSE).getCveDeter());
        keyRutaFormacionMap.put("CveDepto", getValueFromCatDB("NomDepto", values[DEPARTAMENTO].concat("%"), CatDeptos.class, Boolean.FALSE).getCveDepto());
        keyRutaFormacionMap.put("CvePuesto", getValueFromCatDB("NomPuesto", values[PUESTO].concat("%"), CatPuestos.class, Boolean.FALSE).getCvePuesto());
        return keyRutaFormacionMap;
    }

    private <T extends Object> T getValueFromCatDB(String column, Object value, Class<T> clazz, Boolean equalsValue) {
        List<T> result = equalsValue ? this.genericRepository.findByCustomDesc(column, value, clazz) : this.genericRepository.findByCustomDescLike(column, value, clazz);
        return result.stream().findFirst().orElseThrow(() -> new RuntimeException("Unable to find the value [" + value + "] in the column [" + column + "]"));
    }

    @Autowired
    @Qualifier("CustomThreadPoolTaskExecutor")
    public void setTaskExecutor(TaskExecutor taskExecutor) {
        this.taskExecutor = taskExecutor;
    }

    @Autowired
    public void setGenericRepository(GenericOperationsRepository genericRepository) {
        this.genericRepository = genericRepository;
    }

    @Autowired
    public void setPathsRepository(PathsRepository pathsRepository) {
        this.pathsRepository = pathsRepository;
    }

}
