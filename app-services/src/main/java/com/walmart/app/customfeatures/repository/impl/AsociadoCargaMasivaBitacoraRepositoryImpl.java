/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.repository.impl;

import com.walmart.app.customfeatures.dto.BitacoraDTO;
import com.walmart.app.customfeatures.repository.AsociadoCargaMasivaBitacoraRepository;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author alan
 */
@Repository
public class AsociadoCargaMasivaBitacoraRepositoryImpl implements AsociadoCargaMasivaBitacoraRepository{
    
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    
    @Override
    public int save(BitacoraDTO bitacoraDTO){
        String sentence = "INSERT INTO asociados_carga_masiva_bitacora(noAsociado,email,detalle_cambio_asociado,detalle_cambio_relasocwm,errorMessage,fecha_modificacion) VALUES (:noAsociado,:email,:asociadoChanges,:asocChanges,:errorMessage,GETDATE())";
        Map<String,Object> parameters = new HashMap<>();
        parameters.put("errorMessage",bitacoraDTO.getErrorMessage());
        parameters.put("email",bitacoraDTO.getEmail());
        parameters.put("noAsociado",bitacoraDTO.getNoAsociado());
        parameters.put("asociadoChanges", bitacoraDTO.getAsociadosChanges() != null ?  bitacoraDTO.getAsociadosChanges().stream().collect(Collectors.joining(", ")) : "error");
        parameters.put("asocChanges", bitacoraDTO.getRelAsocWmChanges() != null ? bitacoraDTO.getRelAsocWmChanges().stream().collect(Collectors.joining(", ")) : "error");
        return namedParameterJdbcTemplate.update(sentence, parameters);
    }
    
    @Override
    @Autowired
    public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }
    
}
