package com.walmart.app.customfeatures.enums;

public enum CustomAttributeEnum {
    WIN_NUMBER("Win Number");
    private final String value;

    CustomAttributeEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
