/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.repository.config;

import com.walmart.app.customfeatures.model.CustomColumn;
import org.springframework.jdbc.core.namedparam.AbstractSqlParameterSource;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.NotReadablePropertyException;
import org.springframework.beans.PropertyAccessorFactory;
import org.springframework.jdbc.core.StatementCreatorUtils;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.util.StringUtils;

/**
 *
 * @author Alan
 */
public class CustomBeanProperty extends AbstractSqlParameterSource {

    private final BeanWrapper beanWrapper;

    @Nullable
    private String[] propertyNames;

    private final Class clazz;

    /**
     * Create a new BeanPropertySqlParameterSource for the given bean.
     *
     * @param object the bean instance to wrap
     */
    public CustomBeanProperty(Object object) {
        this.beanWrapper = PropertyAccessorFactory.forBeanPropertyAccess(object);
        this.clazz = object.getClass();
    }

    @Override
    public boolean hasValue(String paramName) {
        return this.beanWrapper.isReadableProperty(paramName);
    }

    @Override
    @Nullable
    public Object getValue(String paramName) throws IllegalArgumentException {
        try {
            return this.beanWrapper.getPropertyValue(paramName);
        } catch (NotReadablePropertyException ex) {
            throw new IllegalArgumentException(ex.getMessage());
        }
    }

    /**
     * Derives a default SQL type from the corresponding property type.
     *
     * @see
     * org.springframework.jdbc.core.StatementCreatorUtils#javaTypeToSqlParameterType
     */
    @Override
    public int getSqlType(String paramName) {
        int sqlType = super.getSqlType(paramName);
        if (sqlType != TYPE_UNKNOWN) {
            return sqlType;
        }
        Class<?> propType = this.beanWrapper.getPropertyType(paramName);
        return StatementCreatorUtils.javaTypeToSqlParameterType(propType);
    }

    @Override
    @NonNull
    public String[] getParameterNames() {
        return getReadablePropertyNames();
    }

    public String[] getReadablePropertyNames() {
        if (this.propertyNames == null) {
            List<String> names = null;
            names = Arrays.asList(this.clazz.getDeclaredFields()).stream().map(field -> {
                CustomColumn customColumn = field.getAnnotation(CustomColumn.class);
                return customColumn != null ? customColumn.name() : "Column description not found";
            }).collect(Collectors.toList());
            this.propertyNames = StringUtils.toStringArray(names);
        }
        return this.propertyNames;
    }

}
