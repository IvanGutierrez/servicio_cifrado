/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.web;

import com.walmart.app.securitytool.dto.KeyDTO;
import com.walmart.app.customfeatures.business.AsociadoBusiness;
import com.walmart.app.customfeatures.dto.AsociadoResponse;
import com.walmart.app.customfeatures.model.Asociado;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Alan
 */
@RestController
public class AsociadosController {

    @Autowired
    private AsociadoBusiness asociadoBusiness;

    private static final String VECTOR = "vector";
    private static final String KEY = "key";

    @PostMapping("/save")
    public ResponseEntity<List<AsociadoResponse>> save(@RequestParam("file") MultipartFile file,@RequestParam(defaultValue = ",")String separator) {
        List<AsociadoResponse> response = new ArrayList<>();
        try {
            this.asociadoBusiness.saveBatch(file, separator);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(response, response == null ? HttpStatus.INTERNAL_SERVER_ERROR : HttpStatus.OK);
    }
    
    @PostMapping("/update")
    public ResponseEntity<Integer> update(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
        Integer response = null;
        try {
            response = this.asociadoBusiness.updateStatus(file, new KeyDTO(request.getHeader(KEY), request.getHeader(VECTOR)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(response, response == null ? HttpStatus.INTERNAL_SERVER_ERROR : HttpStatus.OK);
    }
    
    @PostMapping("/update-win")
    public ResponseEntity<Integer> updateWinNumber(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
        Integer response = null;
        try {
            response = this.asociadoBusiness.updateWinNumber(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(response, response == null ? HttpStatus.INTERNAL_SERVER_ERROR : HttpStatus.OK);
    }

}
