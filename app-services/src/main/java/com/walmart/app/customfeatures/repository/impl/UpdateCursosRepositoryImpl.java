package com.walmart.app.customfeatures.repository.impl;

import com.walmart.app.customfeatures.repository.UpdateCursosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Repository
public class UpdateCursosRepositoryImpl implements UpdateCursosRepository {

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public int updateCursosIncompletos(){
        String query = "update UserCursoInscrito\n" +
                "set\n" +
                "    UserCursoInscrito.Ava = 100,\n" +
                "    UserCursoInscrito.FecFin = GETDATE(),\n" +
                "    UserCursoInscrito.IdBadge = CCB.IdBadge\n" +
                "from\n" +
                "    UserCursoInscrito uca\n" +
                "    left join CatCursoBadge CCB on uca.IdCur = CCB.IdCur\n" +
                "where\n" +
                "    uca.ClvEst = 4\n" +
                "    and uca.Ava < 100\n" +
                "    and 1 = (select [dbo].[Fn_CierraCurso](uca.IdUsu,uca.IdCur))";

        return this.namedParameterJdbcTemplate.update(query, new HashMap<>());
    }

    @Autowired
    public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

}
