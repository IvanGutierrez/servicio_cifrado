/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.model;

import java.util.Objects;

/**
 *
 * @author alan
 */
@CustomTable(name = "CatEjes")
public class CatEjes {

    
    private Long IdEje;
    @CustomColumn(name = "Eje")
    private String Eje;
    @CustomColumn(name = "DesEje")
    private String DesEje;
    @CustomColumn(name = "BanSubeje")
    private Integer BanSubeje;
    @CustomColumn(name = "IdEst")
    private Integer IdEst;

    public Long getIdEje() {
        return IdEje;
    }

    public void setIdEje(Long IdEje) {
        this.IdEje = IdEje;
    }

    public String getEje() {
        return Eje;
    }

    public void setEje(String Eje) {
        this.Eje = Eje;
    }

    public String getDesEje() {
        return DesEje;
    }

    public void setDesEje(String DesEje) {
        this.DesEje = DesEje;
    }

    public Integer getBanSubeje() {
        return BanSubeje;
    }

    public void setBanSubeje(Integer BanSubeje) {
        this.BanSubeje = BanSubeje;
    }

    public Integer getIdEst() {
        return IdEst;
    }

    public void setIdEst(Integer IdEst) {
        this.IdEst = IdEst;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.IdEje);
        hash = 53 * hash + Objects.hashCode(this.Eje);
        hash = 53 * hash + Objects.hashCode(this.DesEje);
        hash = 53 * hash + Objects.hashCode(this.BanSubeje);
        hash = 53 * hash + Objects.hashCode(this.IdEst);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CatEjes other = (CatEjes) obj;
        if (!Objects.equals(this.Eje, other.Eje)) {
            return false;
        }
        if (!Objects.equals(this.DesEje, other.DesEje)) {
            return false;
        }
        if (!Objects.equals(this.IdEje, other.IdEje)) {
            return false;
        }
        if (!Objects.equals(this.BanSubeje, other.BanSubeje)) {
            return false;
        }
        if (!Objects.equals(this.IdEst, other.IdEst)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CatEjes{" + "IdEje=" + IdEje + ", Eje=" + Eje + ", DesEje=" + DesEje + ", BanSubeje=" + BanSubeje + ", IdEst=" + IdEst + '}';
    }
    

}
