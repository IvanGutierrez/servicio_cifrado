/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.repository.impl;

import com.walmart.app.customfeatures.mapper.AsociadoMapper;
import com.walmart.app.customfeatures.model.Asociado;
import com.walmart.app.customfeatures.model.CustomColumn;
import com.walmart.app.customfeatures.repository.AsociadoRepository;
import com.walmart.app.customfeatures.repository.config.CustomBeanProperty;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;


/**
 *
 * @author Alan
 */
@Repository
public class AsociadoRepositoryImpl implements AsociadoRepository {

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    
    private AsociadoMapper asociadoMapper;
    
    @Override
    public List<Asociado> findByUsr(String noAsoc) {
        String query = "SELECT * FROM Asociado WHERE NoAsoc = :NoAsoc";
        Map<String, Object> params = new HashMap<>();
        params.put("NoAsoc", noAsoc);
        return this.namedParameterJdbcTemplate.query(query, params, this.asociadoMapper.asociadoRowMapper());
    }
    
    @Override
    public int updateCvePuestoById(Asociado asociado,Long cvePuesto){
        String sentence = "UPDATE Asociado SET CvePuesto = :CvePuesto WHERE IdAsoc = :IdAsoc";
        Map<String,Object> params = new HashMap<>();
        params.put("CvePuesto", cvePuesto);
        params.put("IdAsoc", asociado.getIdAsoc());
        return this.namedParameterJdbcTemplate.update(sentence, params);
    }
    
    @Override
    public int updateAsociadoById(Asociado asociado){
        StringBuilder sentence = new StringBuilder("UPDATE Asociado SET ");
        sentence.append(getAsociadoColumnsToUpdate(asociado));
        sentence.append(" WHERE IdAsoc = :IdAsoc");
        return this.namedParameterJdbcTemplate.update(sentence.toString(), new CustomBeanProperty(asociado));
    }
    
    private String getAsociadoColumnsToUpdate(Asociado asociado) {
        return Arrays.stream(asociado.getClass().getDeclaredFields())
                .filter(field -> {
                    CustomColumn customColumnAnnotation = field.getAnnotation(CustomColumn.class);
                    return customColumnAnnotation != null && !customColumnAnnotation.name().equals("IdAsoc") &&  !customColumnAnnotation.name().equals("CvePuesto") && !customColumnAnnotation.name().equals("NoAsoc") && !customColumnAnnotation.name().equals("IdPerfil");
                })
                .map(field -> {
                    CustomColumn customColumnAnnotation = field.getAnnotation(CustomColumn.class);
                    return customColumnAnnotation.name().concat(" = :").concat(customColumnAnnotation.name());
                }).collect(Collectors.joining(", "));
    }
    
    @Override
    public int[] updateStatus(List<Asociado> asociadoList){
        String sentence = "UPDATE Asociado SET Estatus = :Estatus WHERE Usr = :Usr";
        List<Map<String,Object>> paramsList = asociadoList.stream().map(asociado-> {
            Map<String,Object> params = new HashMap<>();
            params.put("Estatus", asociado.getEstatus());
            params.put("Usr",asociado.getUsr());
            return params;
        }).collect(Collectors.toList());
        return this.namedParameterJdbcTemplate.batchUpdate(sentence, paramsList.toArray(new Map[paramsList.size()]));
    }
    
    @Override
    public int[] updateWinNumberByAsoc(List<Asociado> asociadoList){
        String sentence = "UPDATE Asociado SET winNumber = :winNumber WHERE NoAsoc = :NoAsoc";
        List<Map<String,Object>> paramsList = asociadoList.stream().map(asociado-> {
            Map<String,Object> params = new HashMap<>();
            params.put("NoAsoc", asociado.getNoAsoc());
            params.put("winNumber",asociado.getWinNumber());
            return params;
        }).collect(Collectors.toList());
        return this.namedParameterJdbcTemplate.batchUpdate(sentence, paramsList.toArray(new Map[paramsList.size()]));
    }
    
    @Override
    public Boolean findWinNumberByNoAsoc(String noAsoc){
        String sentence = "select a.winNumber from Asociado a where a.NoAsoc = :NoAsoc";
        Map<String,Object> params = new HashMap<>();
        params.put("NoAsoc", noAsoc);
        List<Map<String,Object>> result = this.namedParameterJdbcTemplate.queryForList(sentence, params);
        return result.stream().findFirst().isPresent();
    }

    @Override
    public Long save(Asociado asociado) {
        KeyHolder generatedKeyHolder = new GeneratedKeyHolder();
        StringBuilder sentence = new StringBuilder("INSERT INTO Asociado(CvePuesto,NoAsoc,Nom,Gen,FecNac,FecIng,FecAlta,FecUMod,Usr,Psw,PswCad,Int,Ses,Apa,Ama,Ema,Ter,FecUVis,FecAct,TFij,TCel,Eciv,Estatus,ImgAsoc,IdPerfil,winNumber,supervisor)");
        sentence.append(" VALUES(:CvePuesto,:NoAsoc,:Nom,:Gen,:FecNac,:FecIng,:FecAlta,:FecUMod,:Usr,:Psw,:PswCad,0,:Ses,:Apa,:Ama,:Ema,:Ter,:FecUVis,:FecAct,:TFij,:TCel,:Eciv,:Estatus,:ImgAsoc,:IdPerfil,:winNumber,:supervisor)");
        this.namedParameterJdbcTemplate.update(sentence.toString(), new CustomBeanProperty(asociado), generatedKeyHolder);
        return Long.valueOf(generatedKeyHolder.getKeys().get("GENERATED_KEYS").toString());
    }

    @Override
    public List<Asociado> getAllAsociados() {
        String query = "SELECT * FROM Asociado order by IdAsoc";
        return this.namedParameterJdbcTemplate.query(query, new BeanPropertyRowMapper<>(Asociado.class));
    }

    @Autowired
    @Override
    public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Autowired
    public void setAsociadoMapper(AsociadoMapper asociadoMapper) {
        this.asociadoMapper = asociadoMapper;
    }
    
}
