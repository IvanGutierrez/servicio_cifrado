/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.app.customfeatures.dto;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 *
 * @author Alan
 */
public class DynamicReportRequestDTO {
    
    private String path;
    private String fileNameSql;
    private String reportFileName;
    private Map<String,?> params = new HashMap<>();

    public DynamicReportRequestDTO(){
    
    }
    
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFileNameSql() {
        return fileNameSql;
    }

    public void setFileNameSql(String fileNameSql) {
        this.fileNameSql = fileNameSql;
    }

    public String getReportFileName() {
        return reportFileName;
    }

    public void setReportFileName(String reportFileName) {
        this.reportFileName = reportFileName;
    }

    public Map<String, ?> getParams() {
        return params;
    }

    public void setParams(Map<String, ?> params) {
        this.params = params;
    }

}
