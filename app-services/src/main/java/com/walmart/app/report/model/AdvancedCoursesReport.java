package com.walmart.app.report.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AdvancedCoursesReport {

	
	private String noAsoc;
	private String idRef;
	private Long ava;
	private String fechaFin;
	private Long cvePuesto;
	private Long cveNivDL;
	private Long cveDepto;
	private Long cveDeter;
	private Long cveDistrito;
	private Long cveRegion;
	private Long cveNegocio;
	private Long cveZE;
	private Long cvePais;
	private Integer cal;
	
	public AdvancedCoursesReport(String noAsoc, String idRef, Long ava, String fechaFin, Long cvePuesto, Long cveNivDL,
			Long cveDepto, Long cveDeter, Long cveDistrito, Long cveRegion, Long cveNegocio, Long cveZE, Long cvePais, Integer cal) {
		super();
		this.noAsoc = noAsoc;
		this.idRef = idRef;
		this.ava = ava;
		this.fechaFin = fechaFin;
		this.cvePuesto = cvePuesto;
		this.cveNivDL = cveNivDL;
		this.cveDepto = cveDepto;
		this.cveDeter = cveDeter;
		this.cveDistrito = cveDistrito;
		this.cveRegion = cveRegion;
		this.cveNegocio = cveNegocio;
		this.cveZE = cveZE;
		this.cvePais = cvePais;
		this.cal = cal;
	}

}
