package com.walmart.app.report.business.converter;

import java.util.Date;
import java.util.Optional;

import com.walmart.app.report.dto.opencsv.AsociadosDTO;
import com.walmart.app.report.model.AsocDatPersonal;
import com.walmart.app.report.model.Asociado;
import com.walmart.app.securitytool.dto.SecurityMessageDTO;
import com.walmart.app.securitytool.util.EncryptUtils;
import com.walmart.app.util.DateUtils;


public class AsociadoConverter {

	public static Asociado convertToAsociadoFromAsociadosDTO(AsociadosDTO asociadoDTO, String key, String vector) {
		
		Asociado asociado = new Asociado();
		SecurityMessageDTO securityMessage = new SecurityMessageDTO(key, vector, asociadoDTO.getApm());
		asociado.setAma(EncryptUtils.encrypt(EncryptUtils.CYPHER_CBC, securityMessage));
		securityMessage.setMessage(asociadoDTO.getApp());
		asociado.setApa(EncryptUtils.encrypt(EncryptUtils.CYPHER_CBC, securityMessage));
		asociado.setCvePuesto(asociadoDTO.getCvePuesto());
		asociado.setEciv(asociadoDTO.getEciv());
		asociado.setEma(asociadoDTO.getEma());
		asociado.setFecAct(new Date());
		asociado.setFecAlta(new Date());
		Optional<Date> date = DateUtils.convertStringToDate(DateUtils.DEFAULT_DATE_FORMAT, asociadoDTO.getFingreso());
		if (date.isPresent()) {
			asociado.setFecIng(date.get());
		}
		date = DateUtils.convertStringToDate(DateUtils.DEFAULT_DATE_FORMAT, asociadoDTO.getFnacimiento());
		if (date.isPresent()) {
			asociado.setFecNac(date.get());
		}
		date = DateUtils.convertStringToDate(DateUtils.DEFAULT_DATE_FORMAT, asociadoDTO.getFumod());
		if (date.isPresent()) {
			asociado.setFecUMod(date.get());
		}
		asociado.setFecUVis(new Date());
		asociado.setGen(asociadoDTO.getGenero());
		asociado.setNoAsoc(asociadoDTO.getNoAsoc());
		securityMessage.setMessage(asociadoDTO.getNombre());
		asociado.setNom(EncryptUtils.encrypt(EncryptUtils.CYPHER_CBC, securityMessage));
		securityMessage.setMessage(asociadoDTO.getPassword());
		asociado.setPsw(EncryptUtils.encrypt(EncryptUtils.CYPHER_CBC, securityMessage));
		asociado.setTCel(asociadoDTO.getTcel());
		asociado.setTer(asociadoDTO.getTerm());
		asociado.setTFij(asociadoDTO.getTfijo());
		asociado.setUsr(asociadoDTO.getUsername());
		
		return asociado;

	}

	public static AsocDatPersonal convertToAsocDatPersonalFromAsociadosDTO(AsociadosDTO asociadoDTO, Long idAsoc, String key, String vector) {
		AsocDatPersonal asocDat = new AsocDatPersonal();
		SecurityMessageDTO securityMessage = new SecurityMessageDTO(key, vector, asociadoDTO.getCerestudios());
		asocDat.setCerEst(EncryptUtils.encrypt(EncryptUtils.CYPHER_CBC, securityMessage));
		securityMessage.setMessage(asociadoDTO.getCurp());
		asocDat.setCurp(asociadoDTO.getCurp());
		asocDat.setFecAct(new Date());
		asocDat.setFecUVis(new Date());
		asocDat.setIdAsoc(idAsoc);
		asocDat.setEscol("1");
		securityMessage.setMessage(asociadoDTO.getImss());
		asocDat.setImss(EncryptUtils.encrypt(EncryptUtils.CYPHER_CBC, securityMessage));
		securityMessage.setMessage(asociadoDTO.getRsocial());
		asocDat.setRazSoc(EncryptUtils.encrypt(EncryptUtils.CYPHER_CBC, securityMessage));
		securityMessage.setMessage(asociadoDTO.getRfc());
		asocDat.setRfc(EncryptUtils.encrypt(EncryptUtils.CYPHER_CBC, securityMessage));
		securityMessage.setMessage(asociadoDTO.getCurp());
		asocDat.setCurp(EncryptUtils.encrypt(EncryptUtils.CYPHER_CBC, securityMessage));
		return asocDat;
	}

}
