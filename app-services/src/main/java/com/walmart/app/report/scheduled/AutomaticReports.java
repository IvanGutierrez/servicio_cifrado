package com.walmart.app.report.scheduled;

import java.util.List;

import com.walmart.app.customfeatures.business.UpdateCursosScheduled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.walmart.app.report.business.CsvBusiness;
import com.walmart.app.report.business.ReportBusiness;
import com.walmart.app.report.model.AdvancedCoursesReport;

@Lazy
@Service
public class AutomaticReports {

	private Logger log = LoggerFactory.getLogger(AutomaticReports.class);

	@Value("${report.advanced-report.path}")
	private String path;
		
	@Autowired
	private ReportBusiness reportBusiness;
	
	@Autowired
	private CsvBusiness csvBusiness;
	
	// @Scheduled(cron = "${report.cron}")
	public void scheduledReports() {
		this.log.info("scheduledReports desactivado");
		List<AdvancedCoursesReport> list = reportBusiness.getAdvancedCoursesReport();
		csvBusiness.createCsvFromBean(AdvancedCoursesReport.class, list, path);
	}
	
	public void generateDataLearningReport() {
		List<AdvancedCoursesReport> list = reportBusiness.getAdvancedCoursesReport();
		csvBusiness.createCsvFromBean(AdvancedCoursesReport.class, list, path);
	}

}
