package com.walmart.app.report.repository;

import java.util.List;

import com.walmart.app.report.model.AdvancedCoursesReport;

public interface ReportsRepository {

	public List<AdvancedCoursesReport> getUserCursoAvance();
}
