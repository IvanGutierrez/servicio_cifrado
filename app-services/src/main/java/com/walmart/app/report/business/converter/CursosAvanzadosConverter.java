package com.walmart.app.report.business.converter;

import java.util.List;
import java.util.stream.Collectors;

import com.walmart.app.report.model.AdvancedCoursesReport;
import com.walmart.app.report.model.UserCursoAvance;
import com.walmart.app.util.DateUtils;
import com.walmart.app.util.StreamUtils;

public class CursosAvanzadosConverter {

	public static List<AdvancedCoursesReport> convertUserCursosAvanceListToAdvancedCoursesReportDTOList(
			List<UserCursoAvance> userCursoAvance) {
		return StreamUtils.asStream(userCursoAvance)
				.map(CursosAvanzadosConverter::convertUserCursosAvanceToAdvancedCoursesReportDTO)
				.collect(Collectors.toList());
	}

	public static AdvancedCoursesReport convertUserCursosAvanceToAdvancedCoursesReportDTO(
			UserCursoAvance userCursoAvance) {
		AdvancedCoursesReport advanceCoursesReport = new AdvancedCoursesReport();
		advanceCoursesReport.setNoAsoc(userCursoAvance.getIdUsu().getNoAsoc());
		advanceCoursesReport
				.setFechaFin(DateUtils.convertDateToString(DateUtils.DEFAULT_DATE_FORMAT, userCursoAvance.getFecFin()));
		advanceCoursesReport.setAva(userCursoAvance.getAva());

		if (userCursoAvance.getIdCur() != null) {
			advanceCoursesReport.setIdRef(userCursoAvance.getIdCur().getIdRef());

			if (userCursoAvance.getIdUsu().getRelAsocWm() != null) {

				advanceCoursesReport.setCveDepto(userCursoAvance.getIdUsu().getRelAsocWm().getCveDepto());
				advanceCoursesReport.setCveDeter(userCursoAvance.getIdUsu().getRelAsocWm().getCveDeter());
				advanceCoursesReport.setCveDistrito(userCursoAvance.getIdUsu().getRelAsocWm().getCveDistrito());
				advanceCoursesReport.setCveNegocio(userCursoAvance.getIdUsu().getRelAsocWm().getCveNegocio());
				advanceCoursesReport.setCveNivDL(userCursoAvance.getIdUsu().getRelAsocWm().getCveNivDL());
				advanceCoursesReport.setCvePais(userCursoAvance.getIdUsu().getRelAsocWm().getCvePais());
				advanceCoursesReport.setCvePuesto(userCursoAvance.getIdUsu().getRelAsocWm().getCvePuesto());
				advanceCoursesReport.setCveRegion(userCursoAvance.getIdUsu().getRelAsocWm().getCveRegion());
				advanceCoursesReport.setCveZE(userCursoAvance.getIdUsu().getRelAsocWm().getCveZE());
			}
		}
		return advanceCoursesReport;

	}

}
