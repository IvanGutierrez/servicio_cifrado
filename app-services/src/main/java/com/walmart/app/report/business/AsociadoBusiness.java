package com.walmart.app.report.business;

import com.walmart.app.report.dto.opencsv.AsociadosDTO;
import com.walmart.app.securitytool.dto.KeyDTO;

public interface AsociadoBusiness {
	
	public void saveAsociadoInformation(AsociadosDTO asociadoDTO, KeyDTO key);

}
