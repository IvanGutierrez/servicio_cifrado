package com.walmart.app.report.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.walmart.app.report.model.UserCursoAvance;

@Repository
public interface UserCursoAvanceRepository extends JpaRepository<UserCursoAvance, Long>{

}
