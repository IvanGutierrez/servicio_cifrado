package com.walmart.app.report.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.walmart.app.report.model.AsocDatPersonal;

@Repository
public interface AsociadoDatPersonalRepository extends JpaRepository<AsocDatPersonal, Long>{

}
