package com.walmart.app.report.dto;

import java.util.List;

import com.walmart.app.report.enums.CsvImportStatusEnum;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CountStatusDTO {
	
	private int count;
	
	public CountStatusDTO(int count, CsvImportStatusEnum status, List<MessageRow> messages) {
		super();
		this.count = count;
		this.status = status;
		this.messages = messages;
	}

	private CsvImportStatusEnum status;
	
	private List<MessageRow> messages;

}
