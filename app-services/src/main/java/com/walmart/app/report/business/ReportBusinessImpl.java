package com.walmart.app.report.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.walmart.app.report.model.AdvancedCoursesReport;
import com.walmart.app.report.repository.ReportsRepository;

@Service
public class ReportBusinessImpl implements ReportBusiness {
	
	@Autowired
	private ReportsRepository reportsRepository;

	@Override
	public List<AdvancedCoursesReport> getAdvancedCoursesReport() {
		return reportsRepository.getUserCursoAvance();
	}

}
