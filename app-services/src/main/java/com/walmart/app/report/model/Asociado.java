package com.walmart.app.report.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "Asociado")
@Getter @Setter @NoArgsConstructor
public class Asociado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdAsoc")
    private Long idAsoc;
    @Basic(optional = false)
    @Column(name = "CvePuesto")
    private Long cvePuesto;
    @Basic(optional = false)
    @Column(name = "NoAsoc")
    private String noAsoc;
    @Basic(optional = false)
    @Column(name = "Nom")
    private String nom;
    @Basic(optional = false)
    @Column(name = "Gen")
    private String gen;
    @Basic(optional = false)
    @Column(name = "FecNac")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecNac;
    @Basic(optional = false)
    @Column(name = "FecIng")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecIng;
    @Basic(optional = false)
    @Column(name = "FecAlta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecAlta;
    @Basic(optional = false)
    @Column(name = "FecUMod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecUMod;
    @Basic(optional = false)
    @Column(name = "Usr")
    private String usr;
    @Basic(optional = false)
    @Column(name = "Psw")
    private String psw;
    @Column(name = "PswCad")
    @Temporal(TemporalType.DATE)
    private Date pswCad;
    @Basic(optional = false)
    @Column(name = "Int")
    private int int1;
    @Column(name = "Ses")
    private String ses;
    @Basic(optional = false)
    @Column(name = "Apa")
    private String apa;
    @Basic(optional = false)
    @Column(name = "Ama")
    private String ama;
    @Basic(optional = false)
    @Column(name = "Ema")
    private String ema;
    @Basic(optional = false)
    @Column(name = "Ter")
    private int ter;
    @Column(name = "FecUVis")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecUVis;
    @Column(name = "FecAct")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecAct;
    @Basic(optional = false)
    @Column(name = "TFij")
    private String tFij;
    @Basic(optional = false)
    @Column(name = "TCel")
    private String tCel;
    @Basic(optional = false)
    @Column(name = "Eciv")
    private String eciv;
    @Column(name = "Estatus")
    private Integer estatus;
    @Column(name = "ImgAsoc")
    private String imgAsoc;
    @JoinColumn(name = "IdAsoc",nullable = true)
    @OneToOne(optional = true, cascade = CascadeType.ALL)
    private RelAsocWm relAsocWm;


    public Asociado(Long idAsoc) {
        this.idAsoc = idAsoc;
    }

    public Asociado(Long idAsoc, long cvePuesto, String noAsoc, String nom, String gen, Date fecNac, Date fecIng, Date fecAlta, Date fecUMod, String usr, String psw, int int1, String apa, String ama, String ema, int ter, String tFij, String tCel, String eciv) {
        this.idAsoc = idAsoc;
        this.cvePuesto = cvePuesto;
        this.noAsoc = noAsoc;
        this.nom = nom;
        this.gen = gen;
        this.fecNac = fecNac;
        this.fecIng = fecIng;
        this.fecAlta = fecAlta;
        this.fecUMod = fecUMod;
        this.usr = usr;
        this.psw = psw;
        this.int1 = int1;
        this.apa = apa;
        this.ama = ama;
        this.ema = ema;
        this.ter = ter;
        this.tFij = tFij;
        this.tCel = tCel;
        this.eciv = eciv;
    }

	@Override
	public int hashCode() {
		return Objects.hash(idAsoc);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Asociado other = (Asociado) obj;
		return Objects.equals(idAsoc, other.idAsoc);
	}

    
    
}
