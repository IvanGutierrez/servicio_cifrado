package com.walmart.app.report.model;

import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "RelAsocWm")
@Getter @Setter @NoArgsConstructor
public class RelAsocWm {
	
	private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdRelAsocWm")
    private Long idRelAsocWm;
    @Column(name = "CvePuesto")
    private Long cvePuesto;
    @Column(name = "CveNivDL")
    private Long cveNivDL;
    @Column(name = "cveDepto")
    private Long cveDepto;
    @Column(name = "cveDeter")
    private Long cveDeter;
    @Column(name = "cveDistrito")
    private Long cveDistrito;
    @Column(name = "CveRegion")
    private Long cveRegion;
    @Column(name = "CveNegocio")
    private Long cveNegocio;
    @Column(name = "CveZE")
    private Long cveZE;
    @Column(name = "CvePais")
    private Long cvePais;
	@Override
	public int hashCode() {
		return Objects.hash(idRelAsocWm);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RelAsocWm other = (RelAsocWm) obj;
		return Objects.equals(idRelAsocWm, other.idRelAsocWm);
	}
    
    

}
