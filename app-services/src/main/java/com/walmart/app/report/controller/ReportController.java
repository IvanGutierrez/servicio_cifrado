package com.walmart.app.report.controller;

import com.walmart.app.customfeatures.business.DynamiReportBusiness;
import com.walmart.app.customfeatures.dto.DynamicReportAsyncResponseDTO;
import com.walmart.app.customfeatures.dto.DynamicReportRequestDTO;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import io.swagger.models.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.walmart.app.exception.BusinessException;
import com.walmart.app.report.business.CsvBusiness;
import com.walmart.app.report.business.ReportBusiness;
import com.walmart.app.report.dto.CountStatusDTO;
import com.walmart.app.report.model.AdvancedCoursesReport;
import com.walmart.app.securitytool.dto.KeyDTO;

import java.util.Map;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpHeaders;
import org.springframework.util.MultiValueMap;

@Controller
@RequestMapping("/reports")
@Validated
public class ReportController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportController.class);
    private static final String VECTOR = "vector";
    private static final String KEY = "key";

    @Autowired
    private CsvBusiness csvBusiness;

    @Autowired
    private ReportBusiness reportBusiness;

    @Autowired
    private DynamiReportBusiness dynamiReportBusiness;

    @PostMapping("/importCsv")
    @CrossOrigin
    public ResponseEntity<List<CountStatusDTO>> importCsv(@RequestParam("file") MultipartFile file,
                                                          HttpServletRequest request) {
        try {
            KeyDTO key = new KeyDTO(request.getHeader(KEY), request.getHeader(VECTOR));
            List<CountStatusDTO> result = csvBusiness.saveAsociateCsvInformation(file, key);
            return new ResponseEntity<List<CountStatusDTO>>(result, HttpStatus.OK);
        } catch (BusinessException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return new ResponseEntity<List<CountStatusDTO>>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @GetMapping("/data-learning/getReport")
    @CrossOrigin
    public ResponseEntity<List<AdvancedCoursesReport>> getReport() {
        try {
            return new ResponseEntity<List<AdvancedCoursesReport>>(reportBusiness.getAdvancedCoursesReport(),
              HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return new ResponseEntity<List<AdvancedCoursesReport>>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @PostMapping("/upload-sql-form-dinamic-report")
    @CrossOrigin
    public ResponseEntity<Integer> uploadSqlForDynamicReport(@RequestParam("file") MultipartFile file){
        this.dynamiReportBusiness.uploadSqlForGenericReport(file);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/generic-report-csv")
    @CrossOrigin
    public void genericReportCsv(@RequestParam Map<String, String> queryMap, HttpServletResponse response) {
        DynamicReportRequestDTO dynamicReportRequestDTO = this.dynamiReportBusiness.buildRequestDTO(queryMap);
        response.setContentType("text/csv");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + dynamicReportRequestDTO.getReportFileName().concat(".csv") + "\"");
        try {
            this.dynamiReportBusiness.generateDynamicReport(dynamicReportRequestDTO, response);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }finally {
            System.gc();
        }
    }

    @GetMapping("/all-async-reports")
    @CrossOrigin
    public ResponseEntity<?> getAllAsyncReports(){
        List<Map<String,Object>> response = this.dynamiReportBusiness.getReportesAsincronos();
        return new ResponseEntity<>(response,HttpStatus.OK);
    }

    @GetMapping("/generic-report-csv-async")
    @CrossOrigin
    public ResponseEntity<DynamicReportAsyncResponseDTO> genericReportCsvAsync(@RequestParam Map<String, String> queryMap) {
        DynamicReportRequestDTO dynamicReportRequestDTO = this.dynamiReportBusiness.buildRequestDTO(queryMap);
        DynamicReportAsyncResponseDTO response = null;
        try {
            response = this.dynamiReportBusiness.generateDynamicReportAsync(dynamicReportRequestDTO);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return new ResponseEntity<>(response,response != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR  );
    }

    @GetMapping("/generic-report-csv-async/{id}")
    @CrossOrigin
    public ResponseEntity<DynamicReportAsyncResponseDTO> genericReportCsvAsync(@PathVariable Integer id) {
        DynamicReportAsyncResponseDTO response = this.dynamiReportBusiness.getReportAsyncInformation(id);
        return new ResponseEntity<>(response,response != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR  );
    }

    @GetMapping("/generic-report-json")
    @CrossOrigin
    public ResponseEntity<List<Map<String, Object>>> genericReportJson(@RequestParam Map<String, String> queryMap) {
        DynamicReportRequestDTO dynamicReportRequestDTO = this.dynamiReportBusiness.buildRequestDTO(queryMap);
        List<Map<String, Object>> responseList = null;
        try {
            responseList = this.dynamiReportBusiness.generateDynamicReportJson(dynamicReportRequestDTO);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return new ResponseEntity<>(responseList, responseList == null ? HttpStatus.INTERNAL_SERVER_ERROR : HttpStatus.OK);
    }
}
