package com.walmart.app.report.business;

import com.walmart.app.report.dto.opencsv.CustomMappingStrategy;
import com.walmart.app.report.enums.CsvImportStatusEnum;
import com.opencsv.CSVWriter;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.enums.CSVReaderNullFieldIndicator;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import com.walmart.app.report.dto.CountStatusDTO;
import com.walmart.app.report.dto.MessageRow;
import com.walmart.app.report.dto.opencsv.AsociadosDTO;
import com.walmart.app.securitytool.dto.KeyDTO;
import com.walmart.app.exception.BusinessException;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.StreamSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class CsvBusinessImpl implements CsvBusiness {

	private static final Logger LOGGER = LoggerFactory.getLogger(CsvBusinessImpl.class);

	private static final char CSV_DELIMITER = ';';

	@Autowired
	private AsociadoBusiness asociadoBusiness;

	@Override
	public List<CountStatusDTO> saveAsociateCsvInformation(MultipartFile file, KeyDTO keys) {
		List<CountStatusDTO> statusList = new ArrayList<>();
		List<MessageRow> asociadosErrorList = new ArrayList<>();
		List<MessageRow> asociadosExistentList = new ArrayList<>();
		AtomicInteger successCount = new AtomicInteger(0);
		AtomicInteger errorCount = new AtomicInteger(0);
		AtomicInteger existentCount = new AtomicInteger(0);
		try (Reader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
			CsvToBean<AsociadosDTO> cb = new CsvToBeanBuilder(reader).withType(AsociadosDTO.class).withSkipLines(1)
					.withSeparator(CSV_DELIMITER).withFieldAsNull(CSVReaderNullFieldIndicator.EMPTY_SEPARATORS)
					.withIgnoreLeadingWhiteSpace(true).withThrowExceptions(true).build();

			StreamSupport.stream(cb.spliterator(), false).forEach(asociado -> {
				try {
					asociadoBusiness.saveAsociadoInformation(asociado,
							new KeyDTO(keys.getSecretKey(), keys.getVector()));
					successCount.getAndIncrement();
				} catch (BusinessException e) {
					asociadosExistentList.add(new MessageRow(e.getMessage(), asociado.toString()));
					existentCount.getAndIncrement();
					LOGGER.error(e.getMessage(), e);
				} catch (Exception e) {
					errorCount.getAndIncrement();
					asociadosErrorList.add(new MessageRow(e.getMessage(), asociado.toString()));
					LOGGER.error(e.getMessage(), e);
				}
			});

			LOGGER.info("-------------------------------------");
			LOGGER.info("Successful row(s): {}", successCount);
			statusList.add(new CountStatusDTO(successCount.get(), CsvImportStatusEnum.SUCCESSFUL, new ArrayList<>()));
			LOGGER.info("-------------------------------------");
			LOGGER.info("Error row(s): {}", errorCount);
			statusList.add(new CountStatusDTO(errorCount.get(), CsvImportStatusEnum.FAILED, asociadosErrorList));
			LOGGER.info("-------------------------------------");
			LOGGER.info("Repeat row(s): {}", existentCount);
			statusList.add(new CountStatusDTO(existentCount.get(), CsvImportStatusEnum.REPEDITED, asociadosExistentList));
			LOGGER.info("-------------------------------------");

		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
		}
		
		return statusList;
	}

	@Override
	public <T> Boolean createCsvFromBean(Class clazz, List<T> beanList, String path) {
		Boolean correct = Boolean.TRUE;
		try (Writer writer = Files.newBufferedWriter(Paths.get(path));) {
			final CustomMappingStrategy<T> mappingStrategy = new CustomMappingStrategy<>();
			mappingStrategy.setType(clazz);
			StatefulBeanToCsv<T> beanToCsv = new StatefulBeanToCsvBuilder(writer)
					.withMappingStrategy(mappingStrategy)
					.withQuotechar(CSVWriter.NO_QUOTE_CHARACTER).build();

			beanToCsv.write(beanList);
		} catch (CsvDataTypeMismatchException | CsvRequiredFieldEmptyException | IOException e) {
			LOGGER.error(e.getMessage(), e);
			correct = Boolean.FALSE;
		}
		return correct;
	}

}
