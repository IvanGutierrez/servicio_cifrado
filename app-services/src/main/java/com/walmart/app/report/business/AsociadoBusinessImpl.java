package com.walmart.app.report.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.walmart.app.report.business.converter.AsociadoConverter;
import com.walmart.app.report.dto.opencsv.AsociadosDTO;
import com.walmart.app.exception.BusinessException;
import com.walmart.app.report.model.Asociado;
import com.walmart.app.report.repository.AsociadoDatPersonalRepository;
import com.walmart.app.report.repository.AsociadoRepository;
import com.walmart.app.securitytool.dto.KeyDTO;

@Service
public class AsociadoBusinessImpl implements AsociadoBusiness{

	@Autowired
	private AsociadoRepository asociadoRepository;
	
	@Autowired
	private AsociadoDatPersonalRepository asociadoDatPersonalRepository;
	
	@Override
	@Transactional(readOnly = false)
	public void saveAsociadoInformation(AsociadosDTO asociadoDTO, KeyDTO key) {
		int countNoAsociado = asociadoRepository.findCountByNoAsoc(asociadoDTO.getNoAsoc());
		if(countNoAsociado>0) {
			throw new BusinessException("No asociado already exist");
		}
		Asociado asociado = asociadoRepository.save(AsociadoConverter.convertToAsociadoFromAsociadosDTO(asociadoDTO, key.getSecretKey(), key.getVector()));
		asociadoDatPersonalRepository.save(AsociadoConverter.convertToAsocDatPersonalFromAsociadosDTO(asociadoDTO, asociado.getIdAsoc(),key.getSecretKey(), key.getVector()));
		
	}

}
