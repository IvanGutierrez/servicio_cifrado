package com.walmart.app.report.dto.opencsv;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class AsociadosDTO {
	
	@CsvBindByName
	@CsvBindByPosition(position = 0)
	private String noAsoc;
	@CsvBindByName
	@CsvBindByPosition(position = 1)
	private Long cvePuesto;
	@CsvBindByName
	@CsvBindByPosition(position = 2)
	private String nombre;
	@CsvBindByName
	@CsvBindByPosition(position = 3)
	private String app;
	@CsvBindByName
	@CsvBindByPosition(position = 4)
	private String apm;
	@CsvBindByName
	@CsvBindByPosition(position = 5)
	private String ema;
	@CsvBindByName
	@CsvBindByPosition(position = 6)
	private String genero;
	@CsvBindByName
	@CsvBindByPosition(position = 7)
	private String fnacimiento;
	@CsvBindByName
	@CsvBindByPosition(position = 8)
	private String fingreso;
	@CsvBindByName
	@CsvBindByPosition(position = 9)
	private String falta;
	@CsvBindByName
	@CsvBindByPosition(position = 10)
	private String fumod;
	@CsvBindByName
	@CsvBindByPosition(position = 11)
	private String username;
	@CsvBindByName
	@CsvBindByPosition(position = 12)
	private String password;
	@CsvBindByName
	@CsvBindByPosition(position = 13)
	private String level;
	@CsvBindByName
	@CsvBindByPosition(position = 14)
	private String idjefe;
	@CsvBindByName
	@CsvBindByPosition(position = 15)
	private String tfijo;
	@CsvBindByName
	@CsvBindByPosition(position = 16)
	private String tcel;
	@CsvBindByName
	@CsvBindByPosition(position = 17)
	private String inix;
	@CsvBindByName
	@CsvBindByPosition(position = 18)
	private int term;
	@CsvBindByName
	@CsvBindByPosition(position = 19)
	private String eciv;
	@CsvBindByName
	@CsvBindByPosition(position = 20)
	private String cerestudios;
	@CsvBindByName
	@CsvBindByPosition(position = 21)
	private String curp;
	@CsvBindByName
	@CsvBindByPosition(position = 22)
	private String rfc;
	@CsvBindByName
	@CsvBindByPosition(position = 23)
	private String imss;
	@CsvBindByName
	@CsvBindByPosition(position = 24)
	private String rsocial;
	@CsvBindByName
	@CsvBindByPosition(position = 25)
	private String nivld;
	@CsvBindByName
	@CsvBindByPosition(position = 26)
	private String depto;
	@CsvBindByName
	@CsvBindByPosition(position = 27)
	private String deter;
	@CsvBindByName
	@CsvBindByPosition(position = 28)
	private String distr;
	@CsvBindByName
	@CsvBindByPosition(position = 29)
	private String regio;
	@CsvBindByName
	@CsvBindByPosition(position = 30)
	private String negoc;
	@CsvBindByName
	@CsvBindByPosition(position = 31)
	private String zecom;
	@CsvBindByName
	@CsvBindByPosition(position = 32)
	private String pais;
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AsociadosDTO [noAsoc=").append(noAsoc).append(", cvePuesto=").append(cvePuesto)
				.append(", nombre=").append(nombre).append(", app=").append(app).append(", apm=").append(apm)
				.append(", ema=").append(ema).append(", genero=").append(genero).append(", fnacimiento=")
				.append(fnacimiento).append(", fingreso=").append(fingreso).append(", falta=").append(falta)
				.append(", fumod=").append(fumod).append(", username=").append(username).append(", password=")
				.append(password).append(", level=").append(level).append(", idjefe=").append(idjefe).append(", tfijo=")
				.append(tfijo).append(", tcel=").append(tcel).append(", inix=").append(inix).append(", term=")
				.append(term).append(", eciv=").append(eciv).append(", cerestudios=").append(cerestudios)
				.append(", curp=").append(curp).append(", rfc=").append(rfc).append(", imss=").append(imss)
				.append(", rsocial=").append(rsocial).append(", nivld=").append(nivld).append(", depto=").append(depto)
				.append(", deter=").append(deter).append(", distr=").append(distr).append(", regio=").append(regio)
				.append(", negoc=").append(negoc).append(", zecom=").append(zecom).append(", pais=").append(pais)
				.append("]");
		return builder.toString();
	}

}
