package com.walmart.app.report.business;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.walmart.app.report.dto.CountStatusDTO;
import com.walmart.app.securitytool.dto.KeyDTO;

public interface CsvBusiness {

	public List<CountStatusDTO> saveAsociateCsvInformation(MultipartFile multipartFile, KeyDTO keys);
	
	public <T> Boolean createCsvFromBean(Class clazz, List<T> beanList, String path);
	
}
