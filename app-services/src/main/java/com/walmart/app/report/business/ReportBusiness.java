package com.walmart.app.report.business;

import java.util.List;

import com.walmart.app.report.model.AdvancedCoursesReport;

public interface ReportBusiness {
	
	List<AdvancedCoursesReport> getAdvancedCoursesReport();


}
