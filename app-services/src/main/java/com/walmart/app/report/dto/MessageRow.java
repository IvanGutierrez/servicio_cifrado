package com.walmart.app.report.dto;

import com.walmart.app.report.dto.opencsv.AsociadosDTO;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class MessageRow {
	
	

	private String message;
	
	private String value;
	
	public MessageRow(String message, String value) {
		super();
		this.message = message;
		this.value = value;
	}
	
	public MessageRow(String value) {
		super();
		this.value = value;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MessageRow [value=").append(value).append(", message=").append(message).append("]");
		return builder.toString();
	}

	

}
