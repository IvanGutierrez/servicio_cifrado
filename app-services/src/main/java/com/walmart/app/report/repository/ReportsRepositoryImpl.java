package com.walmart.app.report.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.walmart.app.report.model.AdvancedCoursesReport;

@Repository
public class ReportsRepositoryImpl implements ReportsRepository{

	@PersistenceContext
	private EntityManager entityManager;

	public List<AdvancedCoursesReport> getUserCursoAvance() {

		StringBuilder query = new StringBuilder("select\n").append("	asociado.NoAsoc as noAsoc,\n")
				.append("	cur.idRef as idRef,\n").append("	uca.ava as ava,\n")
				.append("	convert(varchar,uca.FecFin, 20) as fechaFin,\n").append("	relAsoc.cvePuesto,\n")
				.append("	relAsoc.cveNivDL,\n").append("	relAsoc.cveDepto,\n").append("	relAsoc.cveDeter,\n")
				.append("	relAsoc.cveDistrito,\n").append("	relAsoc.cveRegion,\n").append("	relAsoc.cveNegocio,\n")
				.append("	relAsoc.cveZE,\n").append("	relAsoc.cveDepto,\n").append("	relAsoc.cvePais,\n")
				.append("	curins.Cal as cal\n")
				.append("from\n").append("	UserCursoAvance uca\n").append("join Cursos cur\n")
				.append("on uca.IdCur = cur.IdCur \n").append("join RelAsocWm relAsoc on\n")
				.append("	uca.IdUsu = relAsoc.IdAsoc\n").append("join Asociado asociado on\n")
				.append("	asociado.IdAsoc = uca.IdUsu\n")
				.append("join UserCursoInscrito curins\n")
				.append("	on curins.IdUsu = uca.IdUsu")
				.append("	and curins.IdCur =uca.IdCur");

		Query q = entityManager.createNativeQuery(query.toString(), "AdvancedCoursesReport");
		return q.getResultList();
	}
}
