package com.walmart.app.report.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.walmart.app.report.model.Asociado;

@Repository
public interface AsociadoRepository extends JpaRepository<Asociado, Long>{
	
	@Query("select count(1) from Asociado a where a.noAsoc=:noAsoc")
	int findCountByNoAsoc(@Param("noAsoc") String noAsoc);

}
