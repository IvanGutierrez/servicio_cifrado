package com.walmart.app.userexperience.repository;

public interface UsuarioCursoInscritoRepository {
    int updateCalificacion(Long idUsuario, Long idCurso, Long nuevaCalificacion);
}
