package com.walmart.app.userexperience.web;

import com.walmart.app.userexperience.dto.AvanceCursoDTO;
import com.walmart.app.userexperience.repository.UsuarioCursoInscritoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/usuario-curso-inscrito")
public class AvanceCursoController {

    private UsuarioCursoInscritoRepository usuarioCursoInscritoRepository;

    @Autowired
    public void setUsuarioCursoInscritoRepository(UsuarioCursoInscritoRepository usuarioCursoInscritoRepository) {
        this.usuarioCursoInscritoRepository = usuarioCursoInscritoRepository;
    }

    @PostMapping("/actualiza-calificacion")
    @CrossOrigin
    public ResponseEntity<?> updateCalificacion(@RequestBody AvanceCursoDTO avanceCursoDTO) {
        int cursosActualizados = this.usuarioCursoInscritoRepository.updateCalificacion(
                avanceCursoDTO.idUsuario,
                avanceCursoDTO.idCurso,
                avanceCursoDTO.calificacion
        );


        return new ResponseEntity<>(cursosActualizados, HttpStatus.OK);
    }
}
