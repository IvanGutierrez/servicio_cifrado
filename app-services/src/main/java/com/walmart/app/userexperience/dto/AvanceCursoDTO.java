package com.walmart.app.userexperience.dto;

import java.util.Objects;

public class AvanceCursoDTO {
    public Long idUsuario;
    public Long idCurso;
    public Long calificacion;

    public AvanceCursoDTO() {
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Long getIdCurso() {
        return idCurso;
    }

    public void setIdCurso(Long idCurso) {
        this.idCurso = idCurso;
    }

    public Long getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(Long calificacion) {
        this.calificacion = calificacion;
    }

    @Override
    public String toString() {
        return "AvanceCursoDTO{" +
                "idUsuario=" + idUsuario +
                ", idCurso=" + idCurso +
                ", calificacion=" + calificacion +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AvanceCursoDTO that = (AvanceCursoDTO) o;
        return Objects.equals(idUsuario, that.idUsuario) &&
                Objects.equals(idCurso, that.idCurso) &&
                Objects.equals(calificacion, that.calificacion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idUsuario, idCurso, calificacion);
    }
}
