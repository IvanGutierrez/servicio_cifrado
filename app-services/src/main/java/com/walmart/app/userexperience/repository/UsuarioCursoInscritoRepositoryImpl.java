package com.walmart.app.userexperience.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class UsuarioCursoInscritoRepositoryImpl implements UsuarioCursoInscritoRepository {

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public int updateCalificacion(Long idUsuario, Long idCurso, Long nuevaCalificacion) {
        String sentence = "UPDATE UserCursoInscrito " +
                "SET Cal = :calificacion " +
                "WHERE IdUsu = :idUsuario AND IdCur = :idCurso";
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("idUsuario", idUsuario);
        paramMap.put("idCurso", idCurso);
        paramMap.put("calificacion", nuevaCalificacion);
        return this.namedParameterJdbcTemplate.update(sentence, paramMap);
    }
}
