SELECT   Cur.IdRef,Cur.NomCur as Curso, Asoc.NoAsoc, Asoc.Nom,Asoc.Apa,Asoc.Ama,
		    Preguntas.Pre  as Pregunta,
		    case Preguntas.Tipo when 1 then Respuestas.Abierta when 3 then RepuestasTexto.Res 
			when 2 then RepuestasTexto.Res
		    end as Respuesta 
			FROM
		   (Select MAX(A.ClvRel) as ClvRel,A.ClVPre,A.ClvSol as IdUsu ,B.ClvCur
		   from [dbo].[EvaRelResUsuario] as A INNER JOIN  [dbo].[Evaluaciones] AS B ON A.IdTest = B.ClvTest
		   where  A.ClvSol in (select IdUsu from [dbo].[UserCursoInscrito]  where IdCur =:cveCurso)
		   AND B.ClvCur =:idCur
		   Group by A.ClVPre,A.ClvSol,B.ClvCur) as  UltimasRespuestas 
		   inner join [dbo].[EvaRelResUsuario] as Respuestas ON UltimasRespuestas.ClvRel = Respuestas.ClvRel
		   inner join [dbo].[EvaCatPreg] as Preguntas on Preguntas.ClvPre =Respuestas.ClvPre
		   left outer join [dbo].[EvaCatRes] as RepuestasTexto on RepuestasTexto.ClvRes =Respuestas.ClvRes
		   inner join [dbo].[Asociado] as Asoc on Asoc.IdAsoc=Respuestas.ClvSol
		   inner join [dbo].[Cursos] as Cur on UltimasRespuestas.ClvCur=Cur.IdCur

